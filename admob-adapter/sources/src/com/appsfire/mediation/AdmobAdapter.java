package com.appsfire.mediation;

import android.app.Activity;
import android.util.Log;

import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.mediation.MediationInterstitialListener;
import com.mngads.sdk.listener.MNGNativeAdListener;
import com.mngads.sdk.listener.MNGSushiAdListener;
import com.mngads.sdk.nativead.MNGDisplayableNativeAd;
import com.mngads.sdk.nativead.MNGNativeAd;
import com.mngads.sdk.nativead.MNGSushiAd;
import com.google.ads.mediation.MediationInterstitialAdapter;

public class AdmobAdapter implements MediationInterstitialAdapter<AppsfireNetworkExtras,AppsfireServerParameters>, MNGNativeAdListener, MNGSushiAdListener {
	/** Tag for logging messages */
	public static final String CLASS_TAG = "MNGAdmobAdapter";
	
	/** Publisher ID for MNG Appsfire interstitials */
	private static String mPublisherId;
	
	/** Displayable native ad for serving interstitials */
	private MNGDisplayableNativeAd mInterstitial;

	/** Sushi ad unit for presenting interstitials modally */
    private MNGSushiAd mSushiAd;
	
	/** Host activity */
	private Activity mActivity;
	
	/** Custom event listener */
	private MediationInterstitialListener mListener;
	
	/**
	 * Set publisher ID for MNG Appsfire interstitials
	 * 
	 * @param publisherId publisher ID for MNG Appsfire interstitials
	 */
	public static void setPublisherId (String publisherId) {
		mPublisherId = publisherId;
	}
	
    /*
     * MediationInterstitialAdapter implementation
     */
	
	public Class<AppsfireNetworkExtras> getAdditionalParametersType() {
		return AppsfireNetworkExtras.class;
	}

	public Class<AppsfireServerParameters> getServerParametersType() {
		return AppsfireServerParameters.class;
	}
	
	@Override
	public void requestInterstitialAd(MediationInterstitialListener listener,
			Activity activity, AppsfireServerParameters serverParameters,
			MediationAdRequest mediationAdRequest, AppsfireNetworkExtras customEventExtra) {
		mActivity = activity;
		mListener = listener;
		
		// Request native interstitial
		
		Log.d (CLASS_TAG, "requestInterstitialAd");
		mInterstitial = new MNGDisplayableNativeAd (mActivity, mPublisherId);
        mInterstitial.setNativeAdListener(this);
        mInterstitial.loadAd();        
	}
	
	public void showInterstitial() {
		Log.d (CLASS_TAG, "showInterstitial");
		
		// Check if an interstitial ad is ready
		if (mInterstitial != null && mInterstitial.isAdLoaded()) {
			// Present with a sushi modal ad
			mSushiAd = mInterstitial.getSushiAd();
            mSushiAd.setSushiAdListener(this);
            mSushiAd.showAd();			
		}
	}
	
	public void destroy() {
		Log.d (CLASS_TAG, "destroy");
		
        if (mSushiAd != null) {
            mSushiAd.destroy();
            mSushiAd = null;
        }
        
        if (mInterstitial != null) {
	        mInterstitial.destroy();
        }
		
        mActivity = null;
		mListener = null;
	}
	
	// MNGNativeAdListener implementation 
	
	@Override
	public void onAdLoaded(MNGNativeAd mngNativeAd) {
		Log.i (CLASS_TAG, "onAdLoaded");
		if (mActivity != null && mListener != null)
			mListener.onReceivedAd(this);		
	}

	@Override
	public void onError(MNGNativeAd mngNativeAd, Exception e) {
		Log.i (CLASS_TAG, "onAdLoaded");
		if (mActivity != null && mListener != null)
			mListener.onFailedToReceiveAd(this, ErrorCode.NO_FILL);				
	}
	
	@Override
	public void onAdClicked(MNGNativeAd mngNativeAd) {
		// Only used for native ads
	}
	
	// MNGSushiAdListener implementation 
	
	@Override
	public void onInterstitialDisplayed(MNGSushiAd mngSushiAd) {
		Log.i (CLASS_TAG, "onInterstitialDisplayed");
		if (mActivity != null && mListener != null)
			mListener.onPresentScreen(this);
	}

	@Override
	public void onInterstitialDismissed(MNGSushiAd mngSushiAd) {
		Log.i (CLASS_TAG, "onInterstitialDismissed");
		if (mActivity != null && mListener != null)
			mListener.onDismissScreen(this);
	}

	@Override
	public void onAdClicked(MNGSushiAd mngSushiAd) {
		Log.i (CLASS_TAG, "onAdClicked");
		if (mActivity != null && mListener != null)
			mListener.onLeaveApplication(this);
	}

}
