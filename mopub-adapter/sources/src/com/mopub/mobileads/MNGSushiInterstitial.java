package com.mopub.mobileads;

import android.content.Context;
import android.util.Log;

import com.mngads.sdk.listener.MNGNativeAdListener;
import com.mngads.sdk.listener.MNGSushiAdListener;
import com.mngads.sdk.nativead.MNGDisplayableNativeAd;
import com.mngads.sdk.nativead.MNGNativeAd;
import com.mngads.sdk.nativead.MNGSushiAd;

import java.util.Map;

/*
 * Mopub sushi interstitial ads adapter for MNGAdsServer
 */

public class MNGSushiInterstitial extends CustomEventInterstitial implements MNGNativeAdListener, MNGSushiAdListener {
    public static final String TAG = "MNGSushiInterstitial";
    private MNGDisplayableNativeAd mInterstitial;
    private boolean mIsAdReady;
    private MNGSushiAd mSushiAd;
    private CustomEventInterstitialListener mInterstitialListener;

	// MNG publisher ID
    private static String mPublisherId = null;
    
    /**
     * Set MNG publisher ID for Sushi interstitial ads
     * 
     * @param publisherId publisher ID for Sushi interstitial ads
     */
    static public void setPublisherId (String publisherId) {
    	mPublisherId = publisherId;
    }
    
    // CustomEventInterstitial implementation

    @Override
    protected void loadInterstitial(final Context context,
            final CustomEventInterstitialListener customEventInterstitialListener,
            final Map<String, Object> localExtras,
            final Map<String, String> serverExtras) {
        mInterstitialListener = customEventInterstitialListener;

        mInterstitial = new MNGDisplayableNativeAd (context, mPublisherId);
        mInterstitial.setNativeAdListener(this);
        mInterstitial.loadAd();
    }

    @Override
    protected void showInterstitial() {
        if (mInterstitial != null && mIsAdReady) {
        	mIsAdReady = false;
        	mSushiAd = mInterstitial.getSushiAd();
            mSushiAd.setSushiAdListener(this);
            mSushiAd.showAd();
        } else {
            Log.d(TAG, "Not loaded yet");
        }
    }

    @Override
    protected void onInvalidate() {
    	if (mSushiAd != null) {
    		mSushiAd.destroy();
    		mSushiAd = null;
    	}
    	
        if (mInterstitial != null) {
        	mInterstitial.destroy();
        	mInterstitial = null;
        }
    }

    // MNGNativeAdListener implementation

	@Override
	public void onAdLoaded(MNGNativeAd ad) {
        Log.d(TAG, "onAdLoaded");
		mIsAdReady = true;		
        mInterstitialListener.onInterstitialLoaded();
	}

	@Override
	public void onError(MNGNativeAd ad, Exception ex) {
        Log.d(TAG, "onError: " + ex);
		mIsAdReady = false;		
        mInterstitialListener.onInterstitialFailed(MoPubErrorCode.NETWORK_NO_FILL);
	}
	
	@Override
	public void onAdClicked(MNGNativeAd ad) {		
		mInterstitialListener.onInterstitialClicked();
	}	
	
    // MNGSushiAdListener implementation
	
	@Override
	public void onInterstitialDisplayed(MNGSushiAd ad) {
        Log.d(TAG, "onInterstitialDisplayed");
        mInterstitialListener.onInterstitialShown();
	}

	@Override
	public void onInterstitialDismissed(MNGSushiAd ad) {
        Log.d(TAG, "onInterstitialDismissed");
        mInterstitialListener.onInterstitialDismissed();
	}

	@Override
	public void onAdClicked(MNGSushiAd ad) {		
        Log.d(TAG, "onAdClicked");
        mInterstitialListener.onInterstitialClicked();
	}
}