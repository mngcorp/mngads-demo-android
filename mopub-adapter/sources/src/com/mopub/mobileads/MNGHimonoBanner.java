package com.mopub.mobileads;

import android.content.Context;
import android.util.Log;

import com.mngads.sdk.listener.MNGHimonoAdListener;
import com.mngads.sdk.nativead.MNGHimonoAd;
import com.mngads.sdk.util.MNGAdSize;
import com.mopub.common.util.Views;

import java.util.Map;

/*
 * Mopub himono banner ads adapter for MNGAdsServer
 */

public class MNGHimonoBanner extends CustomEventBanner implements MNGHimonoAdListener {
    public static final String TAG = "MNGSushiInterstitial";
    private CustomEventBannerListener mBannerListener;
    private MNGHimonoAd mHimonoAdView;

	// MNG publisher ID
    private static String mPublisherId = null;
    
    /**
     * Set MNG publisher ID for Himono banner ads
     * 
     * @param publisherId publisher ID for Himono banner ads
     */
    static public void setPublisherId (String publisherId) {
    	mPublisherId = publisherId;
    }
    
    @Override
    protected void loadBanner(
            final Context context,
            final CustomEventBannerListener customEventBannerListener,
            final Map<String, Object> localExtras,
            final Map<String, String> serverExtras) {
        mBannerListener = customEventBannerListener;
        
        mHimonoAdView = new MNGHimonoAd (context, MNGAdSize.getMNGAdsHeight50Banner ());
        mHimonoAdView.setHimonoListener(this);
        mHimonoAdView.loadAd(mPublisherId);        
    }

    @Override
    protected void onInvalidate() {
        Views.removeFromParent(mHimonoAdView);
        if (mHimonoAdView != null) {
        	mHimonoAdView = null;
        }
    }

    @Override
    public void himonoBannerViewDidLoadAd (MNGHimonoAd ad) {
        Log.d(TAG, "himonoBannerViewDidLoadAd");
        if (mBannerListener != null) {
            mBannerListener.onBannerLoaded(ad);
        }
    }

    @Override
    public void himonoBannerViewDidFailToLoadAd (MNGHimonoAd ad, Exception ex) {
        Log.d(TAG, "himonoBannerViewDidFailToLoadAd");
        if (mBannerListener != null) {
            mBannerListener.onBannerFailed(MoPubErrorCode.NETWORK_NO_FILL);
        }
    }

    @Override
    public void himonoBannerViewDidRecordClick (MNGHimonoAd ad) {
        Log.d(TAG, "himono: himonoBannerViewDidRecordClick");
        if (mBannerListener != null) {
            mBannerListener.onBannerClicked();
        }
    }    
}
