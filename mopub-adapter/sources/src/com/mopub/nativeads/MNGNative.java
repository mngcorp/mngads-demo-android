package com.mopub.nativeads;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;

import com.mngads.sdk.nativead.MNGNativeAd;
import com.mngads.sdk.listener.MNGNativeAdListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.mopub.nativeads.NativeImageHelper.preCacheImages;

/*
 * Mopub native ads adapter for MNGAdsServer
 */

public class MNGNative extends CustomEventNative {
	// Tag for logging
	private static final String TAG = "MNGNative";
	
	// MNG publisher ID
    private static String mPublisherId = null;

    /**
     * Set MNG publisher ID for native ads
     * 
     * @param publisherId publisher ID for native ads
     */
    static public void setPublisherId (String publisherId) {
    	mPublisherId = publisherId;
    }
    
    // CustomEventNative implementation for MNGAdsServer
    
    @Override
    protected void loadNativeAd(final Activity activity,
            final CustomEventNativeListener customEventNativeListener,
            final Map<String, Object> localExtras,
            final Map<String, String> serverExtras) {

    	Log.d(TAG, "loadNativeAd");
    	
        final MNGStaticNativeAd mngStaticNativeAd = new MNGStaticNativeAd(activity, customEventNativeListener);
        mngStaticNativeAd.setMNGNativeAd(new MNGNativeAd(activity, mPublisherId));
        mngStaticNativeAd.loadAd();
    }

    static class MNGStaticNativeAd extends StaticNativeAd implements MNGNativeAdListener {
        private final Context mContext;
        private final CustomEventNativeListener mCustomEventNativeListener;
        private MNGNativeAd mNativeAd;

        MNGStaticNativeAd(final Context context,
                final CustomEventNativeListener customEventNativeListener) {
            mContext = context.getApplicationContext();
            mCustomEventNativeListener = customEventNativeListener;
        }

        void setMNGNativeAd(final MNGNativeAd mngNativeAd) {
        	mNativeAd = mngNativeAd;
        }

        void loadAd() {
        	mNativeAd.setNativeAdListener(this);
        	mNativeAd.loadAd();
        }

        // MNGNativeAdListener implementation
        
        @Override
        public void onError(MNGNativeAd mngNativeAd, Exception e) {
        	Log.d(TAG, "onError: " + e.toString());
            mCustomEventNativeListener.onNativeAdFailed(NativeErrorCode.NETWORK_NO_FILL);
        }

        @Override
        public void onAdClicked(MNGNativeAd ad) {
        	Log.d(TAG, "onAdClicked");
        	notifyAdClicked();
        }
        
        @Override
        public void onAdLoaded(final MNGNativeAd mngNativeAd) {
        	// Copy metadata from MNG native ad
        	
        	Log.d(TAG, "onAdLoaded");
        	
        	setTitle(mngNativeAd.getTitle());
            setText(mngNativeAd.getDescription());
            
            String[] screenshotURLs = mngNativeAd.getScreenshotURLs();
            if (screenshotURLs != null && screenshotURLs.length >= 1)
            	setMainImageUrl(screenshotURLs[0]);

            setIconImageUrl(mngNativeAd.getIconURL());

            setCallToAction(mngNativeAd.getCallToActionButtonText());
            
            setStarRating(mngNativeAd.getAverageUserRating());

            // Enumerate images to cache
            
            final List<String> imageUrls = new ArrayList<String>();
            final String mainImageUrl = getMainImageUrl();
            if (mainImageUrl != null) {
                imageUrls.add(getMainImageUrl());
            }
            final String iconUrl = getIconImageUrl();
            if (iconUrl != null) {
                imageUrls.add(getIconImageUrl());
            }
            final String privacyInformationIconImageUrl = getPrivacyInformationIconImageUrl();
            if (privacyInformationIconImageUrl != null) {
                imageUrls.add(privacyInformationIconImageUrl);
            }

            // Cache images
            
            Log.d(TAG, "Cache images, count: " + imageUrls.size());
            
            preCacheImages(mContext, imageUrls, new NativeImageHelper.ImageListener() {
                @Override
                public void onImagesCached() {
                	Log.d(TAG, "onImagesCached");
                    mCustomEventNativeListener.onNativeAdLoaded(MNGStaticNativeAd.this);
                }

                @Override
                public void onImagesFailedToCache(NativeErrorCode errorCode) {
                	Log.d(TAG, "onImagesFailedToCache");
                    mCustomEventNativeListener.onNativeAdFailed(errorCode);
                }
            });                    
        }

        // Lifecycle Handlers
        
        @Override
        public void prepare(final View view) {
        	 mNativeAd.registerViewForInteraction(view);
        }

        @Override
        public void clear(final View view) {
        	mNativeAd.registerViewForInteraction(null);
        }

        @Override
        public void destroy() {
        	mNativeAd.destroy();
        	mNativeAd = null;
        }
    }
}