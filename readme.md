# BlueStack Mediation SDK for Android

Madvertise provides functionalities for monetizing your mobile application: from premium sales with rich media, video and innovative formats, it facilitates inserting native mobile ads as well all standard display formats. MngAds SDK (BlueStack) is a library that allow you to handle Ads servers with the easy way.

| **SDK Integration** | 
| -------- | -------- 
| [Setup the Android SDK] | 
|[Android Change Log] 
|[Android Upgrade Guide] 


| **Ad Formats** | 
| -------- | -------- 
| [Android Banner Ads] | 
| [Android Interstitial Ads] |
| [Android Infeed Ads] |
| [Android Native Ads] | 
| [Android Rewarded Video Ads]| 



| **Advanced Settings** | 
| -------- | -------- 
|[Android FAQ and Troubleshooting]
|[Android Debug Mode with Gyroscope Sensor]
|[Android Targeting Audiences] 


| **Mediation Adapters** | 
| -------- | -------- 
| [Android Google Mobile Ads Adapter]
| [Android Mopub Adapter]



[Android Banner Ads]:https://bitbucket.org/mngcorp/mngads-demo-android/wiki/setup
[Setup the Android SDK]:https://bitbucket.org/mngcorp/mngads-demo-android/wiki/setup
[Android Change Log]:https://bitbucket.org/mngcorp/mngads-demo-android/wiki/change-log
[Android Targeting Audiences]:https://bitbucket.org/mngcorp/mngads-demo-android/wiki/targeting-audiences
[Android Native Ads]:https://bitbucket.org/mngcorp/mngads-demo-android/wiki/nativead
[Android Upgrade Guide]:https://bitbucket.org/mngcorp/mngads-demo-android/wiki/upgrading
[Android FAQ and Troubleshooting]:https://bitbucket.org/mngcorp/mngads-demo-android/wiki/faq
[Android Best practices]:https://bitbucket.org/mngcorp/mngads-demo-android/wiki/guidelines
[Android Debug Mode with Gyroscope Sensor]:https://bitbucket.org/mngcorp/mngads-demo-android/wiki/debug-mode-gyro
[Interstitial Guideline]:https://bitbucket.org/mngcorp/mngads-demo-android/wiki/interstitial-guideline
[Android Mopub Adapter]:https://bitbucket.org/mngcorp/mobile.mng-ads.com-mngperf/wiki/mopub-adaptor-android
[Android Rewarded Video Ads]:https://bitbucket.org/mngcorp/mngads-demo-android/wiki/rewarded-video

[Android Infeed Ads]:https://bitbucket.org/mngcorp/mngads-demo-android/wiki/infeed

[Android Banner Ads]:https://bitbucket.org/mngcorp/mngads-demo-android/wiki/banner
[Android Interstitial Ads]:https://bitbucket.org/mngcorp/mngads-demo-android/wiki/interstitial



[Retency]:https://bitbucket.org/mngcorp/mngads-demo-android/wiki/Retency
[MAdvertiseVectaury for Android]:https://bitbucket.org/mngcorp/mngads-demo-android/wiki/MAdvertiseVectaury
[MAdvertiseVectaury for iOS]:https://bitbucket.org/mngcorp/mngads-demo-ios/wiki/MAdvertiseVectaury
[MadvertiseAdExchanges]:https://bitbucket.org/mngcorp/mobile.mng-ads.com-mngperf/wiki/MadvertiseAdExchanges
[MadvertiseLocation for Android]:https://bitbucket.org/mngcorp/mngads-demo-android/wiki/MadvertiseLocation
[MadvertiseLocation for iOS]:https://bitbucket.org/mngcorp/mngads-demo-ios/wiki/MadvertiseLocation
[Location Data Reporting API]:https://bitbucket.org/mngcorp/mobile.mng-ads.com-mngperf/wiki/location-reporting-api
[MadvertiseLocation iOS Change Log]:https://bitbucket.org/mngcorp/mngads-demo-ios/wiki/change-log-madvertiselocation
[MadvertiseLocation Android Change Log]:https://bitbucket.org/mngcorp/mngads-demo-android/wiki/change-log-madvertiselocation
[AmazonPublisherService for iOS]:https://bitbucket.org/mngcorp/mngads-demo-ios/wiki/AmazonPublisherService
[AmazonPublisherService for Android]:https://bitbucket.org/mngcorp/mngads-demo-android/wiki/AmazonPublisherService
[Android Google Mobile Ads Adapter]:https://bitbucket.org/mngcorp/mngads-demo-android/wiki/dfp-adapter-android
[iOS Google Mobile Ads Adapter]:https://bitbucket.org/mngcorp/mngads-demo-ios/wiki/dfp-adapter-ios