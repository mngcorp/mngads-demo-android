package com.example.mngadsdemo

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.mngadsdemo.DemoApp.Companion.isAdClicked
import com.example.mngadsdemo.global.Constants
import com.example.mngadsdemo.global.MNG
import com.example.mngadsdemo.utils.Utils
import com.mngads.sdk.listener.MNGNativeAdListener
import com.mngads.sdk.nativead.MNGNativeAd
import com.mngads.sdk.nativead.MNGNativeAdActivity
import com.mngads.views.MAdvertiseNativeContainer

class MNGAppsFireNativeAdActivity : BaseActivity() {

    private var mAdView: View? = null
    private var mNativeAdContainer: MAdvertiseNativeContainer? = null
    private var mNative: MNGNativeAd? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val inflater = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val contentView = inflater.inflate(R.layout.native_ad, null)
        setContentView(contentView)
        mNative = MNGNativeAd(this, MNG.APPS_FIRE_PUBLISHER_ID)
        mNative!!.loadAd()
        mNativeAdContainer = contentView.findViewById(R.id.rlNativeAdContainer)
        mAdView = inflater.inflate(R.layout.ad_unit, mNativeAdContainer)
        mNative!!.nativeAdListener = object : MNGNativeAdListener {
            override fun onAdLoaded(mngNativeAd: MNGNativeAd) {
                displayNativeAd(mngNativeAd, mAdView)
            }

            override fun onError(mngNativeAd: MNGNativeAd, e: Exception) {
                Log.d(TAG, "native: onEfror " + e.message)
            }

            override fun onAdClicked(ad: MNGNativeAd) {
                Log.d(TAG, "native: clicked")
                isAdClicked = true
            }
        }
    }

    private fun displayNativeAd(nativeObject: MNGNativeAd, adView: View?) {

        // Create native UI using the native ad .
        val nativeAdIcon = adView?.findViewById<ImageView>(R.id.nativeAdIcon)
        val nativeAdTitle = adView?.findViewById<TextView>(R.id.nativeAdTitle)
        val nativeAdBody = adView?.findViewById<TextView>(R.id.nativeAdBody)
        val nativeAdCallToAction = adView?.findViewById<Button>(R.id.nativeAdCallToAction)
        val nativeBadgeImage = adView?.findViewById<ImageView>(R.id.badgeView)
        val mediaContainer = adView!!.findViewById<ViewGroup>(R.id.mediaContainer)
        val callToAction = nativeObject.callToActionButtonText

        // setting ad meta data
        nativeAdCallToAction?.text = callToAction
        nativeAdTitle?.text = nativeObject.title
        nativeAdBody?.text = nativeObject.description
        nativeBadgeImage?.setImageBitmap(nativeObject.badge)

        //  displaying icon image from url
        if (nativeObject.iconURL != null && nativeObject.iconURL.isNotEmpty() && nativeAdIcon != null) {
            Glide.with(this).load(nativeObject.iconURL).into(nativeAdIcon)
        }
        nativeObject.setMediaContainer(mediaContainer)
        val params = RelativeLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            Utils.convertDpToPixel(
                Constants.SIZE_NATIVE_HEIGHT.toFloat(),
                applicationContext
            ).toInt()
        )
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
        mNativeAdContainer!!.layoutParams = params
        nativeObject.registerViewForInteraction(nativeAdCallToAction)
    }

    override fun onDestroy() {
        mNative?.destroy()
        mNative = null
        super.onDestroy()
    }

    companion object {
        private val TAG = MNGNativeAdActivity::class.java.simpleName
    }
}