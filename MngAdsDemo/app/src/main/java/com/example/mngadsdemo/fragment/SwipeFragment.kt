package com.example.mngadsdemo.fragment

import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.mngadsdemo.R
import com.example.mngadsdemo.adapter.MAdvertisePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout

class SwipeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_swipe, container, false)
        val pagerAdapter = MAdvertisePagerAdapter(requireActivity().supportFragmentManager, 5, arrayOf(2))
        val viewPager: ViewPager = view.findViewById(R.id.pager)
        viewPager.adapter = pagerAdapter
        val tabLayout: TabLayout = view.findViewById(R.id.sliding_tabs)
        tabLayout.setupWithViewPager(viewPager)
        return view
    }
}