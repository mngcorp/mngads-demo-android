package com.example.mngadsdemo.utils

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.widget.RelativeLayout
import com.example.mngadsdemo.global.Constants

class ScaleLayout : RelativeLayout {
    private var scale = Constants.SCALE_TYPE_BIG

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?) : super(context)

    fun setScaleBoth(scale: Float) {
        this.scale = scale
        this.invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        val w = this.width
        val h = this.height
        canvas.scale(1f, scale, (w / 2).toFloat(), (h / 2).toFloat())
        super.onDraw(canvas)
    }
}