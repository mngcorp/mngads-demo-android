package com.example.mngadsdemo.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mngadsdemo.BaseActivity;
import com.example.mngadsdemo.DemoApp;
import com.example.mngadsdemo.R;
import com.example.mngadsdemo.adapter.ListAdapter;
import com.example.mngadsdemo.global.Constants;
import com.example.mngadsdemo.listenter.ActionBarListener;
import com.example.mngadsdemo.utils.Utils;
import com.mngads.MNGAdsFactory;
import com.mngads.MNGNativeObject;
import com.mngads.exceptions.MAdvertiseException;
import com.mngads.listener.MNGBannerListener;
import com.mngads.listener.MNGClickListener;
import com.mngads.listener.MNGInterstitialListener;
import com.mngads.listener.MNGNativeListener;
import com.mngads.util.MNGFrame;
import com.mngads.util.MNGPreference;
import com.mngads.views.MAdvertiseNativeContainer;


public class AdsFragment extends Fragment implements MNGBannerListener, MNGInterstitialListener, MNGNativeListener, ActionBarListener, OnClickListener, MNGClickListener {

    private MNGAdsFactory mngAdsBannerAdsFactory;
    private MNGAdsFactory mngAdsNativeAdsFactory;
    private MNGAdsFactory mngAdsParallaxAdsFactory;
    private MNGAdsFactory mngAdsInterstitialFactory;
    private MNGBannerListener mngParallaxBannerListener;
    private RelativeLayout bannerContainer;
    private MAdvertiseNativeContainer nativeContainer;
    private ListView listNonsense;
    private ListAdapter parallaxListAdapter;
    private ProgressBar progressBarWebService;
    private int screenWidth = 0;
    private MNGPreference mngPreference;


    private final String TAG = AdsFragment.class.getSimpleName();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View _View = inflater.inflate(R.layout.ads, container, false);

        initView(_View);
        initParallaxBannerListener();
        initializeMNGAdsFactory();
        callWebService();
        displayInterstitial();

        return _View;
    }


    private void initView(View _View) {
        screenWidth = getResources().getDisplayMetrics().widthPixels;
        bannerContainer = _View.findViewById(R.id.bannerContainer);
        nativeContainer = _View.findViewById(R.id.nativeContainer);
        listNonsense = _View.findViewById(R.id.listNonsense);
        progressBarWebService = _View.findViewById(R.id.progressBar);
    }
    private void initParallaxBannerListener() {
        mngParallaxBannerListener = new MNGBannerListener() {
            @Override
            public void bannerDidLoad(View view, int preferredHeightDP) {
                Log.d(TAG, "Parallax banner did load preferred height is " + preferredHeightDP + " dp");
                parallaxListAdapter.setAdView(view);
                parallaxListAdapter.notifyDataSetChanged();
            }

            @Override
            public void bannerDidFail(Exception e) {
                Log.e(TAG, "Parallax banner did fail :" + e.toString());
                Toast.makeText(getActivity().getApplicationContext(), "Parallax banner did fail", Toast.LENGTH_SHORT).show();


            }

            @Override
            public void bannerResize(MNGFrame frame) {
                Log.d(TAG, "Parallax banner did resize w " + frame.getWidth() + " h " + frame.getHeight());
            }
        };


    }
    private void initializeMNGAdsFactory() {

        mngAdsBannerAdsFactory = new MNGAdsFactory(this.getActivity());
        mngAdsBannerAdsFactory.setPlacementId(DemoApp.getBannerPlacement(getContext()));
        mngAdsBannerAdsFactory.setBannerListener(this);
        mngAdsBannerAdsFactory.setClickListener(this);

        mngAdsNativeAdsFactory = new MNGAdsFactory(this.getActivity());
        mngAdsNativeAdsFactory.setPlacementId(DemoApp.getNativePlacement(getContext()));
        mngAdsNativeAdsFactory.setNativeListener(this);
        mngAdsNativeAdsFactory.setClickListener(this);

        mngAdsParallaxAdsFactory = new MNGAdsFactory(this.getActivity());
        mngAdsParallaxAdsFactory.setPlacementId(DemoApp.getParallaxPlacement(getContext()));
        mngAdsParallaxAdsFactory.setBannerListener(mngParallaxBannerListener);
        mngAdsParallaxAdsFactory.setClickListener(this);

        mngAdsInterstitialFactory = new MNGAdsFactory(this.getActivity());
        mngAdsInterstitialFactory.setPlacementId(DemoApp.getInterPlacement(getContext()));
        mngAdsInterstitialFactory.setInterstitialListener(this);
        mngAdsInterstitialFactory.setClickListener(this);

        mngPreference = Utils.getPreference(getActivity());


    }




    private void callWebService() {

        listNonsense.postDelayed(new Runnable() {
            @Override
            public void run() {

                onDataReceived();


            }
        }, Constants.WS_DELAY);

    }

    private void onDataReceived() {

        progressBarWebService.setVisibility(View.GONE);

        parallaxListAdapter = new ListAdapter(15, getActivity(),7);

        listNonsense.setAdapter(parallaxListAdapter);

        parallaxListAdapter.notifyDataSetChanged();

        displayParallax();

    }




    private void displayBanner() {

        if (mngAdsBannerAdsFactory != null) {

            if (MNGAdsFactory.getNumberOfRunningFactory() < Constants.MAX_RUNNING_FACTORY) {


                mngAdsBannerAdsFactory.loadBanner(new MNGFrame(
                        (int) Utils.convertPixelsToDp(screenWidth,
                                getActivity().getApplicationContext()), Constants.BANNER_50_HEIGHT));

            } else {


                Log.d(TAG, "NumberOfRunningFactory >= MAX_RUNNING_FACTORY");

            }

            Toast.makeText(getActivity(), "Number of running factory is :" + MNGAdsFactory.getNumberOfRunningFactory(), Toast.LENGTH_SHORT).show();

            Log.d(TAG, "Number of running factory is :" + MNGAdsFactory.getNumberOfRunningFactory());
        }

    }

    private void displayInterstitial() {

        ((BaseActivity) getActivity()).shouldIgnoreResume();

        mngAdsInterstitialFactory.loadInterstitial(mngPreference);

        Toast.makeText(getActivity(), "Number of running factory is :" + MNGAdsFactory.getNumberOfRunningFactory(), Toast.LENGTH_SHORT).show();

        Log.d(TAG, "Number of running factory is :" + MNGAdsFactory.getNumberOfRunningFactory());


    }


    private void displayNative() {


        if (MNGAdsFactory.getNumberOfRunningFactory() < Constants.MAX_RUNNING_FACTORY) {


            mngAdsNativeAdsFactory.loadNative(mngPreference);

        } else {

            Log.d(TAG, "NumberOfRunningFactory >= MAX_RUNNING_FACTORY");
        }

        Toast.makeText(getActivity(), "Number of running factory is :" + MNGAdsFactory.getNumberOfRunningFactory(), Toast.LENGTH_SHORT).show();

        Log.d(TAG, "Number of running factory is :" + MNGAdsFactory.getNumberOfRunningFactory());

    }


    private void displayParallax() {

        if (mngAdsParallaxAdsFactory != null) {

            if (MNGAdsFactory.getNumberOfRunningFactory() < Constants.MAX_RUNNING_FACTORY) {

                mngAdsParallaxAdsFactory.loadBanner(new MNGFrame(
                        (int) Utils.convertPixelsToDp(screenWidth,
                                getActivity().getApplicationContext()), Constants.BANNER_100_HEIGHT), mngPreference);

            } else {

                Log.d(TAG, "NumberOfRunningFactory >= MAX_RUNNING_FACTORY");

            }

            Toast.makeText(getActivity(), "Number of running factory is :" + MNGAdsFactory.getNumberOfRunningFactory(), Toast.LENGTH_SHORT).show();

            Log.d(TAG, "Number of running factory is :" + MNGAdsFactory.getNumberOfRunningFactory());

        }
    }

    @Override
    public void bannerDidLoad(View view, int preferredHeightDP) {

        bannerContainer.setVisibility(View.VISIBLE);

        bannerContainer.removeAllViews();

        bannerContainer.addView(view);

        Log.d(TAG, "Banner did load preferred height is " + preferredHeightDP + " dp");

        Toast.makeText(getActivity().getApplicationContext(), "Banner did load", Toast.LENGTH_SHORT).show();

        displayNative();

    }

    @Override
    public void bannerDidFail(Exception e) {

        MAdvertiseException adException = (MAdvertiseException) e;

        Log.e(TAG, "Banner did fail : " + adException.getMessage() + " error code = " + adException.getErrorCode());


        Toast.makeText(getActivity().getApplicationContext(), "Banner did fail", Toast.LENGTH_SHORT).show();

        displayNative();
    }

    @Override
    public void bannerResize(MNGFrame frame) {

        Log.d(TAG, "Banner did resize w " + frame.getWidth() + " h " + frame.getHeight());

    }


    @Override
    public void interstitialDidLoad() {


        Log.d(TAG, "interstitial did load");

        Toast.makeText(getActivity().getApplicationContext(), "interstitial did load", Toast.LENGTH_SHORT).show();

        displayBanner();


    }

    @Override
    public void interstitialDidFail(Exception e) {

        MAdvertiseException adException = (MAdvertiseException) e;

        Log.e(TAG, "Interstitial did fail : " + adException.getMessage() + " error code " + adException.getErrorCode());

        Toast.makeText(getActivity().getApplicationContext(), "interstitial did fail", Toast.LENGTH_SHORT).show();

        displayBanner();
    }

    @Override
    public void interstitialDisappear() {

        Log.d(TAG, "interstitial did disappear");

        Toast.makeText(getActivity().getApplicationContext(), "interstitial did disappear", Toast.LENGTH_SHORT).show();
    }



    @Override
    public void interstitialDidShown() {
        Toast.makeText(getActivity(), "Interstitial did shown", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void nativeObjectDidLoad(MNGNativeObject mngNativeObject) {


        Log.d(TAG, "nativeObject did load");

        Toast.makeText(getActivity().getApplicationContext(), "nativeObject did load", Toast.LENGTH_SHORT).show();

        createNativeAdView(mngNativeObject);

    }


    @Override
    public void nativeObjectDidFail(Exception e) {

        MAdvertiseException adException = (MAdvertiseException) e;

        Log.e(TAG, "native did fail : " + adException.getMessage() + " error code " + adException.getErrorCode());

        Toast.makeText(getActivity().getApplicationContext(), "nativeObject did fail", Toast.LENGTH_SHORT).show();

    }


    @Override
    public void onDestroy() {

        if (mngAdsBannerAdsFactory != null) {
            mngAdsBannerAdsFactory.releaseMemory();
            mngAdsBannerAdsFactory = null;
        }


        if (mngAdsNativeAdsFactory != null) {
            mngAdsNativeAdsFactory.releaseMemory();
            mngAdsNativeAdsFactory = null;
        }

        if (mngAdsParallaxAdsFactory != null) {
            mngAdsParallaxAdsFactory.releaseMemory();
            mngAdsParallaxAdsFactory = null;
        }


        if (mngAdsInterstitialFactory != null) {
            mngAdsInterstitialFactory.releaseMemory();
            mngAdsInterstitialFactory = null;
        }


        super.onDestroy();
    }


    private void createNativeAdView(final MNGNativeObject nativeObject) {
        ImageView nativeIcon =  nativeContainer.findViewById(R.id.nativeIcon);
        ImageView nativeBadge =  nativeContainer.findViewById(R.id.nativeBadge);
        final Button nativeAdCallToAction = nativeContainer.findViewById(R.id.nativeAdCallToAction);
        TextView nativeTitle =  nativeContainer.findViewById(R.id.nativeTitle);
        TextView nativeBody =  nativeContainer.findViewById(R.id.nativeBody);
        nativeAdCallToAction.setText(nativeObject.getCallToAction());
        nativeBadge.setImageBitmap(nativeObject.getBadge());
        nativeTitle.setText(nativeObject.getTitle());
        nativeBody.setText(nativeObject.getBody());
        nativeContainer.setVisibility(View.VISIBLE);
        ViewGroup mediaContainer = nativeContainer.findViewById(R.id.native_media_container);
        nativeObject.registerViewForInteraction(nativeContainer, mediaContainer, nativeIcon, nativeAdCallToAction);
    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public void switchParams(boolean isOn) {

    }

    @Override
    public void onAdClicked() {

        DemoApp.setAdClicked(true);

        Log.d(TAG, "Ad Clicked");

        Toast.makeText(getActivity(), "Ad Clicked", Toast.LENGTH_SHORT).show();

    }


}