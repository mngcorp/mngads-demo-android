package com.example.mngadsdemo.fragment

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.mngadsdemo.DemoApp
import com.example.mngadsdemo.R
import com.example.mngadsdemo.databinding.FragmentDfpBinding
import com.example.mngadsdemo.global.Constants
import com.google.android.gms.ads.*
import com.google.android.gms.ads.admanager.AdManagerAdRequest
import com.google.android.gms.ads.admanager.AdManagerAdView
import com.google.android.gms.ads.admanager.AdManagerInterstitialAd
import com.google.android.gms.ads.admanager.AdManagerInterstitialAdLoadCallback
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdOptions
import com.google.android.gms.ads.nativead.NativeAdView
import com.google.android.gms.ads.rewarded.RewardedAd
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback
import com.madvertise.GADBlueStackMediationAdapter
import com.mngads.views.MAdvertiseNativeContainer

class DFPFragment : Fragment(), View.OnClickListener {

    private lateinit var mPublisherAdView: AdManagerAdView
    private lateinit var mPublisherAdViewSquare: AdManagerAdView
    private lateinit var mPublisherInterstitialAd: AdManagerInterstitialAd
    private lateinit var mShowButton: Button
    private lateinit var mProgressBar: View
    private lateinit var mMAdvertiseNativeContainer: MAdvertiseNativeContainer
    private lateinit var mAdView: NativeAdView
    private var mRewardedAd: RewardedAd? = null

    private var mIsInterstitialLoaded = false
    private var isRewardedVideo = false
    private lateinit var binding: FragmentDfpBinding

    private val extrasData: Bundle
        get() {
            return Bundle().apply {
                putString("customTargeting", Constants.MNGADS_KEYWORD)
                putString("keywords", Constants.MNGADS_KEYWORD2)
            }
        }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_dfp, container, false)
        binding = FragmentDfpBinding.bind(view)

        initViews()
        initListener()
        return view
    }

    private fun initListener() {
        binding.masButtonInterstitial.setOnClickListener(this)
        binding.masButtonNativeAd.setOnClickListener(this)
        binding.masButtonBanner.setOnClickListener(this)
        binding.masButtonSquare.setOnClickListener(this)
        binding.dfpRewardedBtn.setOnClickListener(this)
        binding.masButtonShow.setOnClickListener(this)
    }

    private fun initViews() {
        mMAdvertiseNativeContainer = binding.nativeContainer
        mPublisherAdView = binding.publisherView
        mPublisherAdViewSquare = binding.publisherViewSquare
        mProgressBar = binding.masProgressBar
        mShowButton = binding.masButtonShow
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.mas_button_interstitial -> loadInterstitial()
            R.id.mas_button_banner -> loadBanner()
            R.id.mas_button_square -> loadSquare()
            R.id.mas_button_nativeAd -> loadNativeAd()
            R.id.dfpRewardedBtn -> loadRewardedAd()
            R.id.mas_button_show -> if (isRewardedVideo) displayRewarded() else displayInterstitial()
            else -> {}
        }
    }

    private fun loadRewardedAd() {
        mMAdvertiseNativeContainer.removeAllViews()
        mShowButton.visibility = View.GONE
        mPublisherAdViewSquare.visibility = View.GONE
        mProgressBar.visibility = View.VISIBLE
        activity?.let {
            val adRequestBuilder = AdManagerAdRequest.Builder()
            adRequestBuilder.addNetworkExtrasBundle(
                GADBlueStackMediationAdapter::class.java,
                extrasData
            )
            RewardedAd.load(it, Constants.DFP_REWARDED_AD_UNIT,
                adRequestBuilder.build(), object : RewardedAdLoadCallback() {
                    override fun onAdLoaded(p0: RewardedAd) {
                        mRewardedAd = p0
                        isRewardedVideo = true
                        mShowButton.visibility = View.VISIBLE
                        mProgressBar.visibility = View.GONE
                        mRewardedAd?.fullScreenContentCallback = object : FullScreenContentCallback(){
                            override fun onAdFailedToShowFullScreenContent(adError: AdError) {
                                mShowButton.visibility = View.GONE
                                mProgressBar.visibility = View.GONE
                                Toast.makeText(
                                    activity,
                                    "Rewarded video failed",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }

                    override fun onAdFailedToLoad(p0: LoadAdError) {
                        mShowButton.visibility = View.GONE
                        mProgressBar.visibility = View.GONE
                        Toast.makeText(activity, "Rewarded video failed", Toast.LENGTH_SHORT).show()
                    }
                }

            )
        }
    }

    private fun displayRewarded() {
        mRewardedAd?.let {  it.show(requireActivity()) {} }
        mShowButton.visibility = View.GONE
    }

    private fun loadInterstitial() {
        mMAdvertiseNativeContainer.removeAllViews()
        mPublisherAdView.visibility = View.GONE
        mPublisherAdViewSquare.visibility = View.GONE
        mProgressBar.visibility = View.VISIBLE
        activity?.let {
            val adRequestBuilder = AdManagerAdRequest.Builder()
            adRequestBuilder.addCustomTargeting("age", "25")
            adRequestBuilder.addNetworkExtrasBundle(
                GADBlueStackMediationAdapter::class.java,
                extrasData
            )
            AdManagerInterstitialAd.load(
                it, Constants.DFP_INTER_AD_UNIT,
                adRequestBuilder.build(), object : AdManagerInterstitialAdLoadCallback() {
                    override fun onAdLoaded(interstitialAd: AdManagerInterstitialAd) {
                        mIsInterstitialLoaded = true
                        isRewardedVideo = false
                        mPublisherInterstitialAd = interstitialAd
                        mShowButton.visibility = View.VISIBLE
                        mProgressBar.visibility = View.GONE
                        mPublisherInterstitialAd.fullScreenContentCallback =
                            object : FullScreenContentCallback() {
                                override fun onAdDismissedFullScreenContent() {
                                    mIsInterstitialLoaded = false
                                }

                                override fun onAdFailedToShowFullScreenContent(adError: AdError) {
                                    mShowButton.visibility = View.GONE
                                    mProgressBar.visibility = View.GONE
                                    Toast.makeText(
                                        activity,
                                        "Interstitial failed",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }

                                override fun onAdShowedFullScreenContent() {
                                    mIsInterstitialLoaded = true
                                }
                            }
                    }

                    override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                        mShowButton.visibility = View.GONE
                        mProgressBar.visibility = View.GONE
                        Toast.makeText(activity, "Interstitial failed", Toast.LENGTH_SHORT).show()
                    }
                })
        }
    }


    private fun displayInterstitial() {
        if (mIsInterstitialLoaded) {
            mPublisherInterstitialAd.show(requireActivity())
            mShowButton.visibility = View.GONE
        }
    }

    private fun loadBanner() {
        mMAdvertiseNativeContainer.removeAllViews()
        mShowButton.visibility = View.GONE
        mPublisherAdViewSquare.visibility = View.GONE
        mProgressBar.visibility = View.VISIBLE
        val adRequestBuilder = AdManagerAdRequest.Builder().apply {
            addNetworkExtrasBundle(
                GADBlueStackMediationAdapter::class.java,
                extrasData
            )
        }

        mPublisherAdView.setAdSizes(AdSize.BANNER)
        mPublisherAdView.adListener = object : AdListener() {
            override fun onAdLoaded() {
                DemoApp.isAdClicked = true
                mProgressBar.visibility = View.GONE
                mPublisherAdView.visibility = View.VISIBLE
            }

            override fun onAdFailedToLoad(errorCode: LoadAdError) {
                mProgressBar.visibility = View.GONE
                Toast.makeText(activity, "Banner failed", Toast.LENGTH_SHORT).show()
            }

            override fun onAdClicked() {
                Toast.makeText(activity, "Ad Clicked", Toast.LENGTH_SHORT).show()
                DemoApp.isAdClicked = true
            }
        }

        mPublisherAdView.loadAd(adRequestBuilder.build())
    }

    private fun loadSquare() {
        mMAdvertiseNativeContainer.removeAllViews()
        mShowButton.visibility = View.GONE
        mPublisherAdView.visibility = View.GONE
        mProgressBar.visibility = View.VISIBLE
        val adRequestBuilder = AdManagerAdRequest.Builder().apply {
            addNetworkExtrasBundle(
                GADBlueStackMediationAdapter::class.java,
                extrasData
            )
        }
        mPublisherAdViewSquare.setAdSizes(AdSize.MEDIUM_RECTANGLE)
        mPublisherAdViewSquare.adListener = object : AdListener() {
            override fun onAdLoaded() {
                DemoApp.isAdClicked = true
                mProgressBar.visibility = View.GONE
                mPublisherAdViewSquare.visibility = View.VISIBLE
            }

            override fun onAdFailedToLoad(errorCode: LoadAdError) {
                mProgressBar.visibility = View.GONE
                Toast.makeText(activity, "Square failed", Toast.LENGTH_SHORT).show()
            }

            override fun onAdClicked() {
                DemoApp.isAdClicked = true
                Toast.makeText(activity, "Ad Clicked", Toast.LENGTH_SHORT).show()
            }
        }

        mPublisherAdViewSquare.loadAd(adRequestBuilder.build())
    }

    private fun loadNativeAd() {
        mMAdvertiseNativeContainer.removeAllViews()
        mPublisherAdView.visibility = View.GONE
        mPublisherAdViewSquare.visibility = View.GONE
        mProgressBar.visibility = View.VISIBLE
        val adOptions = NativeAdOptions.Builder()
            .setReturnUrlsForImageAssets(true)
            .setVideoOptions(VideoOptions.Builder().setStartMuted(false).build())
            .build()

        activity?.let {
            val adLoader = AdLoader.Builder(it, Constants.DFP_NATIVE_AD_UNIT)
                .forNativeAd { nativeAd: NativeAd ->
                    DemoApp.isAdClicked = true
                    mProgressBar.visibility = View.GONE
                    mAdView = layoutInflater
                        .inflate(R.layout.ad_unit_dfp, null) as NativeAdView
                    populateUnifiedNativeAdView(nativeAd, mAdView)
                }
                .withAdListener(object : AdListener() {
                    override fun onAdFailedToLoad(errorCode: LoadAdError) {
                        mProgressBar.visibility = View.GONE
                        Toast.makeText(activity, "NativeAd failed", Toast.LENGTH_SHORT).show()
                    }

                    override fun onAdClicked() {
                        Toast.makeText(activity, "Ad Clicked", Toast.LENGTH_SHORT).show()
                        DemoApp.isAdClicked = true
                    }
                })
                .withNativeAdOptions(adOptions)
                .build()

            val adRequestBuilder = AdManagerAdRequest.Builder().apply {
                addNetworkExtrasBundle(
                    GADBlueStackMediationAdapter::class.java,
                    extrasData
                )
            }

            adLoader.loadAd(adRequestBuilder.build())
        }
    }

    private fun populateUnifiedNativeAdView(nativeAd: NativeAd, nativeAdView: NativeAdView) {
        val nativeAdTitle = nativeAdView.findViewById<TextView>(R.id.nativeAdTitle)
        val nativeAdContext = nativeAdView.findViewById<TextView>(R.id.nativeAdContext)
        val nativeAdCallToAction = nativeAdView.findViewById<TextView>(R.id.nativeAdCallToAction)
        nativeAdTitle.text = nativeAd.headline
        nativeAdContext.text = nativeAd.body
        nativeAdCallToAction.text = nativeAd.callToAction
        nativeAdView.callToActionView = nativeAdCallToAction
        nativeAdView.headlineView = nativeAdTitle
        nativeAdView.bodyView = nativeAdContext
        nativeAd.icon?.let {
            if (it.uri != null && it.uri!!.toString().isNotEmpty()) {
                Glide.with(this).load(it.uri).into(nativeAdView.findViewById(R.id.nativeAdIcon))
            } else {
                nativeAdView.iconView = nativeAdView.findViewById(R.id.nativeAdIcon)
            }
        } ?: kotlin.run {
            nativeAdView.iconView = nativeAdView.findViewById(R.id.nativeAdIcon)
        }

        if (nativeAd.images != null) {
            if (nativeAd.images.size > 0 && nativeAd.images[0] != null) {
                Glide.with(this).load(nativeAd.images[0].uri).into(nativeAdView.findViewById(R.id.nativeAdImage))
            } else {
                nativeAdView.imageView = nativeAdView.findViewById(R.id.mediaContainer)
            }
        } else {
            nativeAdView.imageView = nativeAdView.findViewById(R.id.mediaContainer)
        }
        nativeAdView.storeView = mMAdvertiseNativeContainer
        val badgeView = nativeAdView.findViewById<ImageView>(R.id.badgeView)
        if (nativeAd.extras.getParcelable<Parcelable?>(Constants.AD_CHOICES_VIEW_KEY) != null) badgeView.setImageBitmap(
            nativeAd.extras.getParcelable(
                Constants.AD_CHOICES_VIEW_KEY
            )
        )
        nativeAdView.setNativeAd(nativeAd)
        mMAdvertiseNativeContainer.removeAllViews()
        mMAdvertiseNativeContainer.addView(nativeAdView)
    }
}