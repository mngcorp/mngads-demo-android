package com.example.mngadsdemo.fragment;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mngadsdemo.BaseActivity;
import com.example.mngadsdemo.R;
import com.example.mngadsdemo.global.Constants;
import com.example.mngadsdemo.utils.Utils;
import com.mngads.MNGAdsFactory;
import com.mngads.MNGNativeObject;
import com.mngads.listener.MNGBannerListener;
import com.mngads.listener.MNGClickListener;
import com.mngads.listener.MNGInterstitialListener;
import com.mngads.listener.MNGNativeListener;
import com.mngads.listener.MNGRefreshListener;
import com.mngads.util.MNGFrame;
import com.mngads.util.MNGPreference;
import com.mngads.views.MAdvertiseNativeContainer;

public class AppsFireFragment extends Fragment implements View.OnClickListener, MNGClickListener, MNGRefreshListener {

    private static final String TAG = AppsFireFragment.class.getSimpleName();

    public static final String BANNER_ID = "/5180317/banneraf";
    public static final String SQUARE_ID = "/5180317/squareaf";
    public static final String NATIVE_ID = "/5180317/nativeaf";
    public static final String INTER_ID = "/5180317/interaf";

    private Button mShowButton;
    private MAdvertiseNativeContainer mAdContainer;
    private MNGAdsFactory mFactory;
    private MNGPreference mMngPreference;

    private MNGFrame mFrame;
    private View mProgressBar;
    private View mNativeAdView;
    private LayoutInflater mInflater;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mas, container, false);
        mInflater = LayoutInflater.from(getContext());
        initViews(view);
        initAds();
        return view;
    }

    private void initViews(View view) {
        (view.findViewById(R.id.mas_button_interstitial)).setOnClickListener(this);
        (view.findViewById(R.id.mas_button_square)).setOnClickListener(this);
        (view.findViewById(R.id.mas_button_nativeAd)).setOnClickListener(this);
        (view.findViewById(R.id.mas_button_banner)).setOnClickListener(this);

        mProgressBar = view.findViewById(R.id.masProgressBar);

        mShowButton = view.findViewById(R.id.mas_button_show);
        mShowButton.setOnClickListener(this);
        mAdContainer = view.findViewById(R.id.mas_ad_container);
    }

    private void initAds() {
        mMngPreference = Utils.getPreference(getActivity());
        mFactory = new MNGAdsFactory(getActivity());
        // set banner listener
        mFactory.setBannerListener(getBannerListener());
        //set Ad click listener
        mFactory.setClickListener(this);
        //set Ad refresh listener
        mFactory.setRefreshListener(this);
        mFactory.setInterstitialListener(getInterstitialListener());
        mFactory.setNativeListener(getNativeAdListener());
    }

    private MNGNativeListener getNativeAdListener() {
        return new MNGNativeListener() {
            @Override
            public void nativeObjectDidLoad(MNGNativeObject mngNativeObject) {
                Log.d(TAG, "Native ad did load ");
                mProgressBar.setVisibility(View.GONE);
                mNativeAdView = mInflater.inflate(R.layout.ad_unit, mAdContainer);
                mAdContainer.setVisibility(View.VISIBLE);
                displayNativeAd(mngNativeObject, mNativeAdView, getContext());
            }

            @Override
            public void nativeObjectDidFail(Exception e) {
                mProgressBar.setVisibility(View.GONE);
                Log.d(TAG, "nativeObjectDidFail called with: e = [" + e + "]");
                Log.d(TAG, "Native ad failed ");

            }
        };
    }

    private MNGInterstitialListener getInterstitialListener() {
        return new MNGInterstitialListener() {
            @Override
            public void interstitialDidLoad() {
                Toast.makeText(getContext(), "interstitial did load", Toast.LENGTH_SHORT).show();
                mProgressBar.setVisibility(View.GONE);
                mShowButton.setVisibility(View.VISIBLE);
            }

            @Override
            public void interstitialDidFail(Exception e) {
                Log.d(TAG, "interstitialDidFail: exception = [" + e + "]");
                Toast.makeText(getContext(), "interstitial did fail", Toast.LENGTH_SHORT).show();
                mProgressBar.setVisibility(View.GONE);
                mShowButton.setVisibility(View.GONE);
            }

            @Override
            public void interstitialDisappear() {

                Toast.makeText(getContext(), "interstitial disappeared", Toast.LENGTH_SHORT).show();
                Toast.makeText(getContext(), "interstitial did load", Toast.LENGTH_SHORT).show();
                mProgressBar.setVisibility(View.GONE);
                mShowButton.setVisibility(View.GONE);
            }

            @Override
            public void interstitialDidShown() {
                Toast.makeText(getContext(), "Interstitial did shown", Toast.LENGTH_SHORT).show();

            }
        };
    }

    private MNGBannerListener getBannerListener() {
        return new MNGBannerListener() {
            @Override
            public void bannerDidLoad(View view, int preferredHeightDP) {
                mProgressBar.setVisibility(View.GONE);
                mAdContainer.setVisibility(View.VISIBLE);
                mAdContainer.addView(view);
                Log.d(TAG, "Banner did load preferred Height " + preferredHeightDP + " dp");
                Context context = getActivity();
                if (context != null) {
                    Toast.makeText(getActivity().getApplicationContext(), "Banner did load", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void bannerDidFail(Exception e) {
                mProgressBar.setVisibility(View.GONE);
                Context context = getActivity();
                if (context != null) {
                    Toast.makeText(context, "Banner did fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void bannerResize(MNGFrame mngFrame) {
                Log.d(TAG, "Banner did resize w " + mngFrame.getWidth() + " h " + mngFrame.getHeight());
            }
        };
    }

    private void loadBanner() {
        if (mFactory.isBusy()) {
            return;
        }
        mProgressBar.setVisibility(View.VISIBLE);

        switch (mFrame.getHeight()) {

            case Constants.BANNER_50_HEIGHT:
                // init banner placement id
                mFactory.setPlacementId(BANNER_ID);
                mFactory.loadBanner(mFrame, mMngPreference);
                break;

            case Constants.BANNER_90_HEIGHT:
                // init banner placement id
                mFactory.setPlacementId(BANNER_ID);
                mFactory.loadBanner(mFrame, mMngPreference);
                break;

            case Constants.SQUARE_AD_HEIGHT:
                // init square placement id
                mFactory.setPlacementId(SQUARE_ID);
                mFactory.loadBanner(mFrame, mMngPreference);
                break;
        }
        Log.d(TAG, "Number of running factory is :" + MNGAdsFactory.getNumberOfRunningFactory());
    }

    private void loadInterstitial() {
        if (mFactory.isBusy()) {
            return;
        }
        mProgressBar.setVisibility(View.VISIBLE);
        mFactory.setPlacementId(INTER_ID);
        mFactory.loadInterstitial(mMngPreference, false);
    }

    private void displayNativeAd(MNGNativeObject nativeObject, View adView, Context context) {

        // Create native UI using the native ad .
        ImageView nativeAdIcon = adView.findViewById(R.id.nativeAdIcon);
        TextView nativeAdTitle = adView.findViewById(R.id.nativeAdTitle);
        TextView nativeAdBody = adView.findViewById(R.id.nativeAdBody);
        ImageView nativeBadgeImage = adView.findViewById(R.id.badgeView);
        Button nativeAdCallToAction = adView.findViewById(R.id.nativeAdCallToAction);
        ViewGroup mediaContainer = adView.findViewById(R.id.mediaContainer);

        String callToAction = nativeObject.getCallToAction();

        // setting ad meta data
        nativeAdCallToAction.setText(callToAction);
        nativeAdTitle.setText(nativeObject.getTitle());
        nativeAdBody.setText(nativeObject.getBody());
        nativeBadgeImage.setImageBitmap(nativeObject.getBadge(getActivity(), "Publicité"));

        switch (nativeObject.getDisplayType()) {

            case MNGDisplayTypeAppInstall:
                Drawable imageAppInstall = ContextCompat.getDrawable(context, R.drawable.app_install);
                setDisplayType(imageAppInstall, nativeAdCallToAction, context);
                break;

            case MNGDisplayTypeContent:
                Drawable imageContent = ContextCompat.getDrawable(context, R.drawable.content);
                setDisplayType(imageContent, nativeAdCallToAction, context);
                break;

            case MNGDisplayTypeUnknown:
                break;
        }
        nativeObject.registerViewForInteraction(mAdContainer, mediaContainer, nativeAdIcon, nativeAdCallToAction);
    }

    private void setDisplayType(Drawable drawable, Button nativeAdCallToAction, Context context) {
        int w = (int) Utils.convertDpToPixel(15, context);
        drawable.setBounds(0, 0, w, w);
        nativeAdCallToAction.setCompoundDrawables(drawable, null, null, null);
    }

    @Override
    public void onClick(View v) {
        mAdContainer.removeAllViews();
        mAdContainer.setVisibility(View.GONE);
        switch (v.getId()) {
            case R.id.mas_button_interstitial:
                loadInterstitial();
                break;

            case R.id.mas_button_square:
                mFrame = com.mngads.util.MNGAdSize.MNG_MEDIUM_RECTANGLE;
                loadBanner();
                break;

            case R.id.mas_button_banner:
                mFrame = getResources().getBoolean(R.bool.is_tablet)
                        ? com.mngads.util.MNGAdSize.MNG_DYNAMIC_LEADERBOARD
                        : com.mngads.util.MNGAdSize.MNG_DYNAMIC_BANNER;
                loadBanner();
                break;

            case R.id.mas_button_nativeAd:
                loadNativeAd();
                break;

            case R.id.mas_button_show:
                displayInterstitial();
                break;

            default:
                break;
        }
    }

    private void displayInterstitial() {
        if (mFactory.isInterstitialReady()) {
            ((BaseActivity) getActivity()).shouldIgnoreResume();
            mFactory.displayInterstitial();
        } else {
            Log.d(TAG, "Interstitial not ready ");
        }
    }

    private void loadNativeAd() {
        if (mFactory.isBusy()) {
            return;
        }
        mProgressBar.setVisibility(View.VISIBLE);
        mFactory.setPlacementId(NATIVE_ID);
        mFactory.loadNative(mMngPreference);
    }

    @Override
    public void onAdClicked() { }

    @Override
    public void onRefreshSucceed() { }

    @Override
    public void onRefreshFailed(Exception e) { }

    @Override
    public void onDestroy() {
        mFactory.releaseMemory();
        super.onDestroy();
    }

}