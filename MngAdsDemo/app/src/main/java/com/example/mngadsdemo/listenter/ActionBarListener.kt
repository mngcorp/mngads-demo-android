package com.example.mngadsdemo.listenter

interface ActionBarListener {
    fun switchParams(isOn: Boolean)
}