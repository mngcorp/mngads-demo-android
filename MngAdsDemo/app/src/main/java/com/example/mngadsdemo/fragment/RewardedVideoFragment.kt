package com.example.mngadsdemo.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.mngadsdemo.DemoApp
import com.example.mngadsdemo.R
import com.example.mngadsdemo.databinding.RewardedVideoBinding
import com.example.mngadsdemo.listenter.ActionBarListener
import com.example.mngadsdemo.utils.Utils
import com.mngads.MAdvertiseRewardedVideo
import com.mngads.MNGAdsFactory
import com.mngads.exceptions.MAdvertiseLockedPlacementException
import com.mngads.listener.MAdvertiseRewardedVideoListener
import com.mngads.models.MAdvertiseVideoReward
import com.mngads.util.MNGPreference

class RewardedVideoFragment : Fragment(), ActionBarListener, View.OnClickListener,
    MAdvertiseRewardedVideoListener {

    private val TAG = RewardedVideoFragment::class.java.simpleName

    private lateinit var rewardedVideo: MAdvertiseRewardedVideo
    private var mngPreference: MNGPreference? = null

    private lateinit var binding: RewardedVideoBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.rewarded_video, container, false)
        binding = RewardedVideoBinding.bind(view)

        rewardedVideo = MAdvertiseRewardedVideo(activity)
        rewardedVideo.setRewardedVideoListener(this)
        mngPreference = Utils.getPreference(requireActivity())

        binding.btDisplay.typeface = DemoApp.opensans_regular
        binding.rewardVideoBtn.setOnClickListener(this)
        binding.btDisplay.setOnClickListener(this)

        return view
    }

    override fun switchParams(isOn: Boolean) {
        binding.rlParamsContainer.visibility = if (!isOn) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rewardVideoBtn -> createRewardedVideo()
            R.id.btDisplay -> showRewardedVideo()
            else -> {}
        }
    }

    private fun createRewardedVideo() {

        binding.btDisplay.visibility = View.GONE

        // set Rewarded video placement id
        rewardedVideo.setPlacementId(DemoApp.getRewardedVideoPlacement(activity))
        rewardedVideo.loadRewardedVideo(mngPreference)
        binding.progressBar.visibility = View.VISIBLE
        Log.d(TAG, "Number of running factory is : ${MNGAdsFactory.getNumberOfRunningFactory()}")
    }

    private fun showRewardedVideo() {
        Log.d(TAG, rewardedVideo.isRewardedVideoReady.toString())
        if (!rewardedVideo.showRewardedVideo()) {
            Log.d(
                TAG,
                "showRewardedVideo: displaying a rewarded video is not possible, you have to call loadRewardedVideo or wait at least 5s before displaying another one."
            )
            Toast.makeText(
                activity,
                "Wait at least 5s before displaying another one",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onRewardedVideoError(e: Exception) {
        Log.d(TAG, "onRewardedVideoError $e")
        if (e is MAdvertiseLockedPlacementException) {
            Log.d(
                TAG,
                "onRewardedVideoError: this exception occurs when a rewarded video is already loaded"
            )
        }
        binding.progressBar.visibility = View.GONE
    }

    override fun onRewardedVideoLoaded() {
        Log.d(TAG, "onRewardedVideoLoaded")
        binding.rewardedVideoLog.text = ""
        binding.progressBar.visibility = View.GONE
        binding.btDisplay.visibility = View.VISIBLE
    }

    override fun onRewardedVideoClicked() {
        Log.d(TAG, "onRewardedVideoClicked")
        binding.progressBar.visibility = View.GONE
    }

    override fun onRewardedVideoClosed() {
        Log.d(TAG, "onRewardedVideoClosed")
        binding.progressBar.visibility = View.GONE
        binding.btDisplay.visibility = View.GONE
    }

    override fun onRewardedVideoAppeared() {
        Log.d(TAG, "onRewardedVideoAppeared")
        binding.progressBar.visibility = View.GONE
        binding.btDisplay.visibility = View.GONE
    }

    override fun onRewardedVideoCompleted(videoReward: MAdvertiseVideoReward?) {
        Log.d(TAG, "onRewardedVideoCompleted")
        binding.progressBar.visibility = View.GONE

        videoReward?.let {
            Log.d(
                TAG,
                "onVideoRewarded, type: ${videoReward.type}, amount: ${videoReward.amount}"
            )
            binding.rewardedVideoLog.text =
                ("Video rewarded \n Type: " + videoReward.type + " \n amount: " + videoReward.amount).trimIndent()
        } ?: run {
            Log.d(TAG, "onVideoRewarded with no reward object")
            binding.rewardedVideoLog.text = "Video reward event"
        }
    }


    override fun onDestroy() {
        rewardedVideo.releaseMemory()
        super.onDestroy()
    }
}