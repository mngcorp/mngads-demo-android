package com.example.mngadsdemo.adapter.holder;

import android.view.View;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.example.mngadsdemo.R;

public class ParallaxHolder extends RecyclerView.ViewHolder {
    private LinearLayout mContainerAds;

    public ParallaxHolder(View view) {
        super(view);
        mContainerAds = view.findViewById(R.id.container_ads);
    }

    public LinearLayout getContainerAds()
    {
        return mContainerAds;
    }

}