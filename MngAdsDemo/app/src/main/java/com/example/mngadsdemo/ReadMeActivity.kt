package com.example.mngadsdemo

import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.example.mngadsdemo.DemoApp.Companion.opensans_semiblod
import com.example.mngadsdemo.global.Constants

class ReadMeActivity : BaseActivity(), View.OnClickListener {

    private lateinit var mProgressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_readme)
        initializeActionBar()
        initializeView()
    }

    private fun initializeView() {
        mProgressBar = findViewById(R.id.progressBar)
        mProgressBar.indeterminateDrawable.setColorFilter(
            Color.parseColor("#45cdc9"),
            PorterDuff.Mode.MULTIPLY
        )
        val webView = findViewById<WebView>(R.id.webview)
        webView.settings.setSupportZoom(true)
        webView.settings.builtInZoomControls = true
        webView.settings.useWideViewPort = true
        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                mProgressBar.visibility = View.GONE
            }
        }
        webView.loadUrl(Constants.READ_ME_URL)
    }

    private fun initializeActionBar() {
        val toolbar = findViewById<Toolbar>(R.id.tool_bar)
        val tvTitle = toolbar.findViewById<TextView>(R.id.tvTitle)
        tvTitle.typeface = opensans_semiblod
        tvTitle.text = resources.getStringArray(R.array.ADS_CATEGORY)[17]
        findViewById<View>(R.id.ibBack).setOnClickListener(this)
        setSupportActionBar(toolbar)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.ibBack) {
            finish()
        }
    }

    override fun onBackPressed() {
        setResult(RESULT_OK)
        super.onBackPressed()
    }
}