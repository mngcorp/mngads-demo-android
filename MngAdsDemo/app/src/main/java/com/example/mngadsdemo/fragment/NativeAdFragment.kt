package com.example.mngadsdemo.fragment

import android.content.Context
import com.example.mngadsdemo.utils.Utils.getPreference
import com.example.mngadsdemo.DemoApp.Companion.getNativePlacement
import com.example.mngadsdemo.DemoApp.Companion.getNativePlacementNoCover
import com.example.mngadsdemo.utils.Utils.convertDpToPixel
import com.example.mngadsdemo.DemoApp.Companion.isAdClicked
import com.example.mngadsdemo.listenter.ActionBarListener
import com.mngads.listener.MNGNativeListener
import com.mngads.listener.MNGClickListener
import com.mngads.MNGAdsFactory
import com.mngads.views.MAdvertiseNativeContainer
import com.mngads.util.MNGPreference
import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import com.example.mngadsdemo.R
import com.mngads.MNGNativeObject
import androidx.fragment.app.Fragment
import com.example.mngadsdemo.global.Constants
import java.lang.Exception

class NativeAdFragment : Fragment(), ActionBarListener, MNGNativeListener, MNGClickListener {

    private var mngAdsNativeAdsFactory: MNGAdsFactory? = null
    private var nativeAdContainerWithCover: MAdvertiseNativeContainer? = null
    private var nativeAdWithCover: RelativeLayout? = null
    private var nativeAdWithOutCover: RelativeLayout? = null
    private var nativeAdContainerWithOutCover: MAdvertiseNativeContainer? = null
    private var adViewWithCover: View? = null
    private var adViewWithoutCover: View? = null
    private var mngPreference: MNGPreference? = null
    private var mSetCover = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.native_ad_cover, container, false)
        initView(view)
        initTypeShow(view)
        initNativeAds()
        adViewWithCover = inflater.inflate(R.layout.ad_unit, nativeAdContainerWithCover)
        adViewWithoutCover =
            inflater.inflate(R.layout.ad_unit_without_cover, nativeAdContainerWithOutCover)
        displayNativeAd()
        return view
    }

    private fun initView(view: View) {
        nativeAdContainerWithCover = view.findViewById(R.id.rlNativeAdContainer_with_cover)
        nativeAdWithCover = view.findViewById(R.id.rlAdContainer_with_cover)
        nativeAdContainerWithOutCover = view.findViewById(R.id.rlNativeAdContainer_without_cover)
        nativeAdWithOutCover = view.findViewById(R.id.rlAdContainer_without_cover)
        nativeAdContainerWithOutCover?.visibility = View.GONE
        nativeAdWithOutCover?.visibility = View.GONE
    }

    private fun initTypeShow(view: View) {
        view.findViewById<View>(R.id.rlnoSizeContainer).setOnClickListener {
            nativeAdWithCover!!.visibility = View.GONE
            nativeAdWithOutCover!!.visibility = View.VISIBLE
            mSetCover = false
            displayNativeAd()
        }
        view.findViewById<View>(R.id.rlWithSizeContainer).setOnClickListener {
            nativeAdWithOutCover!!.visibility = View.GONE
            nativeAdWithCover!!.visibility = View.VISIBLE
            mSetCover = true
            displayNativeAd()
        }
    }

    private fun initNativeAds() {
        mngAdsNativeAdsFactory = MNGAdsFactory(activity)
        mngAdsNativeAdsFactory!!.setNativeListener(this)
        mngAdsNativeAdsFactory!!.setClickListener(this)
        mngPreference = getPreference(requireActivity())
    }

    private fun displayNativeAd() {
        if (!mngAdsNativeAdsFactory!!.isBusy) {
            if (mSetCover) {
                mngAdsNativeAdsFactory!!.setPlacementId(getNativePlacement(context))
                mngAdsNativeAdsFactory!!.loadNative(mngPreference)
            } else {
                mngAdsNativeAdsFactory!!.setPlacementId(getNativePlacementNoCover(context))
                mngAdsNativeAdsFactory!!.loadNativeWithOutCover(mngPreference)
            }
        }
    }

    override fun switchParams(isOn: Boolean) {}

    override fun nativeObjectDidLoad(mngNativeObject: MNGNativeObject) {
        Log.e("MNG-Ads-SDK", "Native object did load")
        Toast.makeText(activity, "Native object did load", Toast.LENGTH_SHORT).show()
        if (mSetCover) {
            displayNativeAd(mngNativeObject, adViewWithCover, activity)
        } else {
            displayNativeAd(mngNativeObject, adViewWithoutCover, activity)
        }
    }

    override fun nativeObjectDidFail(e: Exception) {
        Toast.makeText(activity, "Native did fail", Toast.LENGTH_SHORT).show()
        Log.e("MNG-Ads-SDK", "Native did fail$e")
    }

    private fun displayNativeAd(nativeObject: MNGNativeObject?, adView: View?, context: Context?) {
        if (nativeObject != null) {
            val nativeAdCallToAction = adView!!.findViewById<Button>(R.id.nativeAdCallToAction)
            val nativeAdTitle = adView.findViewById<TextView>(R.id.nativeAdTitle)
            val nativeAdBody = adView.findViewById<TextView>(R.id.nativeAdBody)
            nativeAdTitle.text = nativeObject.title
            nativeAdBody.text = nativeObject.body
            nativeAdCallToAction.text = nativeObject.callToAction
            val nativeBadgeImage = adView.findViewById<ImageView>(R.id.badgeView)
            nativeBadgeImage.setImageBitmap(nativeObject.getBadge(activity, "Publicité"))
            initNativeObject(context, nativeAdCallToAction, adView, nativeObject)
        } else Log.e("MNG-Ads-SDK", "nativeObject Null")
    }

    private fun initNativeObject(
        context: Context?,
        nativeAdCallToAction: Button,
        adView: View?,
        nativeObject: MNGNativeObject
    ) {
        val nativeAdIcon = adView!!.findViewById<ImageView>(R.id.nativeAdIcon)
        if (mSetCover) {
            val mediaContainer = adView.findViewById<ViewGroup>(R.id.mediaContainer)
            val params = RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                convertDpToPixel(
                    Constants.SIZE_NATIVE_HEIGHT.toFloat(),
                    requireActivity()
                ).toInt()
            )
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
            nativeAdContainerWithCover!!.layoutParams = params
            nativeObject.registerViewForInteraction(
                nativeAdContainerWithCover,
                mediaContainer,
                nativeAdIcon,
                nativeAdCallToAction
            )
            nativeAdContainerWithCover!!.visibility = View.VISIBLE
        } else {
            val params = RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                convertDpToPixel(
                    Constants.SIZE_NATIVE_HEIGHT_WITH_OUT.toFloat(),
                    requireActivity()
                ).toInt()
            )
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
            nativeAdContainerWithOutCover!!.layoutParams = params
            nativeObject.registerViewForInteraction(
                nativeAdContainerWithOutCover,
                null,
                nativeAdIcon,
                nativeAdCallToAction
            )
            nativeAdContainerWithOutCover!!.visibility = View.VISIBLE
        }
    }

    override fun onDestroy() {
        mngAdsNativeAdsFactory?.releaseMemory()
        mngAdsNativeAdsFactory = null
        super.onDestroy()
    }

    override fun onAdClicked() {
        isAdClicked = true
        Toast.makeText(activity, "Ad Clicked", Toast.LENGTH_SHORT).show()
    }
}