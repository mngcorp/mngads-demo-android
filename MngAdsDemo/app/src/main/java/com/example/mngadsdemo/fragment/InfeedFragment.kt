package com.example.mngadsdemo.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.mngadsdemo.DemoApp
import com.example.mngadsdemo.R
import com.example.mngadsdemo.adapter.ListAdapter
import com.example.mngadsdemo.databinding.InfeedBinding
import com.example.mngadsdemo.utils.Utils
import com.mngads.MNGAdsFactory
import com.mngads.exceptions.MAdvertiseException
import com.mngads.listener.MNGClickListener
import com.mngads.listener.MNGInfeedListener
import com.mngads.listener.MNGRefreshListener
import com.mngads.util.MAdvertiseInfeedFrame
import com.mngads.util.MNGPreference

class InfeedFragment : Fragment(), MNGClickListener, MNGInfeedListener,
    MNGRefreshListener {

    private val TAG = InfeedFragment::class.java.simpleName

    private var mngAdsInfeedAdsFactory: MNGAdsFactory? = null

    private lateinit var listNonsense: ListView
    private lateinit var listAdapter: ListAdapter
    private lateinit var mngPreference: MNGPreference
    private lateinit var binding: InfeedBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.infeed, container, false)
        binding = InfeedBinding.bind(view)

        //init mng factory
        mngAdsInfeedAdsFactory = MNGAdsFactory(activity)
        // set banner listener
        mngAdsInfeedAdsFactory?.setInfeedListener(this)
        //set Ad click listener
        mngAdsInfeedAdsFactory?.setClickListener(this)
        //set Ad refresh listener
        mngAdsInfeedAdsFactory?.setRefreshListener(this)
        listNonsense = binding.listNonsense
        mngPreference = Utils.getPreference(requireActivity())
        createSimpleList()
        displayInfeed()
        return view
    }

    override fun onDestroy() {
        if (mngAdsInfeedAdsFactory != null) {
            mngAdsInfeedAdsFactory!!.releaseMemory()
            mngAdsInfeedAdsFactory = null
        }
        super.onDestroy()
    }

    private fun displayInfeed() {
        mngAdsInfeedAdsFactory?.setPlacementId(DemoApp.getInfeedPlacement(context))
        val screenWidth = resources.displayMetrics.widthPixels
        val widthDP = Utils.convertPixelsToDp(screenWidth.toFloat(), requireActivity()).toInt()
        mngAdsInfeedAdsFactory?.loadInfeed(
            MAdvertiseInfeedFrame(
                widthDP,
                MAdvertiseInfeedFrame.INFEED_RATIO_16_9
            ), mngPreference
        )
        Log.d(TAG, "Number of running factory is :" + MNGAdsFactory.getNumberOfRunningFactory())
    }

    override fun onAdClicked() {
        DemoApp.isAdClicked = true
        Log.d(TAG, "Ad Clicked")
        Toast.makeText(requireActivity(), "Ad Clicked", Toast.LENGTH_SHORT).show()
    }

    override fun infeedDidLoad(view: View, preferredHeightDP: Int) {
        listAdapter.setAdView(view)
        Log.d(TAG, "Infeed did load with preferred height: $preferredHeightDP")
        Toast.makeText(requireActivity(), "Infeed did load", Toast.LENGTH_SHORT).show()
    }

    override fun infeedDidFail(e: Exception) {
        val adException = e as MAdvertiseException
        Log.e(
            TAG,
            "Infeed did fail : ${adException.message} error code = ${adException.errorCode}"
        )
        Toast.makeText(requireActivity(), "Infeed did fail", Toast.LENGTH_SHORT).show()
    }

    override fun onRefreshSucceed() {
        Log.d(TAG, "on refresh succeed")
        Toast.makeText(activity, "on refresh succeed", Toast.LENGTH_SHORT).show()
    }

    override fun onRefreshFailed(e: Exception) {
        Log.d(TAG, "on refresh failed")
        Toast.makeText(activity, "on refresh failed $e", Toast.LENGTH_SHORT).show()
    }

    private fun createSimpleList() {
        listAdapter = ListAdapter(10, requireActivity())
        listNonsense.adapter = listAdapter
    }
}