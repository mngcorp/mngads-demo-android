package com.example.mngadsdemo.fragment

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.mngadsdemo.BaseActivity
import com.example.mngadsdemo.DemoApp.Companion.getInterOverlayPlacement
import com.example.mngadsdemo.DemoApp.Companion.getInterPlacement
import com.example.mngadsdemo.DemoApp.Companion.isAdClicked
import com.example.mngadsdemo.DemoApp.Companion.opensans_regular
import com.example.mngadsdemo.R
import com.example.mngadsdemo.fragment.BannerFragment.Companion.printError
import com.example.mngadsdemo.global.Constants.TIME_OUT_DELAY
import com.example.mngadsdemo.listenter.ActionBarListener
import com.example.mngadsdemo.utils.Utils.getPreference
import com.mngads.MNGAdsFactory
import com.mngads.listener.MNGClickListener
import com.mngads.listener.MNGInterstitialListener
import com.mngads.util.MNGPreference

class InterstitialFragment : Fragment(), ActionBarListener, View.OnClickListener,
    MNGInterstitialListener, MNGClickListener {

    private var mRlParamsContainer: RelativeLayout? = null
    private var mIbOverlayInterstitial: Button? = null
    private var mIbDefaultInterstitial: Button? = null
    private var mBtDisplay: Button? = null
    private var mProgressBar: ProgressBar? = null
    private var mTimeoutHandler: Handler? = null
    private var mRunnable: Runnable? = null
    private var mIsTimeOut = false
    private var mngAdsInterstitialAdsFactory: MNGAdsFactory? = null
    private var isInterstitialOverLay = false
    private var mngPreference: MNGPreference? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.interstitiel, container, false)
        initView(view)
        initStyle()
        initListener()
        initAds()
        return view
    }

    private fun initView(view: View) {
        mRlParamsContainer = view.findViewById(R.id.rlParamsContainer)
        mIbOverlayInterstitial = view.findViewById(R.id.ibOverlayInterstitial)
        mIbDefaultInterstitial = view.findViewById(R.id.ibDefaultInterstitial)
        mProgressBar = view.findViewById(R.id.progressBar)
        mBtDisplay = view.findViewById(R.id.btDisplay)
    }

    private fun initStyle() {
        mBtDisplay!!.typeface = opensans_regular
        mIbDefaultInterstitial!!.isSelected = true
    }

    private fun initListener() {
        mIbOverlayInterstitial!!.setOnClickListener(this)
        mIbDefaultInterstitial!!.setOnClickListener(this)
        mBtDisplay!!.setOnClickListener(this)
    }

    private fun initAds() {
        mngPreference = getPreference(requireActivity())
        mngAdsInterstitialAdsFactory = MNGAdsFactory(activity)
        mngAdsInterstitialAdsFactory!!.setInterstitialListener(this)
        mngAdsInterstitialAdsFactory!!.setTimeOutGlobal(3000)
        mngAdsInterstitialAdsFactory!!.setClickListener(this)
    }

    private fun createInterstitial() {
        initTimeoutRunnable()
        val placement =
            if (isInterstitialOverLay) getInterOverlayPlacement(context) else getInterPlacement(
                context
            )
        mBtDisplay!!.visibility = View.GONE
        mngAdsInterstitialAdsFactory!!.setPlacementId(placement)
        if (!isInterstitialOverLay) {
            mngAdsInterstitialAdsFactory!!.loadInterstitial(mngPreference, false)
        } else {
            mngAdsInterstitialAdsFactory!!.loadInterstitial(false)
        }
        mProgressBar!!.visibility = View.VISIBLE
    }

    override fun switchParams(isOn: Boolean) {
        if (!isOn) {
            mRlParamsContainer!!.visibility = View.VISIBLE
        } else {
            mRlParamsContainer!!.visibility = View.GONE
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ibOverlayInterstitial -> {
                isInterstitialOverLay = true
                createInterstitial()
            }
            R.id.ibDefaultInterstitial -> {
                isInterstitialOverLay = false
                createInterstitial()
            }
            R.id.btDisplay -> displayInterstitial()
            else -> {}
        }
    }

    private fun displayInterstitial() {
        Log.e("mIsTimeOut+", "$mIsTimeOut - displayInterstitial")
        if (!mIsTimeOut) {
            if (mngAdsInterstitialAdsFactory!!.isInterstitialReady) {
                (activity as BaseActivity?)!!.shouldIgnoreResume()
                mngAdsInterstitialAdsFactory!!.displayInterstitial()
                mBtDisplay!!.visibility = View.GONE
            } else {
                Toast.makeText(activity, "Interstitial not ready ", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(activity, "No Ad After $TIME_OUT_DELAY ms", Toast.LENGTH_SHORT).show()
        }
    }

    private fun initTimeoutRunnable() {
        mIsTimeOut = false
        Log.e("mIsTimeOut", "$mIsTimeOut - initTimeoutRunnable")
        mTimeoutHandler = Handler(Looper.getMainLooper())
        mRunnable = Runnable {
            mIsTimeOut = true
            Log.e("mIsTimeOut", "$mIsTimeOut - Runnable")
        }
        mTimeoutHandler!!.postDelayed(mRunnable!!, TIME_OUT_DELAY.toLong())
    }

    private fun cancelTimeOut() {
        if (mTimeoutHandler != null) {
            mTimeoutHandler!!.removeCallbacks(mRunnable!!)
            mTimeoutHandler = null
            mRunnable = null
        }
    }

    override fun interstitialDidLoad() {
        mProgressBar!!.visibility = View.GONE
        mBtDisplay!!.visibility = View.VISIBLE
        Toast.makeText(activity, "Interstitial did load", Toast.LENGTH_SHORT).show()
        cancelTimeOut()
        Log.e("mIsTimeOut", "$mIsTimeOut - interstitialDidLoad")
        if (!mIsTimeOut) {
            Toast.makeText(activity, "Ad load -> Ok < $TIME_OUT_DELAY ms", Toast.LENGTH_SHORT)
                .show()
        } else {
            Toast.makeText(activity, "No Ad After $TIME_OUT_DELAY ms", Toast.LENGTH_SHORT).show()
        }
    }

    override fun interstitialDidFail(adException: Exception) {
        mProgressBar!!.visibility = View.GONE
        mBtDisplay!!.visibility = View.GONE
        printError(adException.message, activity, "Interstitial did fail : ")
    }

    override fun interstitialDisappear() {
        mProgressBar!!.visibility = View.GONE
        mBtDisplay!!.visibility = View.GONE
        Toast.makeText(activity, "Interstitial did disappear", Toast.LENGTH_SHORT).show()
    }

    override fun onAdClicked() {
        isAdClicked = true
        Toast.makeText(activity, "Ad Clicked", Toast.LENGTH_SHORT).show()
    }

    override fun interstitialDidShown() {
        Toast.makeText(activity, "Interstitial did shown", Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        mngAdsInterstitialAdsFactory?.releaseMemory()
        mngAdsInterstitialAdsFactory = null
        super.onDestroy()
    }
}