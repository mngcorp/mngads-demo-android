package com.example.mngadsdemo.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.example.mngadsdemo.R;
import com.mngads.MNGNativeObject;
import com.mngads.views.MAdvertiseNativeContainer;


public class NativeHolder extends RecyclerView.ViewHolder {
    private final TextView nativeAdTitle;
    private final TextView nativeAdBody;
    private final TextView nativeAdContext;
    private final ImageView nativeBadgeImage;
    private final Button nativeAdCallToAction;
    public final MAdvertiseNativeContainer nativeAdContainer;
    private final ImageView nativeAdIcon;
    private final ViewGroup mediaContainer;
    public final View mItemView;

    public NativeHolder(View view) {
        super(view);
        mItemView=view;
        nativeAdContainer = view.findViewById(R.id.adUnit);
        nativeAdTitle = view.findViewById(R.id.nativeAdTitle);
        nativeAdBody = view.findViewById(R.id.nativeAdBody);
        nativeAdContext = view.findViewById(R.id.nativeAdContext);
        nativeBadgeImage = view.findViewById(R.id.badgeView);
        nativeAdCallToAction = view.findViewById(R.id.nativeAdCallToAction);
        nativeAdIcon = view.findViewById(R.id.nativeAdIcon);
        mediaContainer = view.findViewById(R.id.mediaContainer);

    }

    public void displayNativeAd(MNGNativeObject nativeObject) {
        if (nativeObject != null) {
            nativeAdContext.setText(nativeObject.getSocialContext());
            nativeAdTitle.setText(nativeObject.getTitle());
            nativeAdBody.setText(nativeObject.getBody());
            nativeAdCallToAction.setText(nativeObject.getCallToAction());
            nativeBadgeImage.setImageBitmap(nativeObject.getBadge(getContext(), "Publicité"));
            nativeObject.registerViewForInteraction(nativeAdContainer, mediaContainer, nativeAdIcon, nativeAdCallToAction);
            mItemView.setVisibility(View.VISIBLE);
        }
        else
        {
            mItemView.setVisibility(View.GONE);
        }
    }

    Context getContext() {
        return nativeAdContainer.getContext();
    }
}

