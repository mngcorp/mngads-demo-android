package com.example.mngadsdemo.adapter.holder;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.mngadsdemo.R;
import com.mngads.MNGNativeObject;
import com.mngads.views.MAdvertiseNativeContainer;


public class NativeHolder extends RecyclerView.ViewHolder {
    private final TextView nativeAdTitle;
    private final TextView nativeAdBody;
    public final RelativeLayout bodyLayout;
    private final TextView nativeAdContext;
    private final ImageView nativeBadgeImage;
    private final Button nativeAdCallToAction;
    public final MAdvertiseNativeContainer nativeAdContainer;
    private final ImageView nativeAdIcon;
    private final ViewGroup mediaContainer;

    public NativeHolder(View view) {
        super(view);
        nativeAdContainer = view.findViewById(R.id.adUnit);
        bodyLayout = view.findViewById(R.id.bodyLayout);
        nativeAdTitle = view.findViewById(R.id.nativeAdTitle);
        nativeAdBody = view.findViewById(R.id.nativeAdBody);
        nativeAdContext = view.findViewById(R.id.nativeAdContext);
        nativeBadgeImage = view.findViewById(R.id.badgeView);
        nativeAdCallToAction = view.findViewById(R.id.nativeAdCallToAction);
        nativeAdIcon = view.findViewById(R.id.nativeAdIcon);
        mediaContainer = view.findViewById(R.id.mediaContainer);
    }

    public void displayNativeAd(MNGNativeObject nativeObject, boolean setCover) {
        if (nativeObject != null) {
            bodyLayout.setVisibility(View.VISIBLE);
            nativeAdContainer.setVisibility(View.VISIBLE);
            nativeAdContext.setText(nativeObject.getSocialContext());
            nativeAdTitle.setText(nativeObject.getTitle());
            nativeAdBody.setText(nativeObject.getBody());
            nativeAdCallToAction.setText(nativeObject.getCallToAction());
            nativeBadgeImage.setImageBitmap(nativeObject.getBadge(getContext(), "Publicité"));
            if (setCover) {
                nativeObject.registerViewForInteraction(nativeAdContainer, mediaContainer, nativeAdIcon, nativeAdCallToAction);
            } else {
                nativeObject.registerViewForInteraction(nativeAdContainer, null, nativeAdIcon, nativeAdCallToAction);
            }
        } else {
            bodyLayout.setVisibility(View.GONE);
            nativeAdContainer.setVisibility(View.GONE);
        }
    }

    Context getContext() {
        return nativeAdContainer.getContext();
    }
}

