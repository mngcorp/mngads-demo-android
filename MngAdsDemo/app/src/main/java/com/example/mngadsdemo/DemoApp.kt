package com.example.mngadsdemo

import android.content.Context
import android.graphics.Typeface
import androidx.multidex.MultiDexApplication
import com.example.mngadsdemo.global.Constants
import com.example.mngadsdemo.utils.Utils
import com.madvertise.cmp.global.ConsentToolConfiguration
import com.madvertise.cmp.manager.ConsentManager
import com.mngads.MNGAdsFactory
import java.util.*

class DemoApp : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        initSDK()
        initFont()
        initCmp()
    }

    private fun initSDK() {
        MNGAdsFactory.setDebugModeEnabled(true)
        MNGAdsFactory.initialize(this, Utils.getAppId(this))
    }

    private fun initFont() {
        opensans_semiblod = Typeface.createFromAsset(assets, "fonts/nexa_light.ttf")
        opensans_regular = Typeface.createFromAsset(assets, "fonts/nexa_thin_italic.ttf")
        opensans_bold = Typeface.createFromAsset(assets, "fonts/nexa_bold.ttf")
    }

    private fun initCmp() {
        ConsentManager.sharedInstance.setDebugModeEnable(true)
        val configRes: Int = when (Locale.getDefault().isO3Language) {
            "ita" -> R.raw.madvertise_config_it
            "deu" -> R.raw.madvertise_config_de
            "fra" -> R.raw.madvertise_config_fr
            else -> R.raw.madvertise_config_en
        }
        val appId: Int = try {
            Utils.getAppId(this)?.toInt() ?: APP_ID_UNAVAILABLE
        } catch (e: Exception) {
            APP_ID_UNAVAILABLE
        }
        ConsentManager.sharedInstance.configure(
            this,
            appId,
            ConsentToolConfiguration(configRes),
            "FR",
            true
        )
    }

    companion object {

        const val MNGADS_APP_ID_KEY = "MNGADS_APP_ID"
        private const val APP_ID_UNAVAILABLE = 0

        @JvmStatic
        var opensans_semiblod: Typeface? = null
            private set

        @JvmStatic
        var opensans_regular: Typeface? = null
            private set
        
        var opensans_bold: Typeface? = null
            private set

        @JvmStatic
        var isAdClicked = false


        @JvmStatic
        fun getInterPlacement(context: Context?): String {
            return context?.let { "/" + Utils.getAppId(context) + Constants.DEFAULT_INTERSTITIAL_PLACEMENT_ID } ?: ""
        }

        @JvmStatic
        fun getInterOverlayPlacement(context: Context?): String {
            return context?.let{ "/" + Utils.getAppId(context) + Constants.OVERLAY_INTERSTITIAL_PLACEMENT_ID} ?: ""
        }

        @JvmStatic
        fun getBannerPlacement(context: Context?): String {
            return context?.let{"/" + Utils.getAppId(context) + Constants.BANNER_PLACEMENT_ID} ?: ""
        }

        @JvmStatic
        fun getNativePlacement(context: Context?): String {
            return context?.let{"/" + Utils.getAppId(context) + Constants.NATIVE_PLACEMENT_ID} ?: ""
        }

        @JvmStatic
        fun getNativePlacementNoCover(context: Context?): String {
            return context?.let{"/" + Utils.getAppId(context) + Constants.NATIVE_COVER_PLACEMENT_ID} ?: ""
        }

        fun getSquarePlacement(context: Context?): String {
            return context?.let{"/" + Utils.getAppId(context) + Constants.SQUARE_PLACEMENT_ID} ?: ""
        }

        fun getInfeedPlacement(context: Context?): String {
            return context?.let{"/" + Utils.getAppId(context) + Constants.INFEED_PLACEMENT_ID} ?: ""
        }

        @JvmStatic
        fun getThumbnailPlacement(context: Context?): String {
            return context?.let{"/" + Utils.getAppId(context) + Constants.THUMBAIL_PLACEMENT_ID} ?: ""
        }

        @JvmStatic
        fun getParallaxPlacement(context: Context?): String {
            return context?.let{"/" + Utils.getAppId(context) + Constants.PARALLAX_PLACEMENT_ID} ?: ""
        }

        @JvmStatic
        fun getParallaxPerfPlacement(context: Context?): String {
            return context?.let{"/" + Utils.getAppId(context) + Constants.PARALLAX_PERF_PLACEMENT_ID} ?: ""
        }

        @JvmStatic
        fun getRewardedVideoPlacement(context: Context?): String {
            return context?.let{"/" + Utils.getAppId(context) + Constants.REWARDED_VIDEO_PLACEMENT_ID} ?: ""
        }

        @JvmStatic
        fun getSyncDisplayPlacement(context: Context?, acr: Boolean): String {
            return context?.let{"/" + Utils.getAppId(context) + if (acr) Constants.SYNC_PLACEMENT_ID else Constants.SYNC_ACR_OFF_PLACEMENT_ID} ?: ""
        }
    }
}