package com.example.mngadsdemo.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationManager
import android.preference.PreferenceManager
import android.util.DisplayMetrics
import android.util.Log
import android.view.WindowManager
import androidx.core.app.ActivityCompat
import com.example.mngadsdemo.global.Constants
import com.example.mngadsdemo.global.Constants.CONSENT_FLAG
import com.example.mngadsdemo.global.MNG
import com.mngads.util.MNGGender
import com.mngads.util.MNGPreference
import kotlin.math.pow
import kotlin.math.sqrt

object Utils {

    private val TAG = Utils::class.java.simpleName

    @JvmStatic
    fun convertPixelsToDp(px: Float, context: Context): Float {
        val resources = context.resources
        val metrics = resources.displayMetrics
        return px / (metrics.densityDpi / 160f)
    }

    @JvmStatic
    fun convertDpToPixel(dp: Float, context: Context): Float {
        val resources = context.resources
        val metrics = resources.displayMetrics
        return dp * (metrics.densityDpi / 160f)
    }

    /**
     * get user location
     *
     * @param context
     * @return
     */
    @JvmStatic
    fun getCurrentLocation(context: Context): Location? {
        var mostRecentLocation: Location? = null
        val mLocationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val criteria = Criteria()
        criteria.accuracy = Criteria.ACCURACY_FINE
        val provider = mLocationManager.getBestProvider(criteria, true)
        if (provider != null) {
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                mostRecentLocation = mLocationManager.getLastKnownLocation(provider)
            }
        }
        return mostRecentLocation
    }

    @JvmStatic
    fun putAppId(context: Context, appId: String?) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = preferences.edit()
        if (isTabletDevice(context)) {
            editor.putString(Constants.TABLET_APP_ID, appId)
        } else {
            editor.putString(Constants.APP_ID, appId)
        }
        editor.commit()
    }

    @JvmStatic
    fun getAppId(context: Context): String? {
        return if (isTabletDevice(context)) {
            // device is a tablet
            // get tablet mngappid
            val preferences = PreferenceManager
                .getDefaultSharedPreferences(context)
            preferences.getString(
                Constants.TABLET_APP_ID,
                MNG.MNGADS_TABLET_APP_ID
            )
        } else {
            //device is a smart phone
            // get phone mngappid
            val preferences = PreferenceManager
                .getDefaultSharedPreferences(context)
            preferences.getString(Constants.APP_ID, MNG.MNGADS_APP_ID)
        }
    }

    @JvmStatic
    fun getPreference(context: Context): MNGPreference {
        val mngPreference = MNGPreference()
        val location = getCurrentLocation(context)
        //     Log.e("location", String.valueOf(location.getLatitude()));
        /*  Location location= new Location("dummyprovider");
        location.setLatitude(14.154564);
        location.setLongitude(26.1475544);*/mngPreference.age = 25
        mngPreference.gender = MNGGender.MNGGenderMale
        mngPreference.keyword = Constants.MNGADS_KEYWORD
        mngPreference.contentUrl = Constants.MNGADS_CONTENT_URL
        if (location != null) {
            Log.e("location", "location Dispo")
            mngPreference.setLocation(location, CONSENT_FLAG, context)
        } else {
            Log.e("location", "location no Dispo")
        }
        mngPreference.adChoicePosition = MNGPreference.BOTTOM_LEFT
        return mngPreference
    }

    /**
     * Check if device has a big enough screen to be a tablet
     *
     * @param context application context
     * @return true if device is a tablet, false if it is a phone
     */
    fun isTabletDevice(context: Context): Boolean {
        var bIsTabletDevice = false
        try {
            // Get screen information
            val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val display = wm.defaultDisplay
            if (display != null) {
                val dm = DisplayMetrics()
                val fInches: Double
                display.getMetrics(dm)
                val x: Double = (dm.widthPixels / dm.xdpi).toDouble().pow(2.0)
                val y: Double = (dm.heightPixels / dm.ydpi).toDouble().pow(2.0)
                fInches = sqrt(x + y)
                if (fInches >= 6.5) bIsTabletDevice = true
            }
        } catch (ex: Exception) {
            Log.e(TAG, "exception in isTabletDevice:$ex", ex)
        } catch (er: Error) {
            Log.e(TAG, "error in isTabletDevice:$er", er)
        }
        return bIsTabletDevice
    }
}