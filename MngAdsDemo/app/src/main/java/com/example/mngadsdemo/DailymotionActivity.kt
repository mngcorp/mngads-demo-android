package com.example.mngadsdemo

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.preference.PreferenceManager
import com.dailymotion.android.player.sdk.PlayerWebView
import com.example.mngadsdemo.utils.Utils
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.madvertise.cmp.global.Constants.IABTCF.IABTCF_TCString
import java.io.IOException
import java.net.URLEncoder

class DailymotionActivity : BaseActivity(), View.OnClickListener {

    private lateinit var playerWebView: PlayerWebView
    private var advertisingId: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dailymotion)
        initializeActionBar()
        getADID()
        initializeView()
    }

    private fun initializeView() {
        playerWebView = findViewById(R.id.dm_player_web_view)
        val params =
            mapOf("video" to "x71d2s4", "ads_params" to URLEncoder.encode(getAdsParams(), "utf-8"))
        playerWebView.load(params);
    }

    private fun getAdsParams(): String {
        var adsParams = ""
        // Location
        val location = Utils.getCurrentLocation(this)
        if (location != null) {
            adsParams = "lat=" + location.latitude + "&lng=" + location.longitude
        }

        // ADID
        if (advertisingId.isNotEmpty()) {
            adsParams = if (adsParams.isNotEmpty()) {
                "$adsParams&ads_device_id=$advertisingId"
            } else
                "ads_device_id=$advertisingId"
        }

        // TCF V2
        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
        val gdpr_consent = preferences.getString(IABTCF_TCString, null)
        if (gdpr_consent != null) {
            adsParams = if (adsParams.isNotEmpty()) {
                "$adsParams&gdpr_consent=$gdpr_consent"
            } else
                "gdpr_consent=$gdpr_consent"
        }
        return adsParams
    }

    private fun getADID() {
        val thread: Thread = object : Thread() {
            override fun run() {
                try {
                    advertisingId =
                        AdvertisingIdClient.getAdvertisingIdInfo(this@DailymotionActivity).id.toString()
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: GooglePlayServicesNotAvailableException) {
                    e.printStackTrace()
                } catch (e: GooglePlayServicesRepairableException) {
                    e.printStackTrace()
                }
            }
        }
        thread.start()
    }

    private fun initializeActionBar() {
        val toolbar = findViewById<Toolbar>(R.id.tool_bar)
        val tvTitle = toolbar.findViewById<TextView>(R.id.tvTitle)
        tvTitle.typeface = DemoApp.opensans_semiblod
        tvTitle.text = resources.getStringArray(R.array.ADS_CATEGORY)[16]
        findViewById<View>(R.id.ibBack).setOnClickListener(this)
        setSupportActionBar(toolbar)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.ibBack) {
            finish()
        }
    }

    override fun onPause() {
        super.onPause()
        playerWebView.onPause()
    }

    override fun onResume() {
        super.onResume()
        playerWebView.onResume()
    }
}