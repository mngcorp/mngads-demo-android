package com.example.mngadsdemo.adapter;


/*****************************************
 * MAdvertisePagerAdapter and Fragments
 *****************************************/

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.mngadsdemo.fragment.EmptyFragment;
import com.example.mngadsdemo.fragment.SwipeAdFragment;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the primary
 * sections of the app.
 */
public class MAdvertisePagerAdapter extends FragmentPagerAdapter {

    // declare page count
    private int mPageCount;

    // buffer for created Fragments
    private Fragment[] fragments;


    private HashSet<Integer> mAdIndicesSet;


    public MAdvertisePagerAdapter(FragmentManager fm, int pageCount, Integer[] adIndices) {
        super(fm);

        mPageCount=pageCount;
        fragments= new Fragment[pageCount];
        // init indices for ad views
        mAdIndicesSet = new HashSet<Integer>((Collection<Integer>) Arrays.asList(adIndices));
    }

    public boolean isAdPage(int page) {
        return mAdIndicesSet.contains(page);
    }

    @Override
    public Fragment getItem(int i) {
        Fragment item = null;

        if (fragments[i] != null) {
            // return previously created fragment
            item = fragments[i];
        } else {
            // create a new one
            if (isAdPage(i)) {
                // this is an Ad view, create an AdSectionFragment
                item = new SwipeAdFragment();
            } else {
                // The other sections of the app are dummy content fragments.
                Fragment fragment = new EmptyFragment();
                Bundle args = new Bundle();
                args.putInt(EmptyFragment.ARG_SECTION_NUMBER, i + 1);
                fragment.setArguments(args);
                item = fragment;
            }

            // store created fragment
            fragments[i] = item;
        }

        return item;
    }

    @Override
    public int getCount() {
        return mPageCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (isAdPage(position)) {
            // this is an Ad view
            title = "Advertising";
        } else {
            // The other sections of the app are dummy placeholders.
            title = "Section " + (position + 1);
        }
        return title;
    }
}
