package com.example.mngadsdemo.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.mngadsdemo.R;
import com.example.mngadsdemo.global.Constants;
import com.example.mngadsdemo.utils.ScaleLayout;
import com.example.mngadsdemo.utils.Utils;
import com.mngads.MNGNativeObject;
import com.mngads.views.MAdvertiseNativeContainer;

import java.util.ArrayList;


public class CarouselAdapter extends PagerAdapter implements
        OnPageChangeListener {

    private Context context;
    private ViewPager pager;
    private boolean setCover;
    private ArrayList<MNGNativeObject> ads;

    public CarouselAdapter(Context context, ViewPager pager,ArrayList<MNGNativeObject> ads, boolean setCover) {
        this.context = context;
        this.pager = pager;
        this.ads = ads;
        this.setCover = setCover;
    }

    @Override
    public void destroyItem(View container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        RelativeLayout _container = new RelativeLayout(context);

        LayoutInflater.from(context).inflate(R.layout.carousel_container,
                _container, true);

        ScaleLayout _adContainer = (ScaleLayout) _container
                .findViewById(R.id.slAdContainer);

        if(setCover)
        {
            LayoutInflater.from(context).inflate(R.layout.carousel_ad_unit,_adContainer, true);
        }
        else
        {
            LayoutInflater.from(context).inflate(R.layout.carousel_ad_unit_without,_adContainer, true);
        }

        _adContainer.setTag("" + position);

        _adContainer.setScaleBoth(Constants.SCALE_TYPE_SMALL);

        displayNativeAd(ads.get(position), (MAdvertiseNativeContainer) _adContainer.findViewById(R.id.adUnit), context);

        container.addView(_container);

        return _container;

    }

    @Override
    public int getCount() {
        return ads.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset,
                               int positionOffsetPixels) {
        if (positionOffset >= 0f && positionOffset <= 1f) {
            ScaleLayout _currentAdContainer = (ScaleLayout) pager
                    .findViewWithTag("" + (position));
            if (_currentAdContainer != null) {
                _currentAdContainer.setScaleBoth(Constants.SCALE_TYPE_BIG -
                        Constants.DIFFERENCE_IN_SCALE * positionOffset);
            }
            ScaleLayout _nextAdContainer = (ScaleLayout) pager
                    .findViewWithTag("" + (position + 1));
            if (_nextAdContainer != null) {
                _nextAdContainer.setScaleBoth(Constants.SCALE_TYPE_SMALL
                        + Constants.DIFFERENCE_IN_SCALE * positionOffset);
            }
        }
    }

    @Override
    public void onPageSelected(int position) {
        ScaleLayout _currentAdContainer = (ScaleLayout) pager
                .findViewWithTag("" + (position));
        if (_currentAdContainer != null) {
            _currentAdContainer.setScaleBoth(Constants.SCALE_TYPE_BIG);
        }
        ScaleLayout _previousAdContainer = (ScaleLayout) pager
                .findViewWithTag("" + (position - 1));
        if (_previousAdContainer != null) {
            _previousAdContainer.setScaleBoth(Constants.SCALE_TYPE_SMALL);

        }
        ScaleLayout _nextAdContainer = (ScaleLayout) pager.findViewWithTag(""
                + (position + 1));
        if (_nextAdContainer != null) {
            _nextAdContainer.setScaleBoth(Constants.SCALE_TYPE_SMALL);

        }

    }

    private void displayNativeAd(MNGNativeObject nativeObject,
                                 final MAdvertiseNativeContainer adView, final Context context) {

        // Create native UI using the native ad .
        final ImageView nativeAdIcon = (ImageView) adView
                .findViewById(R.id.nativeAdIcon);
        TextView nativeAdTitle = (TextView) adView
                .findViewById(R.id.nativeAdTitle);
        TextView nativeAdBody = (TextView) adView
                .findViewById(R.id.nativeAdBody);

        final ImageView nativeAdImage = (ImageView) adView
                .findViewById(R.id.nativeAdImage);

        ImageView nativeBadgeImage = (ImageView) adView
                .findViewById(R.id.badgeView);


        Button nativeAdCallToAction = (Button) adView
                .findViewById(R.id.nativeAdCallToAction);
        Button clickButton = (Button) adView
                .findViewById(R.id.clickButton);

        ViewGroup mediaContainer = (ViewGroup) adView.findViewById(R.id.mediaContainer);


        // setting ad meta data
        nativeAdCallToAction.setText(nativeObject.getCallToAction());
        nativeAdTitle.setText(nativeObject.getTitle());
        nativeAdBody.setText(nativeObject.getBody());
        nativeBadgeImage.setImageBitmap(nativeObject.getBadge());


        switch (nativeObject.getDisplayType()) {

            case MNGDisplayTypeAppInstall:


                Drawable imageAppInstall = ContextCompat.getDrawable(context, R.drawable.app_install);

                setDisplayType(imageAppInstall, nativeAdCallToAction, context);


                break;

            case MNGDisplayTypeContent:


                Drawable imageContent = ContextCompat.getDrawable(context, R.drawable.content);

                setDisplayType(imageContent, nativeAdCallToAction, context);


                break;


            case MNGDisplayTypeUnknown:

                break;


        }

        nativeObject.registerViewForInteraction(adView, mediaContainer, nativeAdIcon, clickButton);


    }

    private void setDisplayType(Drawable drawable, Button nativeAdCallToAction,Context context) {
        int w = (int) Utils.convertDpToPixel(15, context);
        drawable.setBounds(0, 0, w, w);
        nativeAdCallToAction.setCompoundDrawables(drawable, null, null, null);
    }

}