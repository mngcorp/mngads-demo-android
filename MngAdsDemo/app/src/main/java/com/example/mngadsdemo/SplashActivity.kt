package com.example.mngadsdemo

import android.Manifest.permission
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.preference.PreferenceManager
import android.util.Log
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts.StartActivityForResult
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.mngadsdemo.DemoApp.Companion.getInterPlacement
import com.example.mngadsdemo.fragment.BannerFragment.Companion.printError
import com.example.mngadsdemo.utils.Utils
import com.madvertise.cmp.global.Constants.IABTCF.IABTCF_TCString
import com.madvertise.cmp.listener.OnConsentProvidedListener
import com.madvertise.cmp.listener.OnPrivacyPolicyRequestedListener
import com.madvertise.cmp.manager.ConsentManager
import com.mngads.MNGAdsFactory
import com.mngads.listener.MNGAdsSDKFactoryListener
import com.mngads.listener.MNGClickListener
import com.mngads.listener.MNGInterstitialListener

class SplashActivity : AppCompatActivity(), MNGInterstitialListener, MNGClickListener {

    private var mMngAdsInterstitialFactory: MNGAdsFactory? = null
    var isAdClicked = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        MNGAdsFactory.setMNGAdsSDKFactoryListener(object : MNGAdsSDKFactoryListener {
            override fun onMNGAdsSDKFactoryDidFinishInitializing() {
                Log.d(TAG, "MNGAds SDK Factory Did Finish Initializing")
            }

            override fun onMNGAdsSDKFactoryDidFailInitialization(e: Exception) {
                Log.d(TAG, "MNGAdsSDKFactoryDidFailInitialization: $e")
            }
        })
        checkPermission()
    }

    override fun onResume() {
        super.onResume()
        if (isAdClicked) {
            isAdClicked = false
            startApp()
        }
    }

    private fun checkPermission() {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.Q) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    permission.ACCESS_BACKGROUND_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(
                        permission.ACCESS_FINE_LOCATION,
                        permission.ACCESS_COARSE_LOCATION,
                        permission.ACCESS_BACKGROUND_LOCATION
                    ),
                    LOCATION_REQUEST_CODE
                )
            } else {
                starCMPAndAds()
            }
        } else {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(
                    this,
                    permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(permission.ACCESS_FINE_LOCATION, permission.ACCESS_COARSE_LOCATION),
                    LOCATION_REQUEST_CODE
                )
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    if (ActivityCompat.checkSelfPermission(
                            this,
                            permission.ACCESS_BACKGROUND_LOCATION
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        ActivityCompat.requestPermissions(
                            this,
                            arrayOf(permission.ACCESS_BACKGROUND_LOCATION),
                            LOCATION_REQUEST_CODE_BACK
                        )
                    } else {
                        starCMPAndAds()
                    }
                } else {
                    starCMPAndAds()
                }
            }
        }
    }

    private fun starCMPAndAds() {
        initCMP()
    }

    private fun initCMP() {
        ConsentManager.sharedInstance.showCMP(this, object : OnConsentProvidedListener {
            override fun consentProvided(actionType: String) {
                val handler = Handler(Looper.getMainLooper())
                handler.postDelayed({ displayInterstitial() }, 200)
            }

            override fun consentFailed(error: String) {
                val handler = Handler(Looper.getMainLooper())
                handler.postDelayed({ displayInterstitial() }, 200)
            }
        })
        ConsentManager.sharedInstance.setOnPrivacyPolicyRequestedListener(object :
            OnPrivacyPolicyRequestedListener {
            override fun onPrivacyPolicyRequested(url: String) {
                val i = Intent(this@SplashActivity, ReadMeActivity::class.java)
                someActivityResultLauncher.launch(i)
            }
        })
    }

    var someActivityResultLauncher = registerForActivityResult(
        StartActivityForResult()
    ) { result: ActivityResult ->
        if (result.resultCode == RESULT_OK) {
            val consentString = PreferenceManager.getDefaultSharedPreferences(
                applicationContext
            ).getString(IABTCF_TCString, "")
            if (consentString!!.isEmpty()) {
                ConsentManager.sharedInstance.openCMP(this@SplashActivity)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.d(
            TAG,
            "onRequestPermissionsResult: permission popup is no longer, or has not been, displayed."
        )
        if (requestCode == LOCATION_REQUEST_CODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                if (ActivityCompat.checkSelfPermission(
                        this,
                        permission.ACCESS_BACKGROUND_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(permission.ACCESS_BACKGROUND_LOCATION),
                        LOCATION_REQUEST_CODE_BACK
                    )
                } else {
                    starCMPAndAds()
                }
            } else {
                starCMPAndAds()
            }
        } else {
            starCMPAndAds()
        }
    }

    private fun startApp() {
        MNGAdsFactory.setMNGAdsSDKFactoryListener(null)
        Log.d(TAG, "Starting home activity")
        if (!isAdClicked) {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }

    private fun displayInterstitial() {
        mMngAdsInterstitialFactory = MNGAdsFactory(this)
        mMngAdsInterstitialFactory!!.setPlacementId(getInterPlacement(this))
        mMngAdsInterstitialFactory!!.setInterstitialListener(this)
        mMngAdsInterstitialFactory!!.setClickListener(this)
        mMngAdsInterstitialFactory!!.loadInterstitial(Utils.getPreference(this), false)
    }

    override fun onBackPressed() {}

    override fun interstitialDidLoad() {
        Log.d(TAG, "interstitialDidLoad")
        if (!ConsentManager.sharedInstance.isConsentToolShown()) {
            if (mMngAdsInterstitialFactory!!.isInterstitialReady) {
                mMngAdsInterstitialFactory!!.displayInterstitial()
            } else {
                Log.d(TAG, "Interstitial not ready ")
                startApp()
            }
        } else {
            Log.d(TAG, "Interstitial not ready ")
            startApp()
        }
    }

    override fun interstitialDidFail(adException: Exception) {
        Log.d(TAG, "interstitialDidFail: $adException")
        printError(adException.message, this@SplashActivity, "Interstitial did fail : ")
        startApp()
    }

    override fun onDestroy() {
        if (mMngAdsInterstitialFactory != null) {
            mMngAdsInterstitialFactory!!.releaseMemory()
        }
        super.onDestroy()
    }

    override fun onAdClicked() {
        Log.d(TAG, "onAdClicked")
        isAdClicked = true
    }

    override fun interstitialDisappear() {
        Log.d(TAG, "Interstitial Disappear")
        startApp()
    }

    override fun interstitialDidShown() {
        Log.d(TAG, "Interstitial Shown")
        Toast.makeText(this, "Interstitial did Shown", Toast.LENGTH_SHORT).show()
    }

    companion object {
        private const val TAG = "SplashActivityTAG"
        const val LOCATION_REQUEST_CODE = 1
        const val LOCATION_REQUEST_CODE_BACK = 2
    }
}