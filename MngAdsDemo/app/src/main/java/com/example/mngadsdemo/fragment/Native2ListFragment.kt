package com.example.mngadsdemo.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mngadsdemo.DemoApp.Companion.getNativePlacement
import com.example.mngadsdemo.R
import com.example.mngadsdemo.adapter.Native2ListAdapter
import com.example.mngadsdemo.listenter.ActionBarListener
import com.example.mngadsdemo.utils.Utils.getPreference
import com.mngads.MNGAdsFactory
import com.mngads.MNGNativeObject
import com.mngads.listener.MNGNativeListener

class Native2ListFragment : Fragment(), ActionBarListener, MNGNativeListener {

    private var mRecycler: RecyclerView? = null
    private var mNativeListAdapter: Native2ListAdapter? = null
    private var mngAdsNativeAdsFactory: MNGAdsFactory? = null
    private var mSetCover = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.native_list, container, false)
        initView(view)
        initTypeShow(view)
        initRecycle()
        initNativeAds()
        return view
    }

    private fun initNativeAds() {
        mngAdsNativeAdsFactory = MNGAdsFactory(activity)
        mngAdsNativeAdsFactory!!.setNativeListener(this)
        mngAdsNativeAdsFactory!!.setPlacementId(getNativePlacement(context))
    }

    private fun initView(view: View) {
        mRecycler = view.findViewById(R.id.recycler)
    }

    private fun initTypeShow(view: View) {
        view.findViewById<View>(R.id.rlnoSizeContainer).setOnClickListener {
            mSetCover = false
            initRecycle()
            displayNativeAd()
        }
        view.findViewById<View>(R.id.rlWithSizeContainer).setOnClickListener {
            mSetCover = true
            initRecycle()
            displayNativeAd()
        }
    }

    private fun displayNativeAd() {
        if (!mngAdsNativeAdsFactory!!.isBusy) {
            if (mSetCover) {
                mngAdsNativeAdsFactory!!.loadNative(
                    getPreference(
                        requireActivity()
                    )
                )
            } else {
                mngAdsNativeAdsFactory!!.loadNativeWithOutCover(
                    getPreference(
                        requireActivity()
                    )
                )
            }
        }
    }

    private fun initRecycle() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity)
        mRecycler?.layoutManager = layoutManager
        mRecycler?.itemAnimator = DefaultItemAnimator()
    }

    override fun onDestroy() {
        mngAdsNativeAdsFactory?.releaseMemory()
        mngAdsNativeAdsFactory = null
        super.onDestroy()
    }

    override fun switchParams(isOn: Boolean) {}

    override fun nativeObjectDidLoad(mngNativeObject: MNGNativeObject) {
        mNativeListAdapter = Native2ListAdapter(20, activity, mSetCover, mngNativeObject)
        mRecycler!!.adapter = mNativeListAdapter
    }

    override fun nativeObjectDidFail(e: Exception) {}
}