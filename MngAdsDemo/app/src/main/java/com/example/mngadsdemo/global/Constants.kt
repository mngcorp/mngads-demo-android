package com.example.mngadsdemo.global

import com.example.mngadsdemo.R

object Constants {
    // Sync
    const val SYNC_APP_ID = "madvertise"
    const val SYNC_URL_BASE = "https://services-client.sync.tv/display"

    // Key
    const val AD_CHOICES_VIEW_KEY = "AD_CHOICES_VIEW"
    const val APP_ID = "mng_app_id"
    const val TABLET_APP_ID = "mng_tab_app_id"

    // Size and Scale
    const val SCALE_TYPE_BIG = 1.0f
    const val SCALE_TYPE_SMALL = 0.7f
    const val DIFFERENCE_IN_SCALE = SCALE_TYPE_BIG - SCALE_TYPE_SMALL
    const val BANNER_90_HEIGHT = 90 //dp
    const val BANNER_50_HEIGHT = 50 //dp
    const val BANNER_100_HEIGHT = 100 //dp
    const val SQUARE_AD_HEIGHT = 250 //dp
    const val SQUARE_AD_WIDTH = 300 //dp
    const val DP = "dp"

    // Tag
    const val SIZE_NATIVE_HEIGHT = 400
    const val SIZE_NATIVE_HEIGHT_WITH_OUT = 140
    const val MNGADS_KEYWORD = "semantic=https://www.google.com/"
    const val MNGADS_KEYWORD2 = "BlueStack"

    // URL
    const val MNGADS_CONTENT_URL = "http://madvertise.com/"
    const val READ_ME_URL = " https://bitbucket.org/mngcorp/mngads-demo-android/wiki/Home"

    // Config
    const val WS_DELAY = 5000
    const val TIME_OUT_DELAY = 3000
    const val SPLASH_TIME = 3000
    const val CAROUSEL_ADS_NUMBER = 5
    const val MAX_RUNNING_FACTORY = 2
    @JvmField
    val categoryIcon = intArrayOf(
        R.drawable.ic_privacy,
        R.drawable.ic_bluestack,
        R.drawable.ic_apssfire,
        R.drawable.ic_banner,
        R.drawable.ic_in_feed,
        R.drawable.ic_interstitial,
        R.drawable.ic_native_ad,
        R.drawable.ic_native_ad,
        R.drawable.ic_rewarded_video,
        R.drawable.ic_native_ad,
        R.drawable.ic_native_ad,
        R.drawable.ic_in_feed,
        R.drawable.ic_carroussel,
        R.drawable.ic_4_in_1,
        R.drawable.ic_pager,
        R.drawable.ic_plugin_android,
        R.drawable.ic_settin,
        R.drawable.ic_rewarded_video,
        R.drawable.ic_info
    )

    // Placement
    const val BANNER_PLACEMENT_ID = "/banner"
    const val SQUARE_PLACEMENT_ID = "/square"
    const val DEFAULT_INTERSTITIAL_PLACEMENT_ID = "/interstitial"
    const val NATIVE_PLACEMENT_ID = "/nativead"
    const val NATIVE_COVER_PLACEMENT_ID = "/nativead_without_cover"
    const val REWARDED_VIDEO_PLACEMENT_ID = "/videoRewarded"
    const val INFEED_PLACEMENT_ID = "/infeed"
    const val THUMBAIL_PLACEMENT_ID = "/thumbnailMngperf"

    //  public final static String THUMBAIL_PLACEMENT_ID = "/thumbnailMRAID";
    // public final static String THUMBAIL_PLACEMENT_ID = "/thumbnailVAST";
    const val PARALLAX_PLACEMENT_ID = "/parallaxsmart"
    const val PARALLAX_PERF_PLACEMENT_ID = "/mngperfparallax"
    const val OVERLAY_INTERSTITIAL_PLACEMENT_ID = "/interstitialOverlay"
    const val SYNC_PLACEMENT_ID = "/sync"
    const val SYNC_ACR_OFF_PLACEMENT_ID = "/sync_acr_off"
    const val MOPUB_BANNER_AD_UNIT = "2b5d4adf0f364a6d830e81eda23dfd85"
    const val MOPUB_INTER_AD_UNIT = "ae2f17c982274588b6b336995140113e"
    const val MOPUB_NATIVE_AD_UNIT = "8aa4eee852274e7eb694f90ba62bd4b2"
    const val DFP_INTER_AD_UNIT = "/40369895/71338//12145"
    const val DFP_NATIVE_AD_UNIT = "/40369895/71338//32264"
    const val DFP_REWARDED_AD_UNIT = "/40369895/71338"
    const val CONSENT_FLAG = 2
}