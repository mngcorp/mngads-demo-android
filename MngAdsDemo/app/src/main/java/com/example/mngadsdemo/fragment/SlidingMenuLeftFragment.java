package com.example.mngadsdemo.fragment;


import android.app.Activity;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import com.example.mngadsdemo.R;
import com.example.mngadsdemo.adapter.AdsCategoryAdapter;
import com.example.mngadsdemo.global.Constants;
import com.example.mngadsdemo.listenter.SlidingMenuLeftListener;


public class SlidingMenuLeftFragment extends Fragment {
    private SlidingMenuLeftListener mSlidingMenuLeftListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.left_menu, container, false);
        View headView = inflater.inflate(R.layout.logo_header, container, false);
        ListView listAdCategory = view.findViewById(R.id.lvAdCategory);
        listAdCategory.addHeaderView(headView);
        AdsCategoryAdapter adsCategoryAdapter = new AdsCategoryAdapter(getActivity().getResources().getStringArray(R.array.ADS_CATEGORY),
                Constants.categoryIcon, getActivity().getApplicationContext());
        listAdCategory.setAdapter(adsCategoryAdapter);
        listAdCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mSlidingMenuLeftListener.onItemClickListener(position - 1);
            }
        });
        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSlidingMenuLeftListener = (SlidingMenuLeftListener) activity;
    }
}
