package com.example.mngadsdemo.listenter

interface SlidingMenuLeftListener {
    fun onItemClickListener(position: Int)
}