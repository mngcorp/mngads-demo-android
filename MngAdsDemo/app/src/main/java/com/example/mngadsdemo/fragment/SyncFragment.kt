package com.example.mngadsdemo.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.mngadsdemo.DemoApp.Companion.getSyncDisplayPlacement
import com.example.mngadsdemo.R
import com.mngads.MNGAdsFactory
import com.mngads.exceptions.MAdvertiseException
import com.mngads.listener.BluestackSyncListener

class SyncFragment : Fragment(), View.OnClickListener, BluestackSyncListener {

    private val TAG = SyncFragment::class.java.simpleName

    private var mngAdsFactory: MNGAdsFactory? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mngAdsFactory = MNGAdsFactory(activity)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_sync, container, false)
        v.findViewById<View>(R.id.acr_on).setOnClickListener(this)
        v.findViewById<View>(R.id.acr_off).setOnClickListener(this)
        return v
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.acr_on -> loadSync(true)
            R.id.acr_off -> loadSync(false)
        }
    }

    private fun loadSync(acr: Boolean) {
        mngAdsFactory!!.setPlacementId(getSyncDisplayPlacement(this.context, acr))
        mngAdsFactory!!.setSyncListener(this)
        mngAdsFactory!!.loadSync()
    }

    override fun syncDisplayDidLoad(view: View) {
        if (this.activity != null) {
            (requireActivity().findViewById<View>(android.R.id.content) as ViewGroup).addView(view)
        }
    }

    override fun syncDisplayDidFail(e: Exception) {
        val exception = e as MAdvertiseException
        Log.e(TAG, "SyncDisplay did fail :" + exception.message)
        if (activity != null) {
            Toast.makeText(
                requireActivity().applicationContext,
                "SyncDisplay did fail",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    companion object {
        fun newInstance(): SyncFragment {
            return SyncFragment()
        }
    }
}