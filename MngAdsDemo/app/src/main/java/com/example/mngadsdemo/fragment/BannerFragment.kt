package com.example.mngadsdemo.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.mngadsdemo.DemoApp
import com.example.mngadsdemo.R
import com.example.mngadsdemo.databinding.BannerBinding
import com.example.mngadsdemo.global.Constants
import com.example.mngadsdemo.listenter.ActionBarListener
import com.example.mngadsdemo.utils.Utils
import com.mngads.MNGAdsFactory
import com.mngads.exceptions.MAdvertiseException
import com.mngads.listener.MNGBannerListener
import com.mngads.listener.MNGClickListener
import com.mngads.listener.MNGRefreshListener
import com.mngads.util.MNGAdSize
import com.mngads.util.MNGFrame
import com.mngads.util.MNGPreference
import org.json.JSONObject

class BannerFragment : Fragment(), MNGBannerListener, ActionBarListener, View.OnClickListener,
    MNGClickListener, MNGRefreshListener {

    private val TAG = BannerFragment::class.java.simpleName
    private var mngAdsBannerAdsFactory: MNGAdsFactory? = null
    private lateinit var binding: BannerBinding

    private lateinit var detailsContainer: RelativeLayout
    private lateinit var adContainer: LinearLayout
    private var paramsContainer: RelativeLayout? = null
    private lateinit var squareSizeContainer: RelativeLayout
    private lateinit var bannerSizeContainer: RelativeLayout

    private var screenWidth = 0
    private lateinit var mFrame: MNGFrame
    private lateinit var mngPreference: MNGPreference

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.banner, container, false)
        binding = BannerBinding.bind(view)

        mFrame =
            if (resources.getBoolean(R.bool.is_tablet)) MNGAdSize.MNG_DYNAMIC_LEADERBOARD else MNGAdSize.MNG_DYNAMIC_BANNER

        screenWidth = resources.displayMetrics.widthPixels

        binding.bannerDetails.apply {
            typeface = DemoApp.opensans_semiblod
            text = "${
                Utils.convertPixelsToDp(
                    screenWidth.toFloat(),
                    requireActivity()
                ).toInt()
            } ${Constants.DP} X ${mFrame.height} ${Constants.DP}"
        }

        binding.bannerSize.typeface = DemoApp.opensans_semiblod
        binding.bigSquareSize.typeface = DemoApp.opensans_semiblod

        detailsContainer = binding.rlDetailsContainer
        adContainer = binding.bannerContainer
        paramsContainer = binding.rlParamsContainer
        squareSizeContainer = binding.rlSquareSizeContainer
        bannerSizeContainer = binding.rlBannerSizeContainer
        squareSizeContainer.setOnClickListener(this)
        bannerSizeContainer.setOnClickListener(this)
        mngPreference = Utils.getPreference(requireActivity())

        return view
    }

    private fun displayBanner() {
        when (mFrame.height) {
            Constants.BANNER_50_HEIGHT -> {
                adContainer.removeAllViews()
                // init banner placement id
                mngAdsBannerAdsFactory = MNGAdsFactory(activity)
                mngAdsBannerAdsFactory!!.setBannerListener(this)
                mngAdsBannerAdsFactory!!.setClickListener(this)
                mngAdsBannerAdsFactory!!.setRefreshListener(this)
                mngAdsBannerAdsFactory!!.setPlacementId(DemoApp.getBannerPlacement(context))
                mngAdsBannerAdsFactory!!.loadBanner(mFrame, mngPreference)
            }
            Constants.BANNER_90_HEIGHT -> {
                adContainer.removeAllViews()
                // init banner placement id
                mngAdsBannerAdsFactory = MNGAdsFactory(activity)
                mngAdsBannerAdsFactory!!.setBannerListener(this)
                mngAdsBannerAdsFactory!!.setClickListener(this)
                mngAdsBannerAdsFactory!!.setRefreshListener(this)
                mngAdsBannerAdsFactory!!.setPlacementId(DemoApp.getBannerPlacement(context))
                mngAdsBannerAdsFactory!!.loadBanner(mFrame, mngPreference)
            }
            Constants.SQUARE_AD_HEIGHT -> {
                adContainer.removeAllViews()
                // init square placement id
                mngAdsBannerAdsFactory = MNGAdsFactory(activity)
                mngAdsBannerAdsFactory!!.setBannerListener(this)
                mngAdsBannerAdsFactory!!.setClickListener(this)
                mngAdsBannerAdsFactory!!.setRefreshListener(this)
                mngAdsBannerAdsFactory!!.setPlacementId(DemoApp.getSquarePlacement(context))
                mngAdsBannerAdsFactory!!.loadBanner(mFrame, mngPreference)
            }
        }
        Log.d(TAG, "Number of running factory is :" + MNGAdsFactory.getNumberOfRunningFactory())
    }

    override fun bannerDidLoad(view: View, preferredHeightDP: Int) {
        squareSizeContainer.isClickable = true
        bannerSizeContainer.isClickable = true
        val bannerPreferredHeightPx = Utils.convertDpToPixel(
            preferredHeightDP.toFloat(), requireActivity()
        ).toInt()
        detailsContainer.layoutParams.height = bannerPreferredHeightPx
        detailsContainer.requestLayout()
        adContainer.removeAllViews()
        adContainer.visibility = View.VISIBLE
        adContainer.addView(view)
        Log.d(TAG, "Banner did load preferred Height $preferredHeightDP dp")
        Toast.makeText(requireActivity(), "Banner did load", Toast.LENGTH_SHORT).show()
    }

    override fun bannerDidFail(e: Exception) {
        squareSizeContainer.isClickable = true
        bannerSizeContainer.isClickable = true
        val adException = e as MAdvertiseException
        Log.e(
            TAG,
            "Banner did fail : " + adException.message + " error code = " + adException.errorCode
        )
        printError(adException.message, activity, "Banner did fail : ")
    }

    override fun bannerResize(frame: MNGFrame) {
        Log.d(TAG, "Banner did resize w " + frame.width + " h " + frame.height)
        val bannerPreferredHeightPx = Utils.convertDpToPixel(
            frame.height.toFloat(), requireActivity()
        ).toInt()
        detailsContainer.layoutParams.height = bannerPreferredHeightPx
        detailsContainer.requestLayout()
    }

    override fun onDestroy() {
        if (mngAdsBannerAdsFactory != null) {
            mngAdsBannerAdsFactory!!.releaseMemory()
            mngAdsBannerAdsFactory = null
        }
        super.onDestroy()
    }

    override fun switchParams(isOn: Boolean) {
        if (paramsContainer == null) return
        if (!isOn) {
            paramsContainer!!.visibility = View.VISIBLE
            adContainer.visibility = View.GONE
        } else {
            paramsContainer!!.visibility = View.GONE
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onClick(v: View) {
        detailsContainer.visibility = View.VISIBLE
        when (v.id) {
            R.id.rlSquareSizeContainer -> {
                mFrame = MNGAdSize.MNG_MEDIUM_RECTANGLE
                val squareWith = Utils.convertDpToPixel(mFrame.width.toFloat(), requireActivity())
                    .toInt()
                detailsContainer.layoutParams.width = squareWith
                adContainer.layoutParams.width = squareWith
                binding.bannerDetails.text =
                    "${Constants.SQUARE_AD_WIDTH} ${Constants.DP} X ${mFrame.height} ${Constants.DP}"
            }
            R.id.rlBannerSizeContainer -> {
                mFrame =
                    if (resources.getBoolean(R.bool.is_tablet)) MNGAdSize.MNG_DYNAMIC_LEADERBOARD else MNGAdSize.MNG_DYNAMIC_BANNER
                detailsContainer.layoutParams.width = screenWidth
                adContainer.layoutParams.width = screenWidth
                binding.bannerDetails.text = Utils.convertPixelsToDp(
                    screenWidth.toFloat(),
                    requireActivity()
                ).toInt().toString() + Constants.DP + " X " + mFrame.height + Constants.DP
            } else -> {}
        }
        val bannerHeightPx = Utils.convertDpToPixel(
            mFrame.height.toFloat(), requireActivity()
        ).toInt()
        detailsContainer.layoutParams.height = bannerHeightPx
        detailsContainer.requestLayout()
        updateUI()
        displayBanner()
        squareSizeContainer.isClickable = false
        bannerSizeContainer.isClickable = false
    }

    private fun updateUI() {
        when (mFrame.height) {
            Constants.SQUARE_AD_HEIGHT -> {
                binding.bannerSize.setTextColor(
                    ContextCompat.getColor(
                        requireActivity(),
                        R.color.white
                    )
                )
                binding.bigSquareSize.setTextColor(
                    ContextCompat.getColor(
                        requireActivity(),
                        R.color.blue
                    )
                )
            }
            Constants.BANNER_50_HEIGHT, Constants.BANNER_90_HEIGHT -> {
                binding.bannerSize.setTextColor(
                    ContextCompat.getColor(
                        requireActivity(),
                        R.color.blue
                    )
                )
                binding.bigSquareSize.setTextColor(
                    ContextCompat.getColor(
                        requireActivity(),
                        R.color.white
                    )
                )
            }
            else -> {}
        }
    }

    override fun onAdClicked() {
        Toast.makeText(activity, "Ad Clicked", Toast.LENGTH_SHORT).show()
        DemoApp.isAdClicked = true
        Log.d(TAG, "Ad Clicked")
    }

    override fun onRefreshSucceed() {
        Log.d(TAG, "on refresh succeed")
        Toast.makeText(activity, "on refresh succeed", Toast.LENGTH_SHORT).show()
    }

    override fun onRefreshFailed(e: Exception) {
        Log.d(TAG, "on refresh failed")
        Toast.makeText(activity, "on refresh failed $e", Toast.LENGTH_SHORT).show()
    }

    companion object {
        @JvmStatic
        fun printError(error: String?, activity: Context?, tag: String) {
            var error = error
            try {
                val errorPrint = JSONObject(error)
                val status = JSONObject(errorPrint.getString("status"))
                val config = JSONObject(errorPrint.getString("config"))
                error =
                    tag + "Capped Config = " + config.getString("capping") + "-" + config.getString(
                        "cappingShift"
                    ) + " ** Capping =" + status.getString("capping") + " et Shift=" + status.getString(
                        "cappingShift"
                    ) + "** H =" + errorPrint.getString("history")
                Toast.makeText(activity, error, Toast.LENGTH_LONG).show()
            } catch (x: Exception) {
                Toast.makeText(activity, tag + error, Toast.LENGTH_SHORT).show()
            }
        }
    }
}