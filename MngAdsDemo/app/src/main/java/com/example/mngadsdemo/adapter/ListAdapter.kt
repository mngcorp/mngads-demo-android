package com.example.mngadsdemo.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.RelativeLayout
import android.widget.Toast
import com.example.mngadsdemo.R

class ListAdapter(private val listCount: Int, private val context: Context) : BaseAdapter() {

    private var mView: View? = null
    private var mAdParalexPosition = 5

    constructor(listCount: Int, context: Context, adParalexPosition: Int) : this(
        listCount,
        context
    ) {
        mAdParalexPosition = adParalexPosition
    }

    override fun getCount(): Int {
        var count = listCount
        if (mView != null) {
            count++
        }
        return count
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val container = inflater.inflate(R.layout.section_unit, null)
        if (mView != null && position == mAdParalexPosition) {
            if (mView!!.parent != null) {
                (mView!!.parent as ViewGroup).removeAllViews()
            }
            (container as RelativeLayout).addView(mView)
            setBackgroundNull(container)
        }
        container.setOnClickListener {
            var paralexPosition = position
            if (position > mAdParalexPosition && mView != null) {
                paralexPosition--
            }
            Toast.makeText(context, "$paralexPosition", Toast.LENGTH_SHORT).show()
        }
        return container
    }

    private fun setBackgroundNull(convertView: View) {
        convertView.background = null
    }

    fun setAdView(adView: View?) {
        mView = adView
        notifyDataSetChanged()
    }
}