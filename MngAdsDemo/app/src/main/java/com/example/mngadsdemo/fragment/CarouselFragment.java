package com.example.mngadsdemo.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;

import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.mngadsdemo.DemoApp;
import com.example.mngadsdemo.R;
import com.example.mngadsdemo.adapter.CarouselAdapter;
import com.example.mngadsdemo.adapter.ListAdapter;
import com.example.mngadsdemo.global.Constants;
import com.example.mngadsdemo.listenter.ActionBarListener;
import com.example.mngadsdemo.utils.Utils;
import com.mngads.MNGAdsFactory;
import com.mngads.MNGNativeObject;
import com.mngads.exceptions.MAdvertiseException;
import com.mngads.listener.MNGClickListener;
import com.mngads.listener.MNGNativeCollectionListener;
import com.mngads.util.MNGPreference;

import java.util.ArrayList;


public class CarouselFragment extends Fragment implements MNGNativeCollectionListener, ActionBarListener, MNGClickListener {
    private ViewPager carouselWithOut;
    private ViewPager carousel;

    private RelativeLayout carouselContainer;
    private RelativeLayout carouselContainerWithOut;
    private ListView listNonsense;

    private ListAdapter listAdapter;

    private ArrayList<MNGNativeObject> adsNativeObject;
    private MNGAdsFactory mngNativeAdCollectionFactory;

    private final String TAG = CarouselFragment.class.getSimpleName();
    private MNGPreference mngPreference;
    private boolean mSetCover=true;
    private View mProgressBar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.carousel, container, false);

        listNonsense = view.findViewById(R.id.listNonsense);
        mProgressBar = view.findViewById(R.id.masProgressBar);
        mProgressBar.setVisibility(View.VISIBLE);

        carouselContainer = new RelativeLayout(getActivity().getApplicationContext());
        carouselContainerWithOut = new RelativeLayout(getActivity().getApplicationContext());
        inflater.inflate(R.layout.pager, carouselContainer,true);
        inflater.inflate(R.layout.pager_without, carouselContainerWithOut,true);
        createView();
        createTypeShow(view);
        createAdFactory();
        createSimpleList();
        createNativeAdCollection();
        return view;

    }


    private void createView() {
        int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20 * 2, getResources().getDisplayMetrics());
        carousel = carouselContainer.findViewById(R.id.vpCarousel);
        carousel.setPadding(margin, 0, margin, 0);
        carousel.setClipToPadding(false);
        carousel.setPageMargin(margin / 2);
        carousel.setClipChildren(false);
    }


    private void createViewWithOut() {
        int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20 * 2, getResources().getDisplayMetrics());
        carouselWithOut = carouselContainerWithOut.findViewById(R.id.vpCarousel_without);
        carouselWithOut.setPadding(margin, 0, margin, 0);
        carouselWithOut.setClipToPadding(false);
        carouselWithOut.setPageMargin(margin / 2);
        carouselWithOut.setClipChildren(false);
    }


    private void createTypeShow(View view) {
        view.findViewById(R.id.rlnoSizeContainer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSetCover = false;
                mProgressBar.setVisibility(View.VISIBLE);
                createViewWithOut();
                createNativeAdCollection();
            }
        });
        view.findViewById(R.id.rlWithSizeContainer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSetCover = true;
                mProgressBar.setVisibility(View.VISIBLE);
                createView();
                createNativeAdCollection();
            }
        });
    }



    private void createAdFactory() {
        mngNativeAdCollectionFactory = new MNGAdsFactory(getActivity());
        mngNativeAdCollectionFactory.setNativeCollectionListener(this);
        mngNativeAdCollectionFactory.setClickListener(this);
        mngNativeAdCollectionFactory.setNativeCollectionListener(this);
        mngNativeAdCollectionFactory.setPlacementId(DemoApp.getNativePlacement(getContext()));
        mngPreference = Utils.getPreference(getActivity());
    }

    private void createNativeAdCollection() {
        if(mSetCover)
        {
            mngNativeAdCollectionFactory.loadNativeCollection(Constants.CAROUSEL_ADS_NUMBER, mngPreference);
        }
        else
        {
            mngNativeAdCollectionFactory.loadNativeCollectionWithOutCover(Constants.CAROUSEL_ADS_NUMBER, mngPreference);
        }
    }

    private void createSimpleList() {
        listAdapter = new ListAdapter(10, getActivity().getApplicationContext());
        listNonsense.setAdapter(listAdapter);
    }


    @Override
    public void nativeAdCollectionDidLoad(ArrayList<MNGNativeObject> mngNativeObjects) {
        mProgressBar.setVisibility(View.GONE);
        Log.e(TAG, "Native object collection did load");
        Toast.makeText(getActivity().getApplicationContext(), "Native object collection did load size is :" + mngNativeObjects.size(), Toast.LENGTH_SHORT).show();
        adsNativeObject = mngNativeObjects;
        displayPager();

    }

    @Override
    public void nativeAdCollectionDidFail(Exception e) {
        mProgressBar.setVisibility(View.GONE);
        MAdvertiseException adException = (MAdvertiseException) e;
        Log.e(TAG, "Native object did fail " + adException.getMessage() + " error code = " + adException.getErrorCode());
        Toast.makeText(getActivity().getApplicationContext(), "Native object did fail", Toast.LENGTH_SHORT).show();
    }


    private void displayPager() {
        Toast.makeText(getActivity().getApplicationContext(), "display pager", Toast.LENGTH_SHORT).show();
        PagerAdapter adapter;
        if(mSetCover)
        {
            adapter = new CarouselAdapter(getActivity().getApplicationContext(), carousel, adsNativeObject,mSetCover);
            carousel.setAdapter(adapter);
            carousel.setOnPageChangeListener((OnPageChangeListener) adapter);
            carousel.setOffscreenPageLimit(adapter.getCount());
            listAdapter.setAdView(carouselContainer);
        }
        else
        {
            adapter = new CarouselAdapter(getActivity().getApplicationContext(), carouselWithOut, adsNativeObject,mSetCover);
            carouselWithOut.setAdapter(adapter);
            carouselWithOut.setOnPageChangeListener((OnPageChangeListener) adapter);
            carouselWithOut.setOffscreenPageLimit(adapter.getCount());
            listAdapter.setAdView(carouselContainerWithOut);
        }
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        if (mngNativeAdCollectionFactory != null) {
            mngNativeAdCollectionFactory.releaseMemory();
            mngNativeAdCollectionFactory = null;
        }
        super.onDestroy();
    }

    @Override
    public void switchParams(boolean isOn) {

    }

    @Override
    public void onAdClicked() {
        DemoApp.setAdClicked(true);
        Toast.makeText(getActivity(), "Ad Clicked", Toast.LENGTH_SHORT).show();

    }
}