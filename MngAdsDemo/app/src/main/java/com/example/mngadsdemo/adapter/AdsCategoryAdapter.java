package com.example.mngadsdemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mngadsdemo.DemoApp;
import com.example.mngadsdemo.R;

public class AdsCategoryAdapter extends BaseAdapter {

    private String[] titles;
    private int[] iconIds;
    private Context context;

    public AdsCategoryAdapter(String[] titles, int[] iconIds, Context context) {
        this.titles = titles;
        this.iconIds = iconIds;
        this.context = context;
    }

    @Override
    public int getCount() {
        return iconIds.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CategoryHolder categoryHolder;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.ad_category_unit, null);

            categoryHolder = new CategoryHolder(convertView.findViewById(R.id.ivIcon), convertView.findViewById(R.id.tvTitle));

            convertView.setTag(categoryHolder);

        } else {
            categoryHolder = (CategoryHolder) convertView.getTag();
        }

        try {
            categoryHolder.ivIcon.setImageResource(iconIds[position]);
            categoryHolder.tvTitle.setText(titles[position]);
        } catch (Exception ignored) {
        }

        return convertView;
    }

    private class CategoryHolder {
        private ImageView ivIcon;
        private TextView tvTitle;

        CategoryHolder(ImageView ivIcon, TextView tvTitle) {
            this.ivIcon = ivIcon;
            this.tvTitle = tvTitle;
            this.tvTitle.setTypeface(DemoApp.getOpensans_semiblod());

        }
    }

}