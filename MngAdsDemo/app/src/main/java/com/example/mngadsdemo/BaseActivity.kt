package com.example.mngadsdemo

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.mngadsdemo.DemoApp.Companion.getInterPlacement
import com.example.mngadsdemo.DemoApp.Companion.isAdClicked
import com.example.mngadsdemo.fragment.BannerFragment.Companion.printError
import com.example.mngadsdemo.utils.ApplicationManager
import com.example.mngadsdemo.utils.ApplicationManager.ForegroundListener
import com.example.mngadsdemo.utils.Utils
import com.mngads.MNGAdsFactory
import com.mngads.exceptions.MAdvertiseException
import com.mngads.listener.MNGClickListener
import com.mngads.listener.MNGInterstitialListener
import com.mngads.util.MNGPreference

open class BaseActivity : AppCompatActivity(), ForegroundListener, MNGInterstitialListener,
    MNGClickListener {

    private val TAG: String = javaClass.simpleName
    private var applicationManager: ApplicationManager? = null
    private var mngAdsInterstitialFactory: MNGAdsFactory? = null
    private var mngPreference: MNGPreference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        applicationManager = ApplicationManager.instance
        initializeMNGFactory()
    }

    fun shouldIgnoreResume() {
        applicationManager!!.shouldIgnoreResume()
    }

    protected fun displayInterstitial() {
        // init interstitial placement id
        mngAdsInterstitialFactory!!.setPlacementId(getInterPlacement(this))
        mngAdsInterstitialFactory!!.setInterstitialListener(this)
        mngAdsInterstitialFactory!!.setClickListener(this)
        mngAdsInterstitialFactory!!.loadInterstitial(mngPreference, false)
        Log.d(TAG, "Number of running factory is :" + MNGAdsFactory.getNumberOfRunningFactory())
    }

    private fun initializeMNGFactory() {
        mngAdsInterstitialFactory = MNGAdsFactory(this)
        mngPreference = Utils.getPreference(this)
    }

    override fun onDestroy() {
        if (mngAdsInterstitialFactory != null) {
            mngAdsInterstitialFactory!!.releaseMemory()
            mngAdsInterstitialFactory = null
        }
        super.onDestroy()
    }

    override fun onStart() {
        super.onStart()
        applicationManager!!.onActivityStarted(this, this)
    }

    override fun onStop() {
        applicationManager!!.onActivityStopped(this, this)
        super.onStop()
    }

    override fun onBecameForeground() {
        if (isAdClicked) {
            Log.d(TAG, "on becameForeground ignored  Ad clicked")
            isAdClicked = false
        } else {
            Log.d(TAG, "on becameForeground")
            displayInterstitial()
        }
    }

    override fun onBecameBackground() {
        Log.d(TAG, "onBecameBackground")
    }

    override fun onAdClicked() {
        isAdClicked = true
        Log.d(TAG, "Ad Clicked")
        Toast.makeText(this, "Ad Clicked", Toast.LENGTH_SHORT).show()
    }

    override fun interstitialDidLoad() {
        if (mngAdsInterstitialFactory!!.isInterstitialReady) {
            Log.d(TAG, "Interstitial did load")
            mngAdsInterstitialFactory!!.displayInterstitial()
        }
    }

    override fun interstitialDidFail(e: Exception) {
        val adException = e as MAdvertiseException
        Log.e(
            TAG,
            "Interstitial did fail : " + adException.message + " error code " + adException.errorCode
        )
        try {
            printError(adException.message, applicationContext, "Interstitial did fail : ")
        } catch (ignored: Exception) {
        }
    }

    override fun interstitialDisappear() {
        Log.d(TAG, "Interstitial did disappear")
    }

    override fun interstitialDidShown() {}
}