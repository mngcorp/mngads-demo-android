package com.example.mngadsdemo.fragment

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.mngadsdemo.BaseActivity
import com.example.mngadsdemo.DemoApp
import com.example.mngadsdemo.R
import com.example.mngadsdemo.databinding.FragmentMasBinding
import com.example.mngadsdemo.global.Constants
import com.example.mngadsdemo.utils.Utils
import com.mngads.MAdvertiseRewardedVideo
import com.mngads.MNGAdsFactory
import com.mngads.MNGNativeObject
import com.mngads.exceptions.MAdvertiseLockedPlacementException
import com.mngads.listener.*
import com.mngads.models.MAdvertiseVideoReward
import com.mngads.util.MNGAdSize
import com.mngads.util.MNGDisplayType
import com.mngads.util.MNGFrame
import com.mngads.util.MNGPreference
import com.mngads.views.MAdvertiseNativeContainer

class PerfFragment : Fragment(), View.OnClickListener, MNGClickListener, MNGRefreshListener {

    private lateinit var mFactory: MNGAdsFactory
    private lateinit var mAdContainer: MAdvertiseNativeContainer
    private lateinit var mngPreference: MNGPreference
    private lateinit var mFrame: MNGFrame
    private lateinit var rewardedVideo: MAdvertiseRewardedVideo

    private lateinit var mNativeAdView: View
    private lateinit var mInflater: LayoutInflater

    private lateinit var binding: FragmentMasBinding
    private var isRewardedVideo = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_mas, container, false)
        binding = FragmentMasBinding.bind(view)
        mInflater = LayoutInflater.from(context)
        mAdContainer = binding.masAdContainer
        binding.masRewardedContainer.visibility = View.VISIBLE

        initListener()
        initAds()
        return view
    }

    private fun initListener() {
        binding.masButtonShow.setOnClickListener(this)
        binding.masButtonInterstitial.setOnClickListener(this)
        binding.masButtonSquare.setOnClickListener(this)
        binding.masButtonNativeAd.setOnClickListener(this)
        binding.masButtonBanner.setOnClickListener(this)
        binding.masRewardedContainer.setOnClickListener(this)
    }

    private fun initAds() {
        mngPreference = Utils.getPreference(requireActivity())
        rewardedVideo = MAdvertiseRewardedVideo(activity)
        rewardedVideo.setRewardedVideoListener(rewardedVideoListener)

        mFactory = MNGAdsFactory(activity)
        mFactory.setBannerListener(bannerListener)
        mFactory.setClickListener(this)
        mFactory.setRefreshListener(this)
        mFactory.setInterstitialListener(interstitialListener)
        mFactory.setNativeListener(nativeAdListener)
    }

    private fun handleLoaderVisibility(visible: Boolean) {
        binding.masProgressBar.visibility = if (visible) View.VISIBLE else View.GONE
    }

    override fun onClick(v: View) {
        mAdContainer.removeAllViews()
        mAdContainer.visibility = View.GONE
        binding.masButtonShow.visibility = View.GONE

        when (v.id) {
            R.id.mas_button_interstitial -> loadInterstitial()
            R.id.mas_button_square -> {
                mFrame = MNGAdSize.MNG_MEDIUM_RECTANGLE
                loadBanner()
            }
            R.id.mas_button_banner -> {
                mFrame =
                    if (resources.getBoolean(R.bool.is_tablet)) MNGAdSize.MNG_DYNAMIC_LEADERBOARD else MNGAdSize.MNG_DYNAMIC_BANNER
                loadBanner()
            }
            R.id.mas_button_nativeAd -> loadNativeAd()
            R.id.masRewardedContainer -> createRewardedVideo()
            R.id.mas_button_show -> if (isRewardedVideo) showRewardedVideo() else displayInterstitial()
            else -> {}
        }
    }

    private fun createRewardedVideo() {
        handleLoaderVisibility(visible = true)
        // set Rewarded video placement id
        rewardedVideo.setPlacementId(REWARDED_ID)
        rewardedVideo.loadRewardedVideo(mngPreference)
    }

    private fun showRewardedVideo() {
        Log.d(TAG, rewardedVideo.isRewardedVideoReady.toString())
        if (!rewardedVideo.showRewardedVideo()) {
            Log.d(
                TAG,
                "showRewardedVideo: displaying a rewarded video is not possible, you have to call loadRewardedVideo or wait at least 5s before displaying another one."
            )
            Toast.makeText(
                activity,
                "Wait at least 5s before displaying another one",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun loadBanner() {
        if (mFactory.isBusy) {
            return
        }

        handleLoaderVisibility(visible = true)

        when (mFrame.height) {
            Constants.BANNER_50_HEIGHT -> {
                mFactory.setPlacementId(BANNER_ID)
                mFactory.loadBanner(mFrame, mngPreference)
            }
            Constants.BANNER_90_HEIGHT -> {
                mFactory.setPlacementId(BANNER_ID)
                mFactory.loadBanner(mFrame, mngPreference)
            }
            Constants.SQUARE_AD_HEIGHT -> {
                mFactory.setPlacementId(SQUAREID)
                mFactory.loadBanner(mFrame, mngPreference)
            }
        }
    }

    private fun loadInterstitial() {
        if (mFactory.isBusy) {
            return
        }

        handleLoaderVisibility(visible = true)
        mFactory.setPlacementId(INTER_ID)
        mFactory.loadInterstitial(mngPreference, false)
    }

    private fun displayInterstitial() {
        if (mFactory.isInterstitialReady) {
            (activity as BaseActivity).shouldIgnoreResume()
            mFactory.displayInterstitial()
        } else {
            activity?.let {
                Toast.makeText(it, "Interstitial Not Ready", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun loadNativeAd() {
        if (mFactory.isBusy) {
            return
        }

        handleLoaderVisibility(visible = true)
        mFactory.setPlacementId(NATIVE_ID)
        mFactory.loadNative(mngPreference)
    }

    private fun displayNativeAd(nativeObject: MNGNativeObject, adView: View?, context: Context?) {
        val nativeAdIcon = adView!!.findViewById<ImageView>(R.id.nativeAdIcon)
        val nativeAdTitle = adView.findViewById<TextView>(R.id.nativeAdTitle)
        val nativeAdBody = adView.findViewById<TextView>(R.id.nativeAdBody)
        val nativeBadgeImage = adView.findViewById<ImageView>(R.id.badgeView)
        val nativeAdCallToAction = adView.findViewById<Button>(R.id.nativeAdCallToAction)
        val mediaContainer = adView.findViewById<ViewGroup>(R.id.mediaContainer)
        val callToAction = nativeObject.callToAction
        nativeAdCallToAction.text = callToAction
        nativeAdTitle.text = nativeObject.title
        nativeAdBody.text = nativeObject.body
        nativeBadgeImage.setImageBitmap(nativeObject.getBadge(activity, "Publicité"))
        when (nativeObject.displayType) {
            MNGDisplayType.MNGDisplayTypeAppInstall -> {
                val imageAppInstall = ContextCompat.getDrawable(
                    requireContext(), R.drawable.app_install
                )
                setDisplayType(imageAppInstall, nativeAdCallToAction, context)
            }
            MNGDisplayType.MNGDisplayTypeContent -> {
                val imageContent = ContextCompat.getDrawable(
                    requireContext(), R.drawable.content
                )
                setDisplayType(imageContent, nativeAdCallToAction, context)
            }
            MNGDisplayType.MNGDisplayTypeUnknown -> {}
        }
        nativeObject.registerViewForInteraction(
            mAdContainer,
            mediaContainer,
            nativeAdIcon,
            nativeAdCallToAction
        )
    }

    private fun setDisplayType(
        drawable: Drawable?,
        nativeAdCallToAction: Button,
        context: Context?
    ) {
        context?.let {
            val w = Utils.convertDpToPixel(15f, context).toInt()
            drawable!!.setBounds(0, 0, w, w)
            nativeAdCallToAction.setCompoundDrawables(drawable, null, null, null)
        }
    }

    private val nativeAdListener: MNGNativeListener
        get() = object : MNGNativeListener {

            override fun nativeObjectDidLoad(mngNativeObject: MNGNativeObject) {
                Log.d(TAG, "Native ad did load ")
                handleLoaderVisibility(visible = false)
                mNativeAdView = mInflater.inflate(R.layout.ad_unit, mAdContainer)
                mAdContainer.visibility = View.VISIBLE
                displayNativeAd(mngNativeObject, mNativeAdView, context)
            }

            override fun nativeObjectDidFail(e: Exception) {
                Log.d(TAG, "Native ad failed ")
                handleLoaderVisibility(visible = false)
                activity?.let {
                    Toast.makeText(it, "Interstitial Did Fail", Toast.LENGTH_SHORT).show()
                }
            }
        }

    private val interstitialListener: MNGInterstitialListener
        get() = object : MNGInterstitialListener {

            override fun interstitialDidLoad() {
                Toast.makeText(activity, "interstitialDidLoad", Toast.LENGTH_SHORT).show()
                handleLoaderVisibility(visible = false)
                binding.masButtonShow.visibility = View.VISIBLE
                isRewardedVideo = false
            }

            override fun interstitialDidFail(e: Exception) {
                Toast.makeText(activity, "interstitialDidFail", Toast.LENGTH_SHORT).show()
                handleLoaderVisibility(visible = false)
            }

            override fun interstitialDidShown() {
                Toast.makeText(activity, "Interstitial did shown", Toast.LENGTH_SHORT).show()
            }

            override fun interstitialDisappear() {
                Toast.makeText(activity, "interstitialDisappear", Toast.LENGTH_SHORT).show()
                handleLoaderVisibility(visible = false)
                binding.masButtonShow.visibility = View.GONE
            }
        }

    private val bannerListener: MNGBannerListener
        get() = object : MNGBannerListener {

            override fun bannerDidLoad(view: View, preferredHeightDP: Int) {
                Log.d(TAG, "Banner did load preferred Height $preferredHeightDP dp")
                handleLoaderVisibility(visible = false)
                mAdContainer.visibility = View.VISIBLE
                mAdContainer.addView(view)
                activity?.let {
                    Toast.makeText(it, "Banner did load", Toast.LENGTH_SHORT).show()
                }
            }

            override fun bannerDidFail(e: Exception) {
                handleLoaderVisibility(visible = false)
                activity?.let {
                    Toast.makeText(it, "Banner did fail", Toast.LENGTH_SHORT).show()
                }
            }

            override fun bannerResize(mngFrame: MNGFrame) {
                Log.d(TAG, "Banner did resize w " + mngFrame.width + " h " + mngFrame.height)
                val bannerPreferredHeightPx = Utils.convertDpToPixel(
                    mngFrame.height.toFloat(), activity!!.applicationContext
                ).toInt()
                mAdContainer.layoutParams.height = bannerPreferredHeightPx
                mAdContainer.requestLayout()
            }
        }

    private val rewardedVideoListener: MAdvertiseRewardedVideoListener
        get() = object : MAdvertiseRewardedVideoListener {
            override fun onRewardedVideoError(p0: Exception?) {
                handleLoaderVisibility(visible = false)
                Log.d(TAG, "onRewardedVideoError $p0")
                if (p0 is MAdvertiseLockedPlacementException) {
                    Log.d(
                        TAG,
                        "onRewardedVideoError: this exception occurs when a rewarded video is already loaded"
                    )
                }
            }

            override fun onRewardedVideoLoaded() {
                Log.d(TAG, "onRewardedVideoLoaded")
                handleLoaderVisibility(visible = false)
                binding.masButtonShow.visibility = View.VISIBLE
                isRewardedVideo = true
            }

            override fun onRewardedVideoClicked() {
                Log.d(TAG, "onRewardedVideoClicked")
                handleLoaderVisibility(visible = false)
            }

            override fun onRewardedVideoClosed() {
                Log.d(TAG, "onRewardedVideoClosed")
                handleLoaderVisibility(visible = false)
                binding.masButtonShow.visibility = View.GONE
            }

            override fun onRewardedVideoAppeared() {
                Log.d(TAG, "onRewardedVideoAppeared")
                handleLoaderVisibility(visible = false)
            }

            override fun onRewardedVideoCompleted(p0: MAdvertiseVideoReward?) {
                Log.d(TAG, "onRewardedVideoCompleted")
                handleLoaderVisibility(visible = false)
                binding.masButtonShow.visibility = View.GONE
            }

        }

    override fun onAdClicked() {
        Toast.makeText(activity, "Ad Clicked", Toast.LENGTH_SHORT).show()
        DemoApp.isAdClicked = true
        Log.d(TAG, "Ad Clicked")
    }

    override fun onRefreshSucceed() {}
    override fun onRefreshFailed(e: Exception) {}

    override fun onDestroy() {
        mFactory.releaseMemory()
        super.onDestroy()
    }

    companion object {
        private val TAG = PerfFragment::class.java.simpleName
        const val BANNER_ID = "/5180317/bannermng"
        const val SQUAREID = "/5180317/squaremngperf"
        const val NATIVE_ID = "/5180317/nativeadmng"
        const val INTER_ID = "/5180317/intermng"
        const val REWARDED_ID = "/5180317/videoRewarded"
    }
}