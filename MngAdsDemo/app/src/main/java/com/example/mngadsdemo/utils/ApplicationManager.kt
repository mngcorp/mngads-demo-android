package com.example.mngadsdemo.utils

import android.app.Activity
import android.util.Log

class ApplicationManager {

    interface ForegroundListener {
        fun onBecameForeground()
        fun onBecameBackground()
    }

    private var stateCounter = 0
    private var wasInBackground = false
    private var shouldIgnoreResume = false
    fun shouldIgnoreResume() {
        shouldIgnoreResume = true
    }

    val isForeground: Boolean
        get() = !isBackground
    val isBackground: Boolean
        get() = stateCounter == 0

    fun onActivityStarted(activity: Activity?, listener: ForegroundListener) {
        stateCounter++
        if (wasInBackground) {
            wasInBackground = false
            if (shouldIgnoreResume) {
                shouldIgnoreResume = false
                return
            }
            Log.i(TAG, "went foreground")
            try {
                listener.onBecameForeground()
            } catch (ex: Exception) {
                Log.e(TAG, "Listener threw exception!", ex)
            }
        } else {
            Log.d(TAG, "still foreground")
        }
    }

    fun onActivityStopped(activity: Activity, listener: ForegroundListener) {
        stateCounter--
        if (!activity.isChangingConfigurations) {
            if (isBackground) {
                wasInBackground = true
                if (shouldIgnoreResume) {
                    return
                }
                Log.i(TAG, "went background")
                try {
                    listener.onBecameBackground()
                } catch (ex: Exception) {
                    Log.e(TAG, "Listener threw exception!", ex)
                }
            } else {
                Log.d(TAG, "still foreground")
            }
        } else {
            Log.d(TAG, "isChangingConfigurations")
        }
    }

    companion object {
        val TAG: String = ApplicationManager::class.java.simpleName
        var instance: ApplicationManager? = null
            get() {
                if (field == null) {
                    field = ApplicationManager()
                }
                return field
            }
            private set
    }
}