package com.example.mngadsdemo.adapter.holder;

import android.view.View;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.example.mngadsdemo.R;

public class DataHolder extends RecyclerView.ViewHolder {
    public RelativeLayout container;

    public DataHolder(View view) {
        super(view);
        container = view.findViewById(R.id.container);
    }
}