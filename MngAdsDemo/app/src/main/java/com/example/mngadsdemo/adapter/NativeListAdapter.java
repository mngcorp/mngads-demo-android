package com.example.mngadsdemo.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mngadsdemo.DemoApp;
import com.example.mngadsdemo.R;
import com.example.mngadsdemo.adapter.holder.DataHolder;
import com.example.mngadsdemo.adapter.holder.NativeHolder;
import com.example.mngadsdemo.utils.Utils;
import com.mngads.MNGAdsFactory;
import com.mngads.MNGNativeObject;
import com.mngads.listener.MNGNativeListener;

import java.util.ArrayList;
import java.util.List;

public class NativeListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final FragmentActivity mContext;
    private final boolean mSetCover;
    private List<String> mList;
    private static final int TYPE_ADS_NATIVE = 3;
    private static final int TYPE_NORMAL = 2;

    private static final int ADS_POS = 5;
    private MNGAdsFactory mngAdsNativeAdsFactory;


    public NativeListAdapter(int cont, FragmentActivity activity, boolean setCover) {
        mContext = activity;
        mSetCover = setCover;
        mngAdsNativeAdsFactory = new MNGAdsFactory(activity);
        mList = new ArrayList<>();
        for (int i = 0; i < cont; i++) {
            mList.add(String.valueOf(i));
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_NORMAL) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.section_unit, parent, false);
            return new DataHolder(itemView);
        } else {
            View itemView;
            if (mSetCover) {
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ad_unit_list_with_cover, parent, false);
            } else {
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ad_unit_list, parent, false);
            }
            return new NativeHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof NativeHolder) {
            buildNativeHolder(holder);
        }
    }

    private void buildNativeHolder(RecyclerView.ViewHolder holder) {

        NativeHolder adHolder = (NativeHolder) holder;
        mngAdsNativeAdsFactory.setNativeListener(new MNGNativeListener() {
            @Override
            public void nativeObjectDidLoad(MNGNativeObject mngNativeObject) {
                adHolder.displayNativeAd(mngNativeObject,mSetCover);
            }

            @Override
            public void nativeObjectDidFail(Exception e) {
                adHolder.nativeAdContainer.setVisibility(View.GONE);
                adHolder.bodyLayout.setVisibility(View.GONE);
            }
        });
        mngAdsNativeAdsFactory.setPlacementId(DemoApp.getNativePlacement(mContext));
        if (mSetCover) {
            mngAdsNativeAdsFactory.loadNative(Utils.getPreference(mContext));
        } else {
            mngAdsNativeAdsFactory.loadNativeWithOutCover(Utils.getPreference(mContext));
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (isAdPosition(position) && position > 0) {
            return TYPE_ADS_NATIVE;
        } else {
            return TYPE_NORMAL;
        }
    }

    private boolean isAdPosition(int position) {
        return (position + 1) % (ADS_POS + 1) == 0;
    }


    @Override
    public int getItemCount() {
        return mList.size() + (mList.size() / ADS_POS);
    }
}