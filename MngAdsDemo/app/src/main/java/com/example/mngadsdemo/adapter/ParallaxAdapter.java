package com.example.mngadsdemo.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mngadsdemo.R;
import com.example.mngadsdemo.adapter.holder.DataHolder;
import com.example.mngadsdemo.adapter.holder.NativeHolder;
import com.example.mngadsdemo.adapter.holder.ParallaxHolder;
import com.mngads.MNGNativeObject;

import java.util.ArrayList;
import java.util.List;

public class ParallaxAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<String> mParallaxList;
    private View mAdsView;


    public static final int ADS = 1;
    public static final int NORMAL = 2;

    public void setAdView(View view) {
        mAdsView = view;
        notifyDataSetChanged();
    }


    public ParallaxAdapter(int cont) {
        mParallaxList = new ArrayList<String>();
        for (int i = 0; i < cont; i++) {
            mParallaxList.add(String.valueOf(i));
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == NORMAL) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.section_unit, parent, false);
            return new DataHolder(itemView);
        } else {
            View pubView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ad_unit_paralex, parent, false);
            return new ParallaxHolder(pubView);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ParallaxHolder) {
            buildParallaxHolder(holder);
        }
    }



    private void buildParallaxHolder(RecyclerView.ViewHolder holder) {
        if (mAdsView != null) {
            if (mAdsView.getParent() != null) {
                ((ViewGroup) mAdsView.getParent()).removeView(mAdsView);
            }
            ParallaxHolder VHheader = (ParallaxHolder) holder;
            VHheader.getContainerAds().addView(mAdsView);
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (mAdsView != null) {
            return getParalexType(position);
        }
        else {
            return NORMAL;
        }
    }

    private int getParalexType(int position) {
        if (position == 7) {
            return ADS;
        } else {
            return NORMAL;
        }
    }



    @Override
    public int getItemCount() {
        if (mAdsView != null) {
            return mParallaxList.size() + 1;
        }
        return mParallaxList.size();

    }
}