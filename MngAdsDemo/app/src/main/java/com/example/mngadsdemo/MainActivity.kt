package com.example.mngadsdemo

import android.content.Intent
import android.media.AudioManager
import android.media.AudioManager.OnAudioFocusChangeListener
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.example.mngadsdemo.DemoApp.Companion.opensans_semiblod
import com.example.mngadsdemo.adapter.AdsCategoryAdapter
import com.example.mngadsdemo.fragment.*
import com.example.mngadsdemo.global.Constants
import com.example.mngadsdemo.listenter.ActionBarListener
import com.example.mngadsdemo.utils.Utils
import com.madvertise.cmp.global.ConsentToolConfiguration
import com.madvertise.cmp.manager.ConsentManager
import java.io.IOException
import java.util.*

class MainActivity : BaseActivity(), View.OnClickListener, OnItemClickListener {

    private val TAG = MainActivity::class.java.simpleName

    private var mRadioStatus = RADIO_STOP
    private lateinit var ibDrawer: ImageButton
    private lateinit var ibParams: ImageButton
    private lateinit var tvTitle: TextView
    private lateinit var fragmentTitle: Array<String>
    private var fragmentPosition = 0
    private var actualFragment: Fragment? = null
    private var appsFireFragment: AppsFireFragment? = null
    private var bannerFragment: BannerFragment? = null
    private var infeedFragment: InfeedFragment? = null
    private var interstitialFragment: InterstitialFragment? = null
    private var nativeAdFragment: NativeAdFragment? = null
    private var carouselFragment: CarouselFragment? = null
    private var adsFragment: AdsFragment? = null
    private var settingsFragment: SettingsFragment? = null
    private var mSwipeFragment: SwipeFragment? = null
    private var mRewardedVideoFragment: RewardedVideoFragment? = null
    private var paramsOn = true
    private var mDrawer: DrawerLayout? = null
    private var masFragment: PerfFragment? = null
    private var mAudioManager: AudioManager? = null
    private var mMediaPlayer: MediaPlayer? = null
    private lateinit var mRadioButton: Button
    private var mAudioListener: OnAudioFocusChangeListener? = null
    private var mMediaPrepared = false
    private var mDFPFragment: DFPFragment? = null
    private var mParallaxFragment: ParallaxFragment? = null
    private var nativeListFragment: NativeListFragment? = null
    private var mThumbnailFragment: ThumbnailFragment? = null
    private var mSyncFragment: SyncFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fragmentPosition = 3
        fragmentTitle = resources.getStringArray(R.array.ADS_CATEGORY)
        initializeActionBar()
        initializeSlidingMenu()
        updateTitle()
        mRadioButton = findViewById(R.id.main_radio_btn)
        mRadioButton.setOnClickListener(this)
        bannerFragment = BannerFragment()
        actualFragment = bannerFragment
        showFragment(actualFragment)
        Handler(Looper.getMainLooper()) {
            setupRadioPlayer()
            false
        }
    }

    private fun setupRadioPlayer() {
        mAudioManager = getSystemService(AUDIO_SERVICE) as AudioManager
        mAudioListener = OnAudioFocusChangeListener { focusChange ->
            when (focusChange) {
                AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> pausePlayer(true)
                AudioManager.AUDIOFOCUS_GAIN -> startPlayer()
                AudioManager.AUDIOFOCUS_LOSS -> if (mMediaPlayer != null) {
                    mMediaPlayer!!.stop()
                    mMediaPlayer = null
                }
            }
        }
        mMediaPlayer = MediaPlayer()
        val myUri = Uri.parse(RADIO_URL)
        try {
            mMediaPlayer!!.setDataSource(this, myUri)
            mMediaPlayer!!.setAudioStreamType(AudioManager.STREAM_MUSIC)
            mMediaPlayer!!.setOnPreparedListener {
                mMediaPrepared = true
                mRadioButton.visibility = View.VISIBLE
            }
            mMediaPlayer!!.prepare()
            Log.d(TAG, "music prepared")
        } catch (e: IOException) {
            Log.d(TAG, "error playing music: $e")
        }
    }

    private fun initializeActionBar() {
        val toolbar = findViewById<Toolbar>(R.id.tool_bar)
        ibDrawer = toolbar.findViewById(R.id.ibDrawer)
        ibParams = toolbar.findViewById(R.id.ibParams)
        tvTitle = toolbar.findViewById(R.id.tvTitle)
        tvTitle.typeface = opensans_semiblod
        setSupportActionBar(toolbar)
        ibDrawer.setOnClickListener(this)
        ibParams.setOnClickListener(this)
    }

    private fun initializeSlidingMenu() {
        mDrawer = findViewById(R.id.main_drawer_layout)
        val drawerList = findViewById<ListView>(R.id.main_drawer_list)
        val adsCategoryAdapter = AdsCategoryAdapter(
            resources.getStringArray(R.array.ADS_CATEGORY),
            Constants.categoryIcon,
            applicationContext
        )
        drawerList.adapter = adsCategoryAdapter
        drawerList.onItemClickListener = this
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ibDrawer -> mDrawer!!.openDrawer(GravityCompat.START)
            R.id.ibParams -> toggleParams()
            R.id.main_radio_btn -> if (mRadioStatus == RADIO_START) {
                pausePlayer(false)
            } else {
                startPlayer()
            }
            else -> {}
        }
    }

    private fun pausePlayer(temp: Boolean) {
        if (mMediaPlayer != null && mMediaPrepared) {
            if (temp) {
                mRadioButton.setText(R.string.temp_stop_radio_text)
            } else {
                mRadioButton.setText(R.string.start_radio_text)
                mAudioManager!!.abandonAudioFocus(mAudioListener)
            }
            mMediaPlayer!!.pause()
            mRadioStatus = RADIO_STOP
        }
    }

    private fun startPlayer() {
        if (mMediaPlayer != null && mMediaPrepared) {
            mRadioButton.setText(R.string.stop_radio_text)
            try {
                mAudioManager!!.requestAudioFocus(
                    mAudioListener,
                    AudioManager.STREAM_MUSIC,
                    AudioManager.AUDIOFOCUS_GAIN_TRANSIENT
                )
                mMediaPlayer!!.start()
                mRadioStatus = RADIO_START
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun configCMPTool(isPopup: Boolean) {
        val configRes: Int = when (Locale.getDefault().isO3Language) {
            "eng" -> R.raw.madvertise_config_en
            "ita" -> R.raw.madvertise_config_it
            "deu" -> R.raw.madvertise_config_de
            else -> R.raw.madvertise_config_fr
        }
        if (isPopup) {
            ConsentManager.sharedInstance.configure(
                this@MainActivity.application, Integer.valueOf(
                    Utils.getAppId(this@MainActivity)
                ), ConsentToolConfiguration(configRes), "FR", true
            )
        } else {
            ConsentManager.sharedInstance
                .configure(
                    this@MainActivity.application,
                    Integer.valueOf(Utils.getAppId(this@MainActivity)),
                    ConsentToolConfiguration(configRes)
                        .setConsentToolSize(
                            ConsentToolConfiguration.MATCH_PARENT,
                            ConsentToolConfiguration.MATCH_PARENT
                        ),
                    "FR",
                    true
                )
        }
    }

    override fun onItemClick(adapterView: AdapterView<*>?, view: View, position: Int, l: Long) {
        mDrawer!!.closeDrawer(GravityCompat.START)
        if (position == fragmentPosition || position < 0) {
            return
        } else if (position == 18) {
            val i = Intent(this, ReadMeActivity::class.java)
            startActivity(i)
            return
        } else if (position == 17) {
            val i = Intent(this, DailymotionActivity::class.java)
            startActivity(i)
            return
        } else if (position == 0) {
            configCMPTool(false)
            ConsentManager.sharedInstance.openCMP(this@MainActivity)
            return
        }
        when (position) {
            1 -> {
                if (masFragment == null) {
                    masFragment = PerfFragment()
                }
                ibParams.visibility = View.INVISIBLE
                actualFragment = masFragment
            }
            2 -> {
                if (appsFireFragment == null) {
                    appsFireFragment = AppsFireFragment()
                }
                ibParams.visibility = View.INVISIBLE
                actualFragment = appsFireFragment
            }
            3 -> {
                if (bannerFragment == null) {
                    bannerFragment = BannerFragment()
                }
                ibParams.isEnabled = true
                ibParams.visibility = View.VISIBLE
                actualFragment = bannerFragment
            }
            4 -> {
                if (infeedFragment == null) {
                    infeedFragment = InfeedFragment()
                }
                ibParams.isEnabled = true
                ibParams.visibility = View.INVISIBLE
                actualFragment = infeedFragment
            }
            5 -> {
                if (interstitialFragment == null) {
                    interstitialFragment = InterstitialFragment()
                }
                ibParams.isEnabled = true
                ibParams.visibility = View.VISIBLE
                actualFragment = interstitialFragment
            }
            6 -> {
                if (nativeAdFragment == null) {
                    nativeAdFragment = NativeAdFragment()
                }
                ibParams.isEnabled = false
                ibParams.visibility = View.INVISIBLE
                actualFragment = nativeAdFragment
            }
            7 -> {
                if (nativeListFragment == null) {
                    nativeListFragment = NativeListFragment()
                }
                ibParams.isEnabled = false
                ibParams.visibility = View.INVISIBLE
                actualFragment = nativeListFragment
            }
            8 -> {
                if (mRewardedVideoFragment == null) {
                    mRewardedVideoFragment = RewardedVideoFragment()
                }
                ibParams.isEnabled = false
                ibParams.visibility = View.INVISIBLE
                actualFragment = mRewardedVideoFragment
            }
            9 -> {
                if (mThumbnailFragment == null) {
                    mThumbnailFragment = ThumbnailFragment()
                }
                ibParams.isEnabled = false
                ibParams.visibility = View.INVISIBLE
                actualFragment = mThumbnailFragment
            }
            10 -> {
                if (mSyncFragment == null) {
                    mSyncFragment = SyncFragment.newInstance()
                }
                ibParams.isEnabled = true
                ibParams.visibility = View.INVISIBLE
                actualFragment = mSyncFragment
            }
            11 -> {
                if (mParallaxFragment == null) {
                    mParallaxFragment = ParallaxFragment()
                }
                ibParams.isEnabled = false
                ibParams.visibility = View.INVISIBLE
                actualFragment = mParallaxFragment
            }
            12 -> {
                if (carouselFragment == null) {
                    carouselFragment = CarouselFragment()
                }
                ibParams.isEnabled = false
                ibParams.visibility = View.INVISIBLE
                actualFragment = carouselFragment
            }
            13 -> {
                if (adsFragment == null) {
                    adsFragment = AdsFragment()
                }
                ibParams.isEnabled = false
                ibParams.visibility = View.INVISIBLE
                actualFragment = adsFragment
            }
            14 -> {
                if (mSwipeFragment == null) {
                    mSwipeFragment = SwipeFragment()
                }
                ibParams.isEnabled = false
                ibParams.visibility = View.INVISIBLE
                actualFragment = mSwipeFragment
            }
            15 -> {
                if (mDFPFragment == null) {
                    mDFPFragment = DFPFragment()
                }
                ibParams.isEnabled = false
                ibParams.visibility = View.INVISIBLE
                actualFragment = mDFPFragment
            }
            16 -> {
                if (settingsFragment == null) {
                    settingsFragment = SettingsFragment()
                }
                ibParams.isEnabled = false
                ibParams.visibility = View.INVISIBLE
                actualFragment = settingsFragment
            }
            else -> {}
        }
        showFragment(actualFragment)
        paramsOn = true
        ibParams.setImageResource(R.drawable.ic_close)
        fragmentPosition = position
        updateTitle()
    }

    private fun toggleParams() {
        if (paramsOn) {
            ibParams.setImageResource(R.drawable.params_off)
            ibDrawer.setImageResource(R.drawable.drawer_off)
        } else {
            ibParams.setImageResource(R.drawable.ic_close)
        }
        (actualFragment as ActionBarListener?)!!.switchParams(paramsOn)
        paramsOn = !paramsOn
    }

    private fun updateTitle() {
        tvTitle.text = fragmentTitle[fragmentPosition]
    }

    private fun showFragment(fragment: Fragment?) {
        if (fragment == null) return
        val fm = supportFragmentManager
        val ft = fm.beginTransaction()
        ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
        ft.replace(R.id.container, fragment)
        ft.commit()
    }

    override fun onDestroy() {
        if (mMediaPlayer != null) {
            if (mMediaPlayer!!.isPlaying) {
                mMediaPlayer!!.stop()
            }
            mMediaPlayer = null
            if (mAudioManager != null) {
                mAudioManager!!.abandonAudioFocus(mAudioListener)
            }
            mAudioManager = null
        }
        super.onDestroy()
    }

    companion object {
        const val RADIO_URL = "http://www.skyrock.fm/stream.php/tunein16_128mp3.mp3"
        private const val RADIO_START = 10
        private const val RADIO_STOP = 20
    }
}