package com.example.mngadsdemo.fragment

import com.example.mngadsdemo.utils.Utils.getCurrentLocation
import com.example.mngadsdemo.utils.Utils.getAppId
import com.example.mngadsdemo.utils.Utils.putAppId
import android.widget.TextView.OnEditorActionListener
import androidx.appcompat.widget.SwitchCompat
import com.example.mngadsdemo.DemoApp
import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import com.example.mngadsdemo.R
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.madvertise.cmp.manager.ConsentManager
import com.madvertise.cmp.listener.OnConsentProvidedListener
import com.madvertise.cmp.listener.OnPrivacyPolicyRequestedListener
import android.content.Intent
import android.preference.PreferenceManager
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.*
import androidx.fragment.app.Fragment
import com.example.mngadsdemo.ReadMeActivity
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import com.madvertise.cmp.listener.OnTCFConsentStringDidChange
import com.madvertise.cmp.global.ConsentToolConfiguration
import com.mngads.MNGAdsFactory
import java.io.IOException
import java.lang.Exception
import java.util.*

class SettingsFragment : Fragment(), View.OnClickListener, CompoundButton.OnCheckedChangeListener,
    OnEditorActionListener {

    private val TAG = SettingsFragment::class.java.simpleName

    private var mAppIdTxt: EditText? = null
    private var mExPrIdTxt: EditText? = null
    private var mExVeIdTxt: EditText? = null
    private var mVeIdTxt: EditText? = null
    private var mValidateBtn: Button? = null
    private var mTextViewConsentString: TextView? = null
    private var mTextViewGAID: TextView? = null
    private var mTextViewGoogle: TextView? = null
    private var mLocation: TextView? = null
    private var mDebugSwitch: SwitchCompat? = null
    private var mApp: DemoApp? = null
    private var advertisingId: String? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_settings, container, false)
        mApp = requireActivity().application as DemoApp
        mValidateBtn = view.findViewById(R.id.validate)
        mValidateBtn?.setOnClickListener(this)
        mAppIdTxt = view.findViewById(R.id.app_id_txt)
        mExPrIdTxt = view.findViewById(R.id.ed_pr_id)
        mExVeIdTxt = view.findViewById(R.id.ed_ex_ve_id)
        mVeIdTxt = view.findViewById(R.id.ed_ve_id)
        mLocation = view.findViewById(R.id.tv_location)
        mAppIdTxt?.setOnEditorActionListener(this)
        mDebugSwitch = view.findViewById(R.id.debug_switch)
        mTextViewConsentString = view.findViewById(R.id.tv_consent_string)
        mTextViewGAID = view.findViewById(R.id.tv_gaid)
        mTextViewGoogle = view.findViewById(R.id.tv_google)
        mDebugSwitch?.setOnCheckedChangeListener(this)

        // Read GDPR ConsentString value from SharedPreferences and display it on the screen.
        advertisingId = context?.resources?.getString(R.string.gaid_place_holder)
        // get android-id from Advertising id client
        object : Thread() {
            override fun run() {
                try {
                    advertisingId = AdvertisingIdClient.getAdvertisingIdInfo(
                        (context)!!
                    ).id
                    mTextViewGAID?.post { mTextViewGAID?.text = advertisingId }
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: GooglePlayServicesNotAvailableException) {
                    e.printStackTrace()
                } catch (e: GooglePlayServicesRepairableException) {
                    e.printStackTrace()
                }
            }
        }.start()

        getCurrentLocation((activity)!!)?.let {
            mLocation?.text = "Latitude = " + it.latitude + "et Longitude = " + it.longitude
        }?: kotlin.run { mLocation?.text = "No Location" }

        view.findViewById<View>(R.id.gdpr_settings_location_fullscreen).setOnClickListener {
            val context = context
            if (context != null) {
                try {
                    configCMPTool(false)
                    ConsentManager.sharedInstance.openCMP(
                        (activity)!!,
                        object : OnConsentProvidedListener {
                            override fun consentProvided(actionType: String) {
                                if (activity != null) Toast.makeText(
                                    activity,
                                    "Action Type = $actionType",
                                    Toast.LENGTH_LONG
                                ).show()
                            }

                            override fun consentFailed(error: String) {
                                if (activity != null) Toast.makeText(
                                    activity,
                                    "Failed$error",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        })
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        view.findViewById<View>(R.id.gdpr_settings_location_popup)
            .setOnClickListener {
                val context = context
                if (context != null) {
                    try {
                        configCMPTool(true)
                        ConsentManager.sharedInstance.openCMP(
                            (activity)!!,
                            object : OnConsentProvidedListener {
                                override fun consentProvided(actionType: String) {
                                    if (activity != null) Toast.makeText(
                                        activity,
                                        "Action Type = $actionType",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }

                                override fun consentFailed(error: String) {
                                    if (activity != null) Toast.makeText(
                                        activity,
                                        "Failed$error",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            })
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        view.findViewById<View>(R.id.btn_add_ex).setOnClickListener { getIds(true) }
        view.findViewById<View>(R.id.btn_drop_ex).setOnClickListener { getIds(false) }
        ConsentManager.sharedInstance.setOnPrivacyPolicyRequestedListener(object :
            OnPrivacyPolicyRequestedListener {
            override fun onPrivacyPolicyRequested(url: String) {
                val i = Intent(activity, ReadMeActivity::class.java)
                startActivity(i)
            }
        })
        ConsentManager.sharedInstance.setOnTCFConsentStringDidChange(object :
            OnTCFConsentStringDidChange {
            override fun didAcceptAllTcfConsentString() {
                Toast.makeText(activity, "Accept All TCF ConsentString", Toast.LENGTH_LONG).show()
            }

            override fun didRefuseAllTcfConsentString() {
                Toast.makeText(activity, "Refuse All TCF ConsentString", Toast.LENGTH_LONG).show()
            }
        })
        view.findViewById<View>(R.id.btn_accept).setOnClickListener {
            ConsentManager.sharedInstance.acceptIABConsent((activity)!!)
            val consentString: String? = PreferenceManager.getDefaultSharedPreferences(
                requireActivity().applicationContext
            ).getString(IABTCF_TCString, "No consent string stored yet.")
            mTextViewConsentString?.text = consentString
            val addtlConsent: String? = PreferenceManager.getDefaultSharedPreferences(
                requireActivity().applicationContext
            ).getString(IABTCF_AddtlConsent, "No Google’s Additional Consent stored yet.")
            mTextViewGoogle?.text = addtlConsent
        }
        view.findViewById<View>(R.id.btn_external_purposes)
            .setOnClickListener {
                Toast.makeText(
                    activity,
                    "External Purposes IDs = " + ConsentManager.sharedInstance.getExternalPurposesIDs(
                        (activity)!!
                    ),
                    Toast.LENGTH_LONG
                ).show()
            }
        view.findViewById<View>(R.id.btn_purposes)
            .setOnClickListener {
                Toast.makeText(
                    activity, "Purposes IDs = " + ConsentManager.sharedInstance.getPurposesIDs(
                        (activity)!!
                    ), Toast.LENGTH_LONG
                ).show()
            }
        view.findViewById<View>(R.id.btn_refuse).setOnClickListener {
            ConsentManager.sharedInstance.refuseIABConsent((activity)!!)
            val consentString: String? = PreferenceManager.getDefaultSharedPreferences(
                requireActivity().applicationContext
            ).getString(IABTCF_TCString, "No consent string stored yet.")
            mTextViewConsentString?.text = consentString
            val addtlConsent: String? = PreferenceManager.getDefaultSharedPreferences(
                requireActivity().applicationContext
            ).getString(IABTCF_AddtlConsent, "No Google’s Additional Consent stored yet.")
            mTextViewGoogle?.text = addtlConsent
        }
        return view
    }

    private fun getIds(isAdd: Boolean) {
        val listExternalProID: MutableList<Int> = ArrayList()
        listExternalProID.add(2)
        val listExternalVenID: MutableList<Int> = ArrayList()
        listExternalVenID.add(8)
        listExternalVenID.add(9)
        val listVenID: MutableList<Int> = ArrayList()
        listVenID.add(23)
        ConsentManager.sharedInstance.updateExternalIDs(
            requireActivity(),
            listExternalProID,
            listExternalVenID,
            listVenID,
            isAdd
        )
    }

    /**
     * consent tool configuration,
     *
     * @param isPopup, true: the dialog is showing with popup mode, false: the dialog is showing with fullscreen
     */
    private fun configCMPTool(isPopup: Boolean) {
        val configRes: Int = when (Locale.getDefault().isO3Language) {
            "eng" -> R.raw.madvertise_config_en
            "ita" -> R.raw.madvertise_config_it
            "deu" -> R.raw.madvertise_config_de
            else -> R.raw.madvertise_config_fr
        }
        if (isPopup) {
            /**
             * consent tool location implementation with default configuration
             */
            ConsentManager.sharedInstance.configure(
                requireActivity().application, Integer.valueOf(
                    getAppId(
                        requireActivity()
                    )
                ), ConsentToolConfiguration(configRes), "FR", true
            )
        } else {
            /**
             * consent tool location implementation with custom configuration:
             * setConsentToolSize() to modify dialog size
             */
            ConsentManager.sharedInstance
                .configure(
                    activity!!.application, Integer.valueOf(
                        getAppId(
                            activity!!
                        )
                    ), ConsentToolConfiguration(configRes)
                        .setConsentToolSize(
                            ConsentToolConfiguration.MATCH_PARENT,
                            ConsentToolConfiguration.MATCH_PARENT
                        ), "FR", true
                )
        }
    }

    override fun onClick(v: View) {
        updateAppId()
    }

    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        if (!isChecked) {
            mDebugSwitch!!.text = getString(R.string.enable_debug)
            val context = context
            if (context != null) {
                Toast.makeText(context, "Debug mode is disabled ", Toast.LENGTH_SHORT).show()
            }
        } else {
            mDebugSwitch!!.text = getString(R.string.disable_debug)
            val context = context
            if (context != null) {
                Toast.makeText(context, "Debug mode is enabled", Toast.LENGTH_SHORT).show()
            }
        }
        MNGAdsFactory.setDebugModeEnabled(isChecked)
    }

    override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent): Boolean {
        updateAppId()
        return false
    }

    private fun updateAppId() {
        if (mAppIdTxt!!.text != null && !mAppIdTxt!!.text.toString().trim { it <= ' ' }.isEmpty()) {
            putAppId((mApp)!!, mAppIdTxt!!.text.toString().trim { it <= ' ' })
            MNGAdsFactory.initialize(mApp, mAppIdTxt!!.text.toString().trim { it <= ' ' })
        } else {
            Toast.makeText(activity!!.applicationContext, "Enter your app id", Toast.LENGTH_SHORT)
                .show()
        }
    }

    override fun onResume() {
        super.onResume()
        mAppIdTxt!!.setText(getAppId((mApp)!!))
        val consentString = PreferenceManager.getDefaultSharedPreferences(
            activity!!.applicationContext
        ).getString(IABTCF_TCString, "No consent string stored yet.")
        val addtlConsent = PreferenceManager.getDefaultSharedPreferences(
            activity!!.applicationContext
        ).getString(IABTCF_AddtlConsent, "No Google’s Additional Consent stored yet.")
        mTextViewConsentString!!.text = consentString
        mTextViewGoogle!!.text = addtlConsent
        Log.e("consentString", consentString + "")
    }

    companion object {
        private const val IABTCF_TCString = "IABTCF_TCString"
        private const val IABTCF_AddtlConsent = "IABTCF_AddtlConsent"
    }
}