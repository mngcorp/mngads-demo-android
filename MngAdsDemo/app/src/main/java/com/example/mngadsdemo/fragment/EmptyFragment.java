package com.example.mngadsdemo.fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mngadsdemo.R;


public class EmptyFragment extends Fragment {

    public static final String ARG_SECTION_NUMBER = "section_number";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_swipe_section, container, false);
        Bundle args = getArguments();
        ((TextView) rootView.findViewById(R.id.section_txt)).setText(
                getString(R.string.swipe_section_text, args.getInt(ARG_SECTION_NUMBER)));
        return rootView;
    }
}