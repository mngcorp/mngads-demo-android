package com.example.mngadsdemo.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mngadsdemo.R
import com.example.mngadsdemo.adapter.NativeListAdapter
import com.example.mngadsdemo.listenter.ActionBarListener
import com.mngads.MNGAdsFactory

class NativeListFragment : Fragment(), ActionBarListener {

    private var mRecycler: RecyclerView? = null
    private var mNativeListAdapter: NativeListAdapter? = null
    private var mngAdsNativeAdsFactory: MNGAdsFactory? = null
    private var mSetCover = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.native_list, container, false)
        initView(view)
        initTypeShow(view)
        initRecycle()
        return view
    }

    private fun initView(view: View) {
        mRecycler = view.findViewById(R.id.recycler)
    }

    private fun initTypeShow(view: View) {
        view.findViewById<View>(R.id.rlnoSizeContainer).setOnClickListener { v: View? ->
            mSetCover = false
            initRecycle()
        }
        view.findViewById<View>(R.id.rlWithSizeContainer).setOnClickListener { v: View? ->
            mSetCover = true
            initRecycle()
        }
    }

    private fun initRecycle() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity)
        mNativeListAdapter = NativeListAdapter(20, activity, mSetCover)
        mRecycler!!.layoutManager = layoutManager
        mRecycler!!.itemAnimator = DefaultItemAnimator()
        mRecycler!!.adapter = mNativeListAdapter
    }

    override fun onDestroy() {
        mngAdsNativeAdsFactory?.releaseMemory()
        mngAdsNativeAdsFactory = null
        super.onDestroy()
    }

    override fun switchParams(isOn: Boolean) {}
}