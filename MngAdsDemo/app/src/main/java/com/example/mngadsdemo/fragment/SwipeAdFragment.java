package com.example.mngadsdemo.fragment;

import android.content.res.Configuration;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.mngadsdemo.DemoApp;
import com.example.mngadsdemo.R;
import com.example.mngadsdemo.utils.Utils;
import com.mngads.MNGAdsFactory;
import com.mngads.MNGNativeObject;
import com.mngads.exceptions.MAdvertiseException;
import com.mngads.listener.MNGClickListener;
import com.mngads.listener.MNGNativeListener;
import com.mngads.util.MNGPreference;
import com.mngads.views.MAdvertiseNativeContainer;

public class SwipeAdFragment extends Fragment implements MNGNativeListener, MNGClickListener {
    private final String TAG = SwipeAdFragment.class.getSimpleName();
    private static final float ratio = 0.6f;
    private MNGAdsFactory mNativeFactory;
    private MNGPreference mngPreference;
    private MAdvertiseNativeContainer rootView;
    private int mScreenWidth;
    private FrameLayout mDummyScreenShotView;
    private ProgressBar mProgressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (MAdvertiseNativeContainer) inflater.inflate(R.layout.fragment_swipe_ad, container, false);
        initView();
        resizeScreenShot();
        initFactory();
        createNativeAd();
        return rootView;
    }

    private void initView() {
        mScreenWidth = getResources().getDisplayMetrics().widthPixels;
        mDummyScreenShotView = rootView.findViewById(R.id.dummy_view);
        mProgressBar = rootView.findViewById(R.id.progressBar);
    }

    private void initFactory() {
        mNativeFactory = new MNGAdsFactory(getActivity());
        mNativeFactory.setNativeListener(this);
        mNativeFactory.setClickListener(this);
        mNativeFactory.setPlacementId(DemoApp.getNativePlacement(getActivity()));
        mngPreference = Utils.getPreference(getActivity());
    }

    private void resizeScreenShot() {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            mDummyScreenShotView.getLayoutParams().height = (int) (mScreenWidth * ratio);
            mDummyScreenShotView.requestLayout();
        }
    }

    private void createNativeAd() {
        if (!mNativeFactory.isBusy()) {
            mNativeFactory.setPlacementId(DemoApp.getNativePlacement(getContext()));
            mProgressBar.setVisibility(View.VISIBLE);
            mNativeFactory.loadNative(mngPreference);
        } else {
            Log.d(TAG, "mngAdsNativeAdsFactory is busy");
        }
    }

    @Override
    public void nativeObjectDidLoad(MNGNativeObject mngNativeObject) {
        mProgressBar.setVisibility(View.GONE);
        displayNativeAd(mngNativeObject);
    }


    private void displayNativeAd(MNGNativeObject mngNativeObject) {

        ImageView mAdIconImageView = rootView.findViewById(R.id.ad_icon);

        Button mAdCallToAction = rootView.findViewById(R.id.call_to_action);
        mAdCallToAction.setVisibility(View.VISIBLE);
        mAdCallToAction.setText(mngNativeObject.getCallToAction());
        mngNativeObject.registerViewForInteraction(rootView, mDummyScreenShotView, mAdIconImageView, mAdCallToAction);

        TextView mAdTitleTextView = rootView.findViewById(R.id.ad_title);
        mAdTitleTextView.setText(mngNativeObject.getTitle());

        TextView mAdBodyTextView = rootView.findViewById(R.id.ad_body);
        mAdBodyTextView.setText(mngNativeObject.getBody());

        ImageView mAdBadgeImageView = rootView.findViewById(R.id.ad_badge_View);
        mAdBadgeImageView.setImageBitmap(mngNativeObject.getBadge());

    }

    @Override
    public void nativeObjectDidFail(Exception e) {
        MAdvertiseException adException = (MAdvertiseException) e;
        Log.e(TAG, "native did fail : " + adException.getMessage() + " error code " + adException.getErrorCode());
        mProgressBar.setVisibility(View.GONE);


    }

    @Override
    public void onAdClicked() {

    }

    @Override
    public void onDestroy() {
        if (mNativeFactory != null) {
            mNativeFactory.releaseMemory();
            mNativeFactory = null;
        }
        super.onDestroy();
    }


}
