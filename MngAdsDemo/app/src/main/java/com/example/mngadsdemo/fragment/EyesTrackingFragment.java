package com.example.mngadsdemo.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.mngadsdemo.BaseActivity;
import com.example.mngadsdemo.R;
import com.mngads.MNGAdsFactory;
import com.mngads.listener.MNGInterstitialListener;


public class EyesTrackingFragment extends Fragment implements View.OnClickListener, MNGInterstitialListener {
    public static final String EYE_TRACKING_APP_ID = "1981170";

    private final String TAG = EyesTrackingFragment.class.getSimpleName();

    private final static String PLACEMENT_DEMO_MRAID = "/1981170/demo";
    private final static String PLACEMENT_SHOWCASE = "/1981170/showcase";
    private MNGAdsFactory mngAdsInterstitialAdsFactory;
    private View mProgress;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mngAdsInterstitialAdsFactory = new MNGAdsFactory(context);
        mngAdsInterstitialAdsFactory.setInterstitialListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_eyes_tracking, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgress = view.findViewById(R.id.eyes_track_progress);
        view.findViewById(R.id.eyes_track_demo).setOnClickListener(this);
        view.findViewById(R.id.eyes_track_showcase).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (MNGAdsFactory.isInitialized()) {

            mProgress.setVisibility(View.VISIBLE);
            switch (view.getId()) {
                case R.id.eyes_track_demo:

                    mngAdsInterstitialAdsFactory.setPlacementId(PLACEMENT_DEMO_MRAID);
                    mngAdsInterstitialAdsFactory.loadInterstitial(null, false);


                    break;
                case R.id.eyes_track_showcase:

                    mngAdsInterstitialAdsFactory.setPlacementId(PLACEMENT_SHOWCASE);
                    mngAdsInterstitialAdsFactory.loadInterstitial(null, false);
                    break;
            }
        }
    }

    @Override
    public void interstitialDidLoad() {
        mProgress.setVisibility(View.GONE);
        if (getActivity() != null) {
            Toast.makeText(getActivity().getApplicationContext(), "EyesTracking Interstitial did load", Toast.LENGTH_SHORT).show();
        }
        displayInterstitial();
    }

    private void displayInterstitial() {

        if (mngAdsInterstitialAdsFactory.isInterstitialReady()) {
            ((BaseActivity) getActivity()).shouldIgnoreResume();
            mngAdsInterstitialAdsFactory.displayInterstitial();

        } else {

            Log.d(TAG, "EyesTracking Interstitial not ready ");
        }
    }

    @Override
    public void interstitialDidFail(Exception e) {
        mProgress.setVisibility(View.GONE);
        if (getActivity() != null) {
            Toast.makeText(getActivity().getApplicationContext(), "EyesTracking Interstitial did fail", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void interstitialDisappear() {
        mProgress.setVisibility(View.GONE);
            if (getActivity() != null) {
                Toast.makeText(getActivity().getApplicationContext(), "EyesTracking Interstitial did disappear", Toast.LENGTH_SHORT).show();
            }

    }


    @Override
    public void interstitialDidShown() {
        Toast.makeText(getActivity(), "Interstitial did shown", Toast.LENGTH_SHORT).show();

    }
}
