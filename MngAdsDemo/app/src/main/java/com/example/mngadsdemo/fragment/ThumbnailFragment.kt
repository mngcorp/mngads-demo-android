package com.example.mngadsdemo.fragment

import com.example.mngadsdemo.DemoApp.Companion.getThumbnailPlacement
import com.example.mngadsdemo.DemoApp.Companion.isAdClicked
import com.example.mngadsdemo.listenter.ActionBarListener
import com.mngads.listener.MNGClickListener
import com.mngads.listener.BluestackThumbnailListener
import com.mngads.MNGAdsFactory
import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ListView
import com.example.mngadsdemo.R
import com.mngads.util.MNGPreference
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.mngadsdemo.adapter.ListAdapter
import java.lang.Exception

class ThumbnailFragment : Fragment(), ActionBarListener, MNGClickListener,
    BluestackThumbnailListener {

    private val TAG = ThumbnailFragment::class.java.simpleName
    private var mngAdsAdsFactory: MNGAdsFactory? = null
    private var listNonsense: ListView? = null
    private var mPos = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val _View = inflater.inflate(R.layout.thumbnail, container, false)
        listNonsense = _View.findViewById(R.id.listNonsense)
        createSimpleList()
        loadThumbnail()
        _View.findViewById<View>(R.id.btnTopLeft).setOnClickListener {
            mPos = 1
            loadThumbnail()
        }
        _View.findViewById<View>(R.id.btnTopRight).setOnClickListener {
            mPos = 2
            loadThumbnail()
        }
        _View.findViewById<View>(R.id.btnBottomLeft).setOnClickListener {
            mPos = 3
            loadThumbnail()
        }
        _View.findViewById<View>(R.id.btnBottomRight).setOnClickListener {
            mPos = 4
            loadThumbnail()
        }
        return _View
    }

    private fun loadThumbnail() {
        mngAdsAdsFactory = MNGAdsFactory(activity)
        mngAdsAdsFactory!!.setPlacementId(getThumbnailPlacement(context))
        mngAdsAdsFactory!!.setThumbnailListener(this)
        mngAdsAdsFactory!!.setClickListener(this)
        when (mPos) {
            1 -> mngAdsAdsFactory!!.loadThumbnail(MNGPreference.TOP_LEFT, 10, 150)
            2 -> mngAdsAdsFactory!!.loadThumbnail(MNGPreference.TOP_RIGHT, 10, 150)
            3 -> mngAdsAdsFactory!!.loadThumbnail(MNGPreference.BOTTOM_LEFT, 20, 10)
            4 -> mngAdsAdsFactory!!.loadThumbnail(MNGPreference.BOTTOM_RIGHT, 20, 10)
            else -> mngAdsAdsFactory!!.loadThumbnail()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (activity != null) mngAdsAdsFactory!!.hideThumbnail(activity)
    }

    override fun onDestroy() {
        if (mngAdsAdsFactory != null) {
            mngAdsAdsFactory!!.releaseMemory()
            mngAdsAdsFactory = null
        }
        super.onDestroy()
    }

    override fun switchParams(isOn: Boolean) {}

    override fun onAdClicked() {
        isAdClicked = true
        Log.d(TAG, "Ad Clicked")
        Toast.makeText(activity, "Ad Clicked", Toast.LENGTH_SHORT).show()
    }

    private fun createSimpleList() {
        val listAdapter = ListAdapter(10, requireActivity().applicationContext)
        listNonsense!!.adapter = listAdapter
    }

    override fun thumbnailDidLoad() {
        if (mngAdsAdsFactory != null && mngAdsAdsFactory!!.isThumbnailReady) {
            Log.d(TAG, "Thumbnail Ready")
            if (this.activity != null) {
                mngAdsAdsFactory!!.showThumbnail(activity)
                //      mngAdsAdsFactory.showThumbnail(getActivity(), TOP_RIGHT, 50, 60);
            }
        } else Log.d(TAG, "Thumbnail Not Ready")
    }

    override fun thumbnailDidClosed() {
        Log.d(TAG, "Thumbnail did closed")
    }

    override fun thumbnailDidFail(e: Exception) {
        Toast.makeText(requireActivity().applicationContext, "Thumbnail Ad did fail", Toast.LENGTH_SHORT)
            .show()
        Log.d(TAG, "Thumbnail did Fail")
        Log.d(TAG, e.toString())
    }

    override fun thumbnailDidDisplayed() {
        Log.d(TAG, "Thumbnail did displayed")
    }

    override fun thumbnailDidClicked() {}
}