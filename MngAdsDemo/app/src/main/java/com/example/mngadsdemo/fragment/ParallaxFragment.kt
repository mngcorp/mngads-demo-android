package com.example.mngadsdemo.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ListView
import android.widget.ScrollView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mngadsdemo.DemoApp.Companion.getParallaxPerfPlacement
import com.example.mngadsdemo.DemoApp.Companion.isAdClicked
import com.example.mngadsdemo.R
import com.example.mngadsdemo.adapter.ListAdapter
import com.example.mngadsdemo.adapter.ParallaxAdapter
import com.example.mngadsdemo.listenter.ActionBarListener
import com.example.mngadsdemo.utils.Utils.convertPixelsToDp
import com.example.mngadsdemo.utils.Utils.getPreference
import com.mngads.MNGAdsFactory
import com.mngads.listener.MNGClickListener
import com.mngads.listener.MNGInfeedListener
import com.mngads.listener.MNGRefreshListener
import com.mngads.util.MAdvertiseInfeedFrame
import com.mngads.util.MNGPreference

class ParallaxFragment : Fragment(), ActionBarListener, MNGClickListener, MNGInfeedListener,
    MNGRefreshListener {

    private val TAG = ParallaxFragment::class.java.simpleName

    private var mngAdsParallaxAdsFactory: MNGAdsFactory? = null
    private var mList: ListView? = null
    private var mRecycler: RecyclerView? = null
    private var mPub: LinearLayout? = null
    private var mScroll: ScrollView? = null
    private var listAdapter: ListAdapter? = null
    private var mngPreference: MNGPreference? = null
    private var mAdapter: ParallaxAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.parallax, container, false)
        initView(view)
        initTypeShow(view)
        initParallax()
        initList()
        initRecycle()
        displayParallax(mList)
        return view
    }

    private fun initView(view: View) {
        mList = view.findViewById(R.id.list)
        mRecycler = view.findViewById(R.id.recycler)
        mScroll = view.findViewById(R.id.scroll)
        mPub = view.findViewById(R.id.pub)
    }

    private fun initTypeShow(view: View) {
        view.findViewById<View>(R.id.rlScrollViewContainer).setOnClickListener {
            displayParallax(mScroll)
            mRecycler!!.visibility = View.GONE
            mList!!.visibility = View.GONE
            mScroll!!.visibility = View.VISIBLE
        }
        view.findViewById<View>(R.id.rlRecycleViewContainer).setOnClickListener {
            displayParallax(mRecycler)
            mList!!.visibility = View.GONE
            mScroll!!.visibility = View.GONE
            initRecycle()
        }
        view.findViewById<View>(R.id.rlListviewContainer).setOnClickListener {
            displayParallax(mList)
            mRecycler!!.visibility = View.GONE
            mScroll!!.visibility = View.GONE
            initList()
        }
    }

    private fun initParallax() {
        mngAdsParallaxAdsFactory = MNGAdsFactory(activity)
        mngAdsParallaxAdsFactory!!.setInfeedListener(this)
        mngAdsParallaxAdsFactory!!.setClickListener(this)
        mngAdsParallaxAdsFactory!!.setPlacementId(getParallaxPerfPlacement(context))
        mngPreference = getPreference(requireActivity())
    }

    private fun initList() {
        listAdapter = ListAdapter(15, requireActivity())
        mList!!.adapter = listAdapter
        mList!!.visibility = View.VISIBLE
    }

    private fun initRecycle() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity)
        mAdapter = ParallaxAdapter(15)
        mRecycler!!.layoutManager = layoutManager
        mRecycler!!.itemAnimator = DefaultItemAnimator()
        mRecycler!!.adapter = mAdapter
        mRecycler!!.visibility = View.VISIBLE
    }

    private fun displayParallax(view: View?) {
        val screenWidth = resources.displayMetrics.widthPixels
        val widthDP = convertPixelsToDp(screenWidth.toFloat(), requireActivity()).toInt()
        mngAdsParallaxAdsFactory!!.setInfeedMarginTopParalex(view)
        mngAdsParallaxAdsFactory!!.loadInfeed(
            MAdvertiseInfeedFrame(
                widthDP,
                MAdvertiseInfeedFrame.INFEED_RATIO_16_9
            ), mngPreference
        )
    }

    override fun onDestroy() {
        mngAdsParallaxAdsFactory?.releaseMemory()
        mngAdsParallaxAdsFactory = null
        super.onDestroy()
    }

    override fun switchParams(isOn: Boolean) {}

    override fun onAdClicked() {
        isAdClicked = true
        Toast.makeText(activity, "Ad Clicked", Toast.LENGTH_SHORT).show()
    }

    override fun infeedDidLoad(view: View, preferredHeightDP: Int) {
        listAdapter!!.setAdView(view)
        mAdapter!!.setAdView(view)
        mPub!!.removeAllViews()
        mPub!!.addView(view)
        Toast.makeText(activity, "Parallax did load", Toast.LENGTH_SHORT).show()
    }

    override fun infeedDidFail(e: Exception) {
        listAdapter!!.setAdView(null)
        mAdapter!!.setAdView(null)
        mPub!!.removeAllViews()
        Toast.makeText(activity, "Parallax did fail", Toast.LENGTH_SHORT).show()
    }

    override fun onRefreshSucceed() {
        Toast.makeText(activity, "on refresh succeed", Toast.LENGTH_SHORT).show()
    }

    override fun onRefreshFailed(e: Exception) {
        Toast.makeText(activity, "on refresh failed $e", Toast.LENGTH_SHORT).show()
    }
}